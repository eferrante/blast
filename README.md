# BLAST - Brain Lesion Analysis & Segmentation Tool

BLAST, the Brain Lesion Analysis & Segmentation Tool, implements pre-processing pipelines based on SimpleITK and several CNN architectures (DeepMedic, UNet3D, etc) for lesion and anatomy segmentation in brain images using Lasagne (Theano).

The folder "examples" include some toy examples for pre-processing and segmentation.

The **UNet3D** model implements the 3D UNet component described in the [Ensemble of Multiple Models and Architectures](https://arxiv.org/abs/1711.01468) paper. The complete EMMA model (which includes not only the UNet implemented in BLAST but also other models and architectures) won the 1st price in **BRATS 2017** competition [1].

Please, if you find this framework useful in your work and publications, reference our paper:

[1] **"Ensembles of Multiple Models and Architectures for Robust Brain Tumour Segmentation"**
K Kamnitsas, W Bai, E Ferrante, S McDonagh, M Sinclair, N Pawlowski, M Rajchl, M Lee, B Kainz, D Rueckert, B Glocker. Published in BrainLes 2017: Brainlesion: Glioma, Multiple Sclerosis, Stroke and Traumatic Brain Injuries. LNCS Springer.
[ArXiV Link](https://arxiv.org/abs/1711.01468), [Springer Link](https://link.springer.com/chapter/10.1007/978-3-319-75238-9_38)



## Installation instructions
Before installing BLAST, we will need to create a Virtual Environment and install the dependencies. These instructions suppose you are using Ubuntu Linux and a **bash** terminal.

* Install the following Ubuntu packages: 
```
$ sudo apt-get install -y gcc g++ gfortran build-essential git wget libopenblas-dev python-dev python-pip python-nose python-numpy python-scipy
```

* Install Cuda: 
 * See instalation instructions here: https://developer.nvidia.com/cuda-downloads

* Clone Blast repo
```
$ git clone https://gitlabUser:gitlabPassword@gitlab.com/eferrante/blast.git
```

* Create a virtual environment and activate it
```
$ virtualenv -p /usr/bin/python2.7 venv_lasagne
$ source venv_lasagne/bin/activate
```
* Install required dependencies by doing:
```
pip install numpy scipy matplotlib simpleitk nose pycuda
```

* Export Cuda variables: 
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/your/cuda/lib64/`
export PATH=$PATH:/path/to/your/cuda/bin/
```
 * TIP: You can set your virtualenv to automatically export these variables when you activate it. For that, you just edit the file located in venv_lasagne/bin/postactivate (you can create the file if it doesnt exist) and add the export sentences there. The next time you activate the virtualenv, it will export automatically the variablesl.

* Set Theano Flags so that it can run in GPU:
```
export THEANO_FLAGS='floatX=float32,device=gpu0,force_device=True'
```

* Clone Blast repo in your local computer (use the following command replacing gitlabUsr and password with yours):
```
git clone https://gitlabUsr:password@gitlab.com/eferrante/blast.git
```

* At this point, you should be able to run the attached example ([testGPU.py](https://gitlab.com/eferrante/blast/blob/master/examples/testGPU.py)) and you should get as a result something like:

~~~~
Using gpu device 0: TITAN X (Pascal) (CNMeM is disabled, cuDNN not available)
 0.9.0dev2.dev-RELEASE
[GpuElemwise{exp,no_inplace}(<CudaNdarrayType(float32, vector)>), HostFromGpu(GpuElemwise{exp,no_inplace}.0)]
Looping 1000 times took 0.171442 seconds
Result is [ 1.23178029  1.61879349  1.52278066 ...,  2.20771813  2.29967761
  1.62323296]
Used the gpu
~~~~

* Now we can proceed installing Lasagne. Run 
```
pip install --upgrade https://github.com/Lasagne/Lasagne/archive/master.zip
```

* You should be able to run this [mnist.py](https://github.com/Lasagne/Lasagne/blob/master/examples/mnist.py) script , running from your virtual environment (with all the exported variables) the following:
```
$ python mnist.py
```
* **NOTE:** In order to avoid exporting the environment variables every time you activate the virtual environment, you can append the 'EXPORT XXXX' lines to the bottom of the venv_lasagne/bin/activate.

### Activate cuDNN
* To activate cuDNN, just download it and unzip it in your Cuda folder: https://developer.nvidia.com/rdp/cudnn-download

### [Optional] Configuring PyCharm to run Blast
You can simple activate your Virtual Environment and run Blast from there, or use an IDE like PyCharm if you are planning to develop on top of Blast. If you are only interested in running pre-defined models, you can go to the next section.

To configure PyCharm so that we can run Blast from the IDE, we just have to do the following:

* Set your Virutal Environment venv_lasagne in **File -> Settings -> Project -> Project Interpreter** (you can add a new one choosing 'Add local...' clicking on the small gear icon and selecting the python interpreter of your virtual environment:

![image](/uploads/f7fac6dc90826cdcab7c46c492b69a66/image.png)

* Since the environment variables are not going to be exported automatically, the simplest solution to run Theano/Lasagne with GPU support is to activate the virtual envirnment in a terminal, and run pycharm-community from there.
Doc: http://lasagne.readthedocs.io/en/latest/user/installation.html

## Running Blast

We provide example configuration files in the folder blast/examples/conf, which are ready to run using the images in blast/examples/data. There are 3 type of configuration files:

*  **Model** (e.g *blast/examples/conf/Example_Model_Deepmedic.conf*) : It specifies the CNN architecture that will be used to train a model
*  **Training** (e.g *blast/examples/conf/Example_Training.conf*): It specifies parameters  about the training process, and indicates the images, ground truth annotations and ROI masks that will be used to train the model.
*  **Testing** (e.g *blast/examples/conf/Example_Testing.conf*): It specifies the parameters for testing on unseen data.

### Training
For training, you need to specify model and training configuration files. For example, to train a Deepmedic architecture using the provided toy example, you can just run:

```
$ python RunBLAST.py --mode training --conf-model ./examples/conf/Example_Model_UNet2D_Sandwich.conf --conf-training ./examples/conf/Example_Training.conf
```

This command will compile a Theano model, train it, and store the resulting pickled trained model into a subfolder called "training", in the directory specified in Example_Training (parameter *folderForOutput*). In the example provided, a file *./outputFolder/training/Blast_Training_Example_trainedModel.p* will be created. All the information about the training process is stored in a log file in *folderForOutput*.

### Testing
For testing, you need to specify the saved training model that you will use, together with the images that you want to segment. This information is provided through a testing configuration file. For example, to segment using the previously trained model, you can just run:

```
$ python RunBLAST.py --mode testing --conf-testing ./examples/conf/Example_Testing.conf
``` 

## Extra examples
We provide some extra tools 
