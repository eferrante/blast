from blast.cnn.MetricsMonitor import MetricsMonitor

import getopt
import sys
import pickle

def printHelp():
    print '\n  ======= Running MetricsMonitor ======\n'

    print '  Plot the stats for a training session: \n       $ python RunMetricsMonitor.py --mode plotSingleModel --model <trainedModel.p> [--label numLabel]\n'
    print '  \n       If no --label is provided, the plot will contain the average for all labels. \n'

    print '  Print Help: $ python RunMetricsMonitor.py -h \n'

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"",["mode=","model=", "h", "help", "label="])
    except getopt.GetoptError as err:
        print "\n  ERROR: " + str(err)

        printHelp()
        sys.exit(2)

    labelValue = -1

    for opt, arg in opts:
        if opt == "--mode":
            mode = arg
        elif opt == "--model":
            modelFilename = arg
        elif opt == "--label":
            labelValue = int(arg)
        elif opt == "--help" or opt == "-h":
            printHelp()
            sys.exit(0)

    if opts == []:
        printHelp()
        sys.exit(0)

    if mode == "plotSingleModel":
        print "Plotting single model .."
        dump = pickle.load(open(modelFilename, "rb"))
        stats = dump['stats']
        confModel = dump['confModel']
        print labelValue
        MetricsMonitor.plotStats(stats, confModel, labelValue=labelValue)
    else:
        print "ERROR: Mode '" + "' not recongnized. \n"
        printHelp()
if __name__ == "__main__":
   main(sys.argv[1:])