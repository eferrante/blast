from Queue import Queue
from threading import Thread
import datetime
import time
import random as rnd
import nibabel as nib
import numpy as np
import logging
from ..aux import Common
import gc
import cv2
import scipy.ndimage.interpolation
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter

""" #################################################################################################################
                                              BatchGenerator Class
    ################################################################################################################# """

class BatchGenerator:
    """
        This class implements an interface for batch generators. Any batch generator deriving from this class
        should implement its own data augmentation strategy.

        In this class, we suppose that in one epoch we process several subepochs, every one formed by multiple batches
        (every batch is processed independently by the CNN). In every subepoch, new volumes are read from disk
        and used to sample segments for several batches.

        The method generateBatches( ) runs a thread that will call the abstract method generateBatchesForOneEpoch( ) as many
        times as confTrain['numEpochs'] indicates. The method generateBatchesForOneEpoch( ) runs several subepochs,
        reading new volumes in every subepoch. In every subepoch, this method generates several batches composed
        of different random segments extracted from the loaded volumes.
        The batches will be all queued in self.queue using self.queue.put(batch).

        Note that generateBatchesForOneEpoch( ) should read data only once, produce all the necessary batches for the
        given subepochs, insert them at the end of the queue and close the data files.

        The idea is that all the batches (corresponding to all the epochs) are stored (in order) in the same queue.

        The methods that must be implemented by any class inheriting BatchGenerator are:
        - generateBatchesForOneEpoch(self):
            This method will read some data, produce all the necessary batches for the subepochs, insert them at
            the end of the queue and close the data files.

        The class using any BatchGenerator will proceed as follows:

        ===================== Training Loop =====================

            batchGen = BatchGenerator(confTrain)
            batchGen.generateBatches()

            for e in range(0, confTrain['numEpochs']):
                for se in range (0, confTrain['numSubepochs']):
                    batchesPerSubepoch = confTrain['numTrainingSegmentsLoadedOnGpuPerSubep'] // confTrain['batchSizeTraining']

                    for bps in range(0, batchesPerSubepoch)
                        batch = batchGen.getBatch()
                        updateCNN(batch)

            assert (batchGen.emptyQueue()), "The training loop finished before the queue is empty".

        ==========================================

         ====== Pseudocode of the data generation loop =======
         for epoch in range(0, numEpochs):
           for subepoch in range (0, numSubepochs):
               read 'numOfCasesLoadedPerSubepoch' volumes
               batchesPerSubepoch = numberTrainingSegmentsLoadedOnGpuPerSubep // batchSizeTraining
               for batch in range(0, batchesPerSubepoch):
                   data = extractSegments(batchSizeTraining)
                   queue.put(data)

         ====== Pseudocode of the training loop (running in parallel with data generation) =======
         for epoch in range(0, numEpochs):
           for subepoch in range (0, numSubepochs):
               batchesPerSubepoch = numberTrainingSegmentsLoadedOnGpuPerSubep // batchSizeTraining
               for batch in range(0, batchesPerSubepoch):
                   data = queue.get()
                   updateCNNWeights(data)
    """

    def __init__(self, confTrain, confModel, maxQueueSize = 15, infiniteLoop = False):
        """
            Creates a batch generator.

            :param confTrain: a configuration dictionary containing the necessary training parameters
            :param maxQueueSize: maximum number of batches that will be inserted in the queue at the same time.
                           If this number is achieved, the batch generator will wait until one batch is
                           consumed to generate a new one.

                           The number of elements in the queue can be monitored using getNumBatchesInQueue. The queue
                           should never be empty so that the GPU is never idle. Note that the bigger maxQueueSize,
                           the more RAM will the program consume to store the batches in memory. You should find
                           a good balance between RAM consumption and keeping the GPU processing batches all the time.

            :param infiniteLoop: if it's True, then epochs are ignored and the batch generator itearates until it's
                                killed by the system.

            :return: self.queue.empty()
        """
        self.confTrain = confTrain
        self.confModel = confModel
        print "Creating Queue with size: " + str(maxQueueSize)
        self.queue = Queue(maxsize=maxQueueSize)
        self.currentEpoch = 0
        self.infiniteLoop = infiniteLoop

        self.rndSequence = rnd.Random()
        self.rndSequence.seed(1)
        #self.rndSequence.seed(2)
        self.keepOnRunning = True

        # ID used when printing LOG messages.
        self.id = "[BATCHGEN]"

    def emptyQueue(self):
        """
            Checks if the batch queue is empty or not.

            :return: self.queue.empty()
        """

        return self.queue.empty()

    def _generateBatches(self):
        """
            Private function that generates as many batches as epochs were specified
        """
        self.currentEpoch = 0

        while (self.infiniteLoop or (self.currentEpoch < self.confTrain['numEpochs'])) and self.keepOnRunning:
            self.generateBatchesForOneEpoch()
            self.currentEpoch += 1

        logging.info(self.id + " The batch generation process finished. Elements still in the queue before finishing: %s. The queue will be destroyed." % str(self.getNumBatchesInQueue()))

    def generateBatches(self):
        """
            This public interface lunches a thread that will start generating batches for the epochs/subepochs specified
            in the configuration file, and storing them in the self.queue.
            To extract these batches, use self.getBatch()
        """
        worker = Thread(target=self._generateBatches, args=())
        worker.setDaemon(False)
        worker.start()

    def getBatch(self):
        """
            It returns a batch and removes it from the front of the queue

            :return: a batch from the queue
        """
        batch = self.queue.get()
        self.queue.task_done()
        return batch

    def getNumBatchesInQueue(self):
        """
            It returns the number of batches currently in the queue, which are ready to be processed

            :return: number of batches in the queue
        """
        return self.queue.qsize()

    def finish(self, delay = .5):
        """
            It will interrupt the batch generation process, even if there are still batches to be created.
            If the batch generator is currently producing a batch, then it will stop after finishing that batch.
            The queue will be destroyed together with the process.

            Note: if there is a process waiting for a batch in the queue, the behaviour is unpredictable. The process
                  that is waiting may wait forever.

            :param delay: the delay will be used after getting an element from the queue. If your batch generation
                        process is too time consuming, you should increase the delay to guarantee that once
                        the queue is empty, the batch generation process is done.
        """
        self.keepOnRunning = False
        logging.info(self.id + " Stopping batch generator. Cleaning the queue which currently contains %s elements ..." % str(self.queue.qsize()))

        while not self.queue.empty():
            self.queue.get_nowait()
            self.queue.task_done()

            time.sleep(delay)
            if not self.queue.empty():
                logging.info(self.id + " Still %s elements in the queue ..." % str(self.queue.qsize()))

        logging.info(self.id + " Done.")

    def generateBatchesForOneEpoch(self):
        """
            This abstract function must be implemented. It must generate all the batches corresponding to one epoch
            (one epoch is divided in subepochs where different data samples are read from disk, and every subepoch is
            composed by several batches, where every batch includes many segments.)

            Every batch must be queued using self.queue.put(batch) and encoded using lasagne-compatible format,
            i.e: a 5D tensor with size (batch_size, num_input_channels, input_depth, input_rows, input_columns)

        """
        raise NotImplementedError('users must define "generateBatches" to use this base class')


""" #################################################################################################################
                                              DummyBatchGenerator Class
    ################################################################################################################# """
class DummyBatchGenerator(BatchGenerator):
    """
        Dummy BatchGenerator class that gives some template to implement a useful batch generator.
    """

    def __init__(self, confTrain, confModel, maxQueueSize = 15):
        BatchGenerator.__init__(self, confTrain, confModel, maxQueueSize)

    def generateBatchesForOneEpoch(self):
        for se in range(self.confTrain['numSubepochs']):
            batchesPerSubepoch = self.confTrain['numTrainingSegmentsLoadedOnGpuPerSubep'] // self.confTrain['batchSizeTraining']
            for batch in range(0, batchesPerSubepoch):
                newBatch = range(0, self.confTrain['batchSizeTraining'])
                self.queue.put(newBatch)

""" #################################################################################################################
                                              Simple3DBatchGenerator Class
    ################################################################################################################# """


def checkFlip(array, flip):
    if flip:
        return np.flipud(array)
    else:
        return array

def checkAddGaussianNoise(array, addNoise, sigma = 0.1):
    if addNoise:
        # Add gaussian noise
        gauss = np.random.normal(loc=0, scale=sigma, size=(array.shape[0], array.shape[1], array.shape[2]))
        gauss = gauss.reshape(array.shape[0], array.shape[1], array.shape[2])
        array += gauss

    return array

def rotateImage(img, angle=0.0, interp_order=1, invert = False):
    # Perform affine transformation on image and label, which are 3D tensors of dimension (X, Y, Z).

    # Based on code from Ozan Oktay.

    if angle != 0.0:

        if len(img.shape) == 3:
            row, col = img.shape[0:2]

            M = cv2.getRotationMatrix2D((row / 2, col / 2), angle, 1.0)

            if invert:
                M_inv = np.ndarray(shape=M.shape)
                cv2.invertAffineTransform(M, M_inv)
                M = M_inv

            for z in range(img.shape[2]):
                img[:, :, z] = scipy.ndimage.interpolation.affine_transform(img[:, :, z], M[:, :2], M[:, 2], order = interp_order)

        elif len(img.shape) == 4:
            row, col = img.shape[1:3]

            M = cv2.getRotationMatrix2D((row / 2, col / 2), angle, 1.0)
            if invert:
                M_inv = np.ndarray(shape=M.shape)
                cv2.invertAffineTransform(M, M_inv)
                M = M_inv

            for c in range(img.shape[0]):
                for z in range(img.shape[3]):
                    img[c, :, :, z] = scipy.ndimage.interpolation.affine_transform(img[c, :, :, z].T, M[:, :2], M[:, 2], order = interp_order)
        elif len(img.shape) == 5:
            row, col = img.shape[2:4]
            print str(row)  + ", "  + str(col)

            M = cv2.getRotationMatrix2D((row / 2, col / 2), angle, 1.0)
            if invert:
                M_inv = np.ndarray(shape=M.shape)
                cv2.invertAffineTransform(M, M_inv)
                M = M_inv

            for s in range(img.shape[0]):
                for c in range(img.shape[1]):
                    for z in range(img.shape[4]):
                        img[s, c, :, :, z] = scipy.ndimage.interpolation.affine_transform(img[s, c, :, :, z], M[:, :2], M[:, 2], order = interp_order)

class Simple3DBatchGenerator(BatchGenerator):
    """
        Simple batch generator that takes multi-channel 3D images with labels and masks,
        and samples random segments from them following the default strategy by DeepMedic.
    """

    def __init__(self, confTrain, confModel, mode = 'training', infiniteLoop = False, maxQueueSize = 15):

        """
            Initialize a 3D batch generator with a confTrain and a confModel object.
            Modes can be 'training' or 'validation'. Depending on the mode, the batch generator
            will load the training files (channelsTraining, gtLabelsTraining, roiMasksTraining)
            or the validation files (channelsValidation, gtLabelsValidation, roiMasksValidation) from
            the confTrain object.

            If 'infiniteLoop', then the batch generator will generate batches until is killed by the system
            and it will ignore the subepochs and epochs parameters.
        """
        BatchGenerator.__init__(self, confTrain, confModel, infiniteLoop = infiniteLoop, maxQueueSize=maxQueueSize)

        # List of currently loaded channel images (size:numOfCasesLoadedPerSubepochTraining or numOfCasesLoadedPerSubepochValidation X channels)
        self.currentChannelImages = []
        # List of currently loaded ROI images (size:numOfCasesLoadedPerSubepochTraining or numOfCasesLoadedPerSubepochValidation)
        self.currentRois = []
        # List of currently loaded GT images (size:numOfCasesLoadedPerSubepochTraining or numOfCasesLoadedPerSubepochValidation)
        self.currentGt = []

        # Mode: can be training or validation
        self.mode = mode
        if mode == 'training':
            self.id = '[BATCHGEN TRAIN]'
        elif mode == 'validation':
            self.id = '[BATCHGEN VAL]'

        if not infiniteLoop:
            self.numSubepochs = confTrain['numSubepochs']
        else:
            self.numSubepochs = 1

        self.sampledCoordinates = []

        # ===================================== ABOUT THE IMAGE CHANNELS LIST INDEXING     ================================
        # ===================================== IMPORTANT (READ BEFORE MODIFYING ANYTHING) ================================

        # IMPORTANT: The indices in the lists of channel filenames are inverted with respect to the currently loaded images
        # self.currentChannelImages is [img][channel] while the filenames list self.allChannelsFilenames is [channel][img]

        # ===================================== IMPORTANT (READ BEFORE MODIFYING ANYTHING) ================================

        # List of lists of filenames (size: numChannels x numOfFilenames)
        self.allChannelsFilenames = []
        # List of roiMask filenames (size: numOfFilenames)
        self.roiFilenames = []
        # List of GT filenames (size:numOfFilenames)
        self.gtFilenames = []

        # Allowed indices for a given volume and label are stored in the following structure indexed by [volumeNbr][label]
        # self.allowedIndices[volumeNbr][label] returns a list with the indices corresponding to the voxels in volumeNbr having label 'label'
        # If ROIs are specified, then the indices inserted in this list must be into the corresponding ROIs.
        # IMPORTANT: volumeNbr here is an index relative to the choosen samples (0 < volumeNbr < self.confTrain['numOfCasesLoadedPerSubepochTraining'].
        #            It does not correspond to the real index in the list of ALL the filenames.
        self.allowedIndicesPerVolumePerLabel = {}

        # Dictionary indexed by label index (not value). By label index i mean the position of the label in the list of labels defined in the confModel.
        # The dict is then indexed as [labelIndex] -> list of volumes that contain this label.
        # IMPORTANT: volumeNbr here is an index relative to the choosen samples (0 < volumeNbr < self.confTrain['numOfCasesLoadedPerSubepochTraining'].
        #            It does not correspond to the real index in the list of ALL the filenames.
        self.volumesContainingLabel = {}

        # List of label indices allowed to be sampled in the current subepoch
        self.allowedLabels = []

        # List of volume index (absolute indexes, i.e. the index from the list of image filenames) that has not been chosen yet to be sampled.
        # Every time new volumes are loaded from disk, they are fetched from this list and removed from it. Once the list lenght becomes < self.numOfCasesLoadedPerSubepoch
        # the list is refilled with all the indices of the image filenames.
        self.volumesNotSampledYet = []

        self.numClasses =  len(self.confModel['labels'])

        if self.numClasses > 2 and not self.confTrain['ensureProportionalLabelSamplingPerBatch']:
            print("WARNING: SAMPLING STRATEGY IS NOT CURRENTLY CONSIDERING BALNCE BETWEEN MULTIPLE FOREGROUND CLASSES. IF YOU WANT TO BALANCE THEM, THEN SET ensureProportionalLabelSamplingPerBatch = True IN THE TRAINING CONFIGURATION FILE")

        # Load the filenames from the configuration files and the parameters specific to the mode
        if mode == 'training':
            logging.info('-- Initializing TRAINING Batch generator')
            self.numChannels = len(self.confTrain['channelsTraining'])
            self.numOfCasesLoadedPerSubepoch = self.confTrain['numOfCasesLoadedPerSubepochTraining']
            self.loadFilenames(self.confTrain['channelsTraining'], self.confTrain['gtLabelsTraining'],
                          self.confTrain['roiMasksTraining'] if ('roiMasksTraining' in self.confTrain) else None)
        elif mode == 'validation':
            logging.info('-- Initializing VALIDATION Batch generator')
            self.numChannels = len(self.confTrain['channelsValidation'])
            self.numOfCasesLoadedPerSubepoch = self.confTrain['numOfCasesLoadedPerSubepochValidation']
            self.loadFilenames(self.confTrain['channelsValidation'], self.confTrain['gtLabelsValidation'],
                               self.confTrain['roiMasksValidation'] if ('roiMasksValidation' in self.confTrain) else None)
        else:
            raise Exception('ERROR: Batch generator mode is not valid. Valid options are training or validation')


        # Check if, given the number of cases loaded per subepoch and the total number of samples, we
        # will need to load new data in every subepoch or not.

        # If numOfCasesLoadedPerSubepoch == -1, it means that all data will be loaded only once.
        if self.numOfCasesLoadedPerSubepoch == -1:
            self.loadNewFilesEverySubepoc = False
        else:
            self.loadNewFilesEverySubepoc = len(self.allChannelsFilenames[0]) > self.numOfCasesLoadedPerSubepoch

        logging.info(self.id + " Loading new files every subepoch: " + str(self.loadNewFilesEverySubepoc))

        # This shouldnt be harcoded
        self.tileSize = confModel['tileSize']
        self.gtSize = confModel['gtSize']

    def loadFilenames(self, channels, gtLabels, roiMasks = None):
        """
            Load the filenames that will be used to generate the batches.

            :param channels: list containing the path to the text files containing the path to the channels (this list
                            contains one file per channel).
            :param gtLabels: path of the text file containing the paths to gt label files
            :param roiMasks: [Optional] path of the text file containing the paths to ROI files

        """
        assert len(channels) == self.confModel["numChannels"], "ERROR: The number of channels specified in the model conf file (confModel[\"numChannels\"]) must be equal to the number of files in the list containing the training/validation/test images in the corresponding configuration file (confTrain[\"channelsTraining\"] or confTrain[\"channelsVal\"] or confTest[\"channelsTest\"])"

        self.allChannelsFilenames, self.gtFilenames, self.roiFilenames, _ = Common.loadFilenames(channels, gtLabels, roiMasks)

    def unloadFiles(self):
        logging.debug(self.id + " Unloading Files")

        for image in self.currentChannelImages:
            for channelImage in image:
                del channelImage
            del image

        for image in self.currentGt:
            del image


        for image in self.currentRois:
            del image

        del self.currentChannelImages
        del self.currentGt
        del self.currentRois

        for k in self.allowedIndicesPerVolumePerLabel.keys():
            for q in self.allowedIndicesPerVolumePerLabel[k].keys():
                del self.allowedIndicesPerVolumePerLabel[k][q]
            del self.allowedIndicesPerVolumePerLabel[k]

        del self.allowedIndicesPerVolumePerLabel

        self.currentChannelImages = []
        self.currentGt = []
        self.currentRois = []
        self.allowedIndicesPerVolumePerLabel = {}

        gc.collect()

    def generateRandomSegmentCenteredAtLabel(self, volumeToSample, labelToSample, useRoi = False):
        """
            Generates a 3D segment and the corresponding Ground truth
            :param volumeToSample: indicates the volume number (from the current ones) that will be sampled
            :param labelToSample: indicates in which label must the segment be centered
            :param useRoi: if it's true, then only voxels from within this roi will be sampled (independently of the label)

            :return: returns a 3D segment and the corresponding ground truth
        """
        #writeSegmentsToDisk = True
        # Filter allowed indices given the ROI and the labelToSample values.
        labelCandidates = self.allowedIndicesPerVolumePerLabel[volumeToSample][labelToSample]
        voxelIndex = labelCandidates[self.rndSequence.randint(0, labelCandidates.shape[0]-1),:]

        #self.sampledCoordinates.append(np.squeeze(voxelIndex))

        tileOffset = [x // 2 for x in self.tileSize]
        extraTileOffset = [ x % 2 for x in self.tileSize]

        gtOffset = [x // 2 for x in self.gtSize]
        extraGtOffset = [x % 2 for x in self.gtSize]

        data = np.ndarray(shape=(1, self.numChannels, self.tileSize[0], self.tileSize[1], self.tileSize[2]), dtype=np.float32)
        gt =   np.ndarray(shape=(1, self.numClasses,  self.gtSize[0], self.gtSize[1], self.gtSize[2]), dtype=np.float32)

        # Check if we have to flip the segment
        flip = np.random.binomial(1, self.confTrain["randomlyFlipSegmentsProbability"], 1)[0] == 1
        addGaussianNoise = np.random.binomial(1, self.confTrain["randomlyAddGaussianNoiseProbability"], 1)[0] == 1

        #logging.debug(self.id + " Extracting patch centered at voxel %s with label %d from volume %d" % (str(voxelIndex),  labelToSample, volumeToSample))
        #logging.debug(self.id + " Real label: %d" % self.currentGt[volumeToSample].get_data()[voxelIndex[0],voxelIndex[1],voxelIndex[2]])
        #assert self.currentGt[volumeToSample].get_data()[voxelIndex[0],voxelIndex[1],voxelIndex[2]] == labelToSample, "The label is not the one it was supposed to be"

        for channel in range(self.numChannels):
            data[0,channel,:,:,:] = checkAddGaussianNoise(checkFlip(self.currentChannelImages[volumeToSample][channel][voxelIndex[0]-tileOffset[0]:voxelIndex[0]+tileOffset[0]+extraTileOffset[0],
                                                                                                                          voxelIndex[1]-tileOffset[1]:voxelIndex[1]+tileOffset[1]+extraTileOffset[1],
                                                                                                                          voxelIndex[2]-tileOffset[2]:voxelIndex[2]+tileOffset[2]+extraTileOffset[2]],
                                                             flip), addGaussianNoise, sigma=self.confTrain['sigmaForGaussianNoise'])

        # Crop the area corresponding to the GT
        auxGt = checkFlip(self.currentGt[volumeToSample][voxelIndex[0] - gtOffset[0]:voxelIndex[0] + gtOffset[0] + extraGtOffset[0],
                voxelIndex[1] - gtOffset[1]:voxelIndex[1] + gtOffset[1] + extraGtOffset[1],
                voxelIndex[2] - gtOffset[2]:voxelIndex[2] + gtOffset[2] + extraGtOffset[2]],
                                         flip)


        # Transform the GT in a format that can be read by
        for i in range(self.numClasses):
            gt[0, i, :, :, :] = (auxGt==self.confModel['labels'][i]).astype(np.int16)

#        if writeSegmentsToDisk:
#            name = str(rnd.randint(0,100000))
#            affine = np.diag([1, 1, 1, 1])
#            array_img = nib.Nifti1Image(np.squeeze(data[0, 0, :, :, :]), affine)
#            nib.save(array_img, '../tmp/' + name +"_data.nii")

#            array_img = nib.Nifti1Image(np.squeeze(auxGt), affine)
#            nib.save(array_img, '../tmp/' + name + "_gt_all.nii")

#            array_img = nib.Nifti1Image(np.squeeze(gt[0, 0, :, :, :]), affine)
#            nib.save(array_img, '../tmp/' + name + "_gt0.nii")

#            array_img = nib.Nifti1Image(np.squeeze(gt[0, 1, :, :, :]), affine)
#            nib.save(array_img, '../tmp/' + name + "_gt1.nii")

        return data, gt

    def generateSingleBatch(self):
        """
            Creates a batch of segments according to the conf file. It supposes that the images are already
            loaded in self.currentVolumes.

            :return: It returns the data and ground truth of a complete batch as data, gt. These structures are theano-compatible with shape:
                        np.ndarray(shape=(self.confTrain['batchSizeTraining'], self.numChannels, tileSize, tileSize, tileSize), dtype=np.float32)
        """

        if self.mode == 'validation':
            batchSize = self.confTrain['batchSizeValidation']
        else:
            batchSize = self.confTrain['batchSizeTraining']

        # tileSize, this variable shouldnt be modified. If it is, the CNNModel tileSize should be modified accordingly.

        batch = np.ndarray(shape=(batchSize, self.numChannels, self.tileSize[0], self.tileSize[1], self.tileSize[2]), dtype=np.float32)
        gt =    np.ndarray(shape=(batchSize, self.numClasses,  self.gtSize[0], self.gtSize[1], self.gtSize[2]),       dtype=np.float32)

        # TODO: Add support for multilabel
        # The segments are going to be extracted from patches centered in voxels with the labels stored in labelsToSample
        # to ensure a balanced sampling. If not 'ensureProportionalLabelSamplingPerBatch' then the preportions defined by
        labelsToSample = []
        if not self.confTrain['ensureProportionalLabelSamplingPerBatch']:
            # Number of segments that will be centered at a positive sample
            numPositiveCases = int(batchSize * self.confTrain['percentOfSamplesToExtractPositiveTrain'])

            labelsToSample = [1] * numPositiveCases + [0] * (batchSize - numPositiveCases)
            self.rndSequence.shuffle(labelsToSample)
        else:
            #labelsToSample = [self.rndSequence.randint(0, len(self.confTrain['labels'])-1) for x in range(batchSize)]
            labelsToSample = [self.rndSequence.choice(self.allowedLabels) for x in range(batchSize)]

        # If there are ROIs loaded, then positive and negative samples are only sampled from those voxels whose ROI value is 1
        useRoi = not ( self.currentRois == [] )

        #logging.debug(self.id + " Sampling labels: " + str(labelsToSample))

        if not(self.currentChannelImages == []):
            # For every sample we have to generate
            # TODO: Parallelize the data augmentation in the segment level, in CPU.
            for i in range(0, batchSize):
                # Choose a random volume to sample from
                if not self.confTrain['ensureProportionalLabelSamplingPerBatch']:
                    if self.numOfCasesLoadedPerSubepoch == -1:
                        # Then all images are loaded
                        volumeToSampleFrom = self.rndSequence.randint(0, len(self.allChannelsFilenames[0]) - 1)
                    else:
                        # Then only self.numOfCasesLoadedPerSubepoch are loaded
                        volumeToSampleFrom = self.rndSequence.randint(0, self.numOfCasesLoadedPerSubepoch - 1)
                else:
                    volumeToSampleFrom = self.rndSequence.choice(self.volumesContainingLabel[labelsToSample[i]])
                # Generate a new segment (data and ground truth) and add it to the batch
                batch[i, :, :, :, :], gt[i, :, :, :, :] = self.generateRandomSegmentCenteredAtLabel(volumeToSampleFrom, labelsToSample[i], useRoi)

#            if False:
#                name = "/vol/biomedic/users/eferrant/code/simple-deepmedic/tmp/imgs/" + "Vol_"+str(volumeToSampleFrom) + "_lab_" + str(labelsToSample[i]) + str(datetime.datetime.now()).replace(' ', '_')
#                array_img = nib.Nifti1Image(np.squeeze(gt[i, :, :, :, :]), affine=None)

#                nib.save(array_img, name + "_gt.nii.gz")
#                array_img = nib.Nifti1Image(batch[i, :, :, :, :], affine=None)

#                nib.save(array_img, name + "_img.nii.gz")

            # Return the batch data with its corresponding ground truth
            return batch, gt
        else:
            raise Exception(self.id + " No images loaded in self.currentVolumes." )

    def checkExtraPadding(self, imgArray, mandatoryUseMinVal = False):
        if self.confTrain['padTrainingVolumesForTileSampling']:
            if not mandatoryUseMinVal and (self.confModel['paddingStrategy'] == 'fixedValue'):
                padVal = self.confModel['paddingValue']
            else:
                padVal = np.min(imgArray)

            if self.confModel['modelType'] == 'SimpleDeepmedic2D' or self.confModel['modelType'] == 'SimpleDeepmedic_Sandwich':
                return np.pad(imgArray, pad_width = ((21,21),(21,21),(1,1)), mode = 'constant', constant_values = padVal)
            elif self.confModel['modelType'] == 'UNet2D_Sandwich':
                return np.pad(imgArray, pad_width=((0, 0), (0, 0), (1, 1)), mode='constant', constant_values=padVal)
            elif self.confModel['modelType'] == 'UNet3D':
                return np.pad(imgArray, pad_width=((0, 0), (0, 0), (0, 0)), mode='constant', constant_values=padVal)
            elif self.confModel['modelType'] == 'SimpleDeepmedic_3Paths':
                return np.pad(imgArray, pad_width=((24, 24), (24, 24), (24, 24)), mode='constant', constant_values=padVal)

            else:
                return np.pad(imgArray, 21, 'constant', constant_values =padVal)
        else:
            return imgArray

    def checkDeform(self, shape, deform, gt, sigma=4, alpha=50):
        """
            Deform image randomly. Based on code by Ricardo Guerrero Moreno.

            :param shape: shape of the image (Nx, Ny, Nz).
            :param deform: get a deformation index matrix or identity (no) deformation
            :param sigma: how smooth should the deformation be
            :param alpha: strength of the deformation
        """
        if deform:
            w = np.zeros(shape=gt.shape)# Per class deformation strength matrix
            w[gt == 0] = 1 # strength half of alpha
            w[gt == 1] = 2 # strength equal to alpha
            w[gt == 2] = 4 # strength twice of alpha
            dx = (gaussian_filter((np.random.rand(*shape) * 2 - 1) * w, sigma, mode="constant", cval=0) * alpha)
            dy = (gaussian_filter((np.random.rand(*shape) * 2 - 1) * w, sigma, mode="constant", cval=0) * alpha)
            dz = (gaussian_filter((np.random.rand(*shape) * 2 - 1) * w, sigma, mode="constant", cval=0) * alpha)
            x, y, z = np.mgrid[0:shape[0], 0:shape[1], 0:shape[2]]
            inds = np.reshape(x + dx, (-1, 1)), np.reshape(y + dy, (-1, 1)), np.reshape(z + dz, (-1, 1))
        else:
            x, y, z = np.mgrid[0:shape[0], 0:shape[1], 0:shape[2]]
            inds = np.reshape(x, (-1, 1)), np.reshape(y, (-1, 1)), np.reshape(z, (-1, 1))

        return inds

    def generateBatchesForOneEpoch(self):
        labels = self.confModel['labels']

        for se in range(self.numSubepochs):

            if ((self.currentEpoch == 0) and (se == 0)) or self.loadNewFilesEverySubepoc:
                # Choose the random images that will be sampled in this epoch
                #print str(len(self.allChannelsFilenames[0]))

                # Refill the list of volumes to be loaded with all the indices if there are not enough volumes
                if len(self.volumesNotSampledYet) < self.numOfCasesLoadedPerSubepoch:
                    self.volumesNotSampledYet = list(xrange(0, len(self.allChannelsFilenames[0])))

                logging.debug(self.id + " Volumes still not sampled (" + str(len(self.volumesNotSampledYet)) + ") = " + str(self.volumesNotSampledYet))

                print "Loading images ....."
                # Select the volumes that will be loaded in the current subepoch. If self.numOfCasesLoadedPerSubepoch == -1, then all volumes are loaded only once.
                if not self.numOfCasesLoadedPerSubepoch == -1:
                    indexCurrentImages = self.rndSequence.sample(self.volumesNotSampledYet, self.numOfCasesLoadedPerSubepoch)
                else:
                    indexCurrentImages = range(0, len(self.allChannelsFilenames[0]))

                # Remove the loaded volumes from the list of volumes that will be loaded
                self.volumesNotSampledYet = [x for x in self.volumesNotSampledYet if x not in indexCurrentImages]

                # Clean the list of volumes containing every label
                for l in range(len(labels)):
                    self.volumesContainingLabel[l] = []

                logging.debug(self.id + " Loading images number : %s" % indexCurrentImages )

                # Load the images for the epoch
                i = 0
                for realImageIndex in indexCurrentImages:
                    print "Loading image %s" % str(realImageIndex)
                    # Decide if the image is going to be deformed or not
                    deform = np.random.binomial(1, self.confTrain["randomlyDeformSegmentsProbability"], 1)[0] == 1

                    #rotate = ('scaleRotationAngleAugmentation' in self.confTrain.keys()) and (self.confTrain["scaleRotationAngleAugmentation"] != 0.0) and np.random.binomial(1, self.confTrain["randomlyRotateProbability"], 1)[0] == 1
                    rotate = False

                    if rotate:
                        angle = np.random.uniform(-self.confTrain["scaleRotationAngleAugmentation"],self.confTrain["scaleRotationAngleAugmentation"])

                    # Load GT for the current image
                    gt = self.checkExtraPadding(nib.load(self.gtFilenames[realImageIndex]).get_data(), mandatoryUseMinVal=True)

                    # Create the deformation field in case we will deform the volume for data augmentation
                    shape = gt.shape
                    if deform:
                        deformationInds = self.checkDeform(shape, deform, gt)
                        gt = map_coordinates(gt, deformationInds, order = 0, mode='constant').reshape(shape)

                    if rotate:
                        rotateImage(gt, angle, interp_order=0)

                    self.currentGt.append(gt)

                    # For every channel
                    loadedImageChannels = []
                    img=None
                    for channel in range(0, len(self.allChannelsFilenames)):
                        # Load the corresponding image for the corresponding channel and append it to the list of channels
                        # for the current imageIndex
                        img = self.checkExtraPadding(nib.load(self.allChannelsFilenames[channel][realImageIndex]).get_data())

                        if deform:
                            img = map_coordinates(img, deformationInds, order=1, mode='constant').reshape(shape)

                        if rotate:
                            rotateImage(img, angle, interp_order=1)

                        loadedImageChannels.append(img)

                        # Check that all the channels have the same dimensions
                        if channel > 0:
                            assert loadedImageChannels[channel].shape == loadedImageChannels[0].shape, self.id + " Data size incompatibility when loading image channels for volume %s" % self.allChannelsFilenames[channel][realImageIndex]

                    assert gt.shape == loadedImageChannels[0].shape, self.id + " Data size incompatibility when loading GT %s" % self.gtFilenames[realImageIndex]

                    # Append all the channels of the image to the list
                    self.currentChannelImages.append(loadedImageChannels)
                    roi = None
                    if not (self.roiFilenames == []):
                        # Load roi for the current image
                        roi = self.checkExtraPadding(nib.load(self.roiFilenames[realImageIndex]).get_data(), mandatoryUseMinVal=True)

                        if deform:
                            roi = map_coordinates(roi, deformationInds, order=0, mode='constant').reshape(shape)

                        if rotate:
                            rotateImage(roi, angle, interp_order=0)

                        assert roi.shape == loadedImageChannels[0].shape, self.id + " Data size incompatibility when loading ROI %s" % self.roiFilenames[realImageIndex]
                        self.currentRois.append(roi)

                    # Filter pixel positions by label. If ROI is specified, then only those pixels within the ROI are included.
                    #np.unique(gt.get_data())

                    self.allowedIndicesPerVolumePerLabel[i] = {}

                    # Create a mask to restrict the indices to valid limits
                    validArea = np.zeros(shape=gt.shape)

                    tileOffset = [x // 2 + 1 for x in self.tileSize]

                    validArea[tileOffset[0]:-tileOffset[0], tileOffset[1]:-tileOffset[1], tileOffset[2]:-tileOffset[2]] = 1

                    if not self.confTrain['ensureProportionalLabelSamplingPerBatch']:
                        if roi is not None:
                            self.allowedIndicesPerVolumePerLabel[i][0] = np.argwhere((roi == 1) & (gt == labels[0]) & (validArea == 1))
                            self.allowedIndicesPerVolumePerLabel[i][1] = np.argwhere((roi == 1) & (gt != labels[0]) & (validArea == 1))
                        else:
                            self.allowedIndicesPerVolumePerLabel[i][0] = np.argwhere((gt == labels[0]) & (validArea == 1))
                            self.allowedIndicesPerVolumePerLabel[i][1] = np.argwhere((gt != labels[0]) & (validArea == 1))

                        assert (len(self.allowedIndicesPerVolumePerLabel[i][0]) > 0), "ERROR: No label background could be sampled from the valid area in the GT image %s" % (self.gtFilenames[realImageIndex])
                        assert (len(self.allowedIndicesPerVolumePerLabel[i][1]) > 0), "ERROR: No label foreground could be sampled from the valid area in the GT image %s" % (self.gtFilenames[realImageIndex])
                    else:
                        for l in range(len(labels)):
                            if roi is not None:
                                self.allowedIndicesPerVolumePerLabel[i][l] = np.argwhere((roi == 1) & (gt == labels[l]) & (validArea == 1))
                            else:
                                self.allowedIndicesPerVolumePerLabel[i][l] = np.argwhere((gt == labels[l]) & (validArea == 1))

                            if len(self.allowedIndicesPerVolumePerLabel[i][l]) > 0:
                                self.volumesContainingLabel[l].append(i)

                    if deform:
                        name = "/vol/biomedic/users/eferrant/code/simple-deepmedic/tmp/imgs/" + Common.getMedicalImageBasename(self.allChannelsFilenames[0][realImageIndex]) + str(datetime.datetime.now()).replace(' ','_')
                        array_img = nib.Nifti1Image(gt, affine=None)

                        nib.save(array_img, name + "_gt.nii.gz")
                        array_img = nib.Nifti1Image(img, affine=None)

                        nib.save(array_img, name + "_img.nii.gz")
                        array_img = nib.Nifti1Image(roi, affine=None)

                        nib.save(array_img, name + "_roi.nii.gz")

    #                for l in labels:
    #                    if roi is not None:
    #                        self.allowedIndicesPerVolumePerLabel[i][l] = np.argwhere((roi.get_data() == 1) & (gt.get_data() == l) & (validArea == 1))
    #                    else:
    #                        self.allowedIndicesPerVolumePerLabel[i][l] = np.argwhere((gt.get_data() == l) & (validArea == 1))
    #                    assert (len(self.allowedIndicesPerVolumePerLabel[i][l]) > 0), "ERROR: No label %d could be sampled from the valid area in the GT image %s" % (l, self.gtFilenames[realImageIndex])

                    del validArea

                    i += 1

                print "\nDONE Loading images"

            if self.confTrain['ensureProportionalLabelSamplingPerBatch']:
                self.allowedLabels = [x for x in self.volumesContainingLabel.keys() if len(self.volumesContainingLabel[x]) > 0]

                logging.debug(self.id + " Loaded volumes containing labels:")
                for l in range(len(labels)):
                    logging.debug(self.id + " -- Label index [" + str(l) + "] --> " + str(self.volumesContainingLabel[l]))

            # Set the numSubEpochs variable to 1 if it s genearating batches for validation,
            # since a single batch is required per
            if self.mode == 'validation':
                batchesPerSubepoch = 1
            elif self.mode == 'training':
                # If we are generating batches for training, calculate the number of batches that we need in a subepoch to sample all the segments
                batchesPerSubepoch = self.confTrain['numTrainingSegmentsLoadedOnGpuPerSubep'] // self.confTrain['batchSizeTraining']

            for batch in range(0, batchesPerSubepoch):
                #logging.debug(self.id + " Generating batch: %d" % batch )
                data, gt = self.generateSingleBatch()
                #logging.info("Number of batches in the queue before putting: %d" % self.queue.qsize())
                self.queue.put((data,gt))

            # Unload the files if we are loading new files every subepoch
            if self.loadNewFilesEverySubepoc:
                self.unloadFiles()

""" #################################################################################################################
                                              WholeVolumeBatchGenerator Class
    ################################################################################################################# """

class WholeVolumeBatchGenerator(BatchGenerator):
    """
        Simple batch generator that takes multi-channel 3D images with labels,
        and generates batches consisting on 1 volume and its ground truth.
    """

    def __init__(self, confTrain, confModel, mode = 'training'):
        self.object__ = """
            Initialize a 3D batch generator with a confTrain and a confModel object.
            Modes can be 'training' or 'validation'. Depending on the mode, the batch generator
            will load the training files (channelsTraining, gtLabelsTraining, roiMasksTraining)
            or the validation files (channelsValidation, gtLabelsValidation, roiMasksValidation) from
            the confTrain object.
        """

        BatchGenerator.__init__(self, confTrain, confModel)

        # List of currently loaded channel images (size:numOfCasesLoadedPerSubepochTraining or numOfCasesLoadedPerSubepochValidation X channels)
        self.currentChannelImages = []
        # List of currently loaded ROI images (size:numOfCasesLoadedPerSubepochTraining or numOfCasesLoadedPerSubepochValidation)
        self.currentRois = []
        # List of currently loaded GT images (size:numOfCasesLoadedPerSubepochTraining or numOfCasesLoadedPerSubepochValidation)
        self.currentGt = []

        # Mode: can be training or validation
        self.mode = mode
        if mode == 'training':
            self.id = '[WHOLEVOL BATCHGEN TRAIN]'
        elif mode == 'validation':
            self.id = '[WHOLEVOL BATCHGEN VAL]'

        # ===================================== ABOUT THE IMAGE CHANNELS LIST INDEXING     ================================
        # ===================================== IMPORTANT (READ BEFORE MODIFYING ANYTHING) ================================

        # IMPORTANT: The indices in the lists of channel filenames are inverted with respect to the currently loaded images
        # self.currentChannelImages is [img][channel] while the filenames list self.allChannelsFilenames is [channel][img]

        # ===================================== IMPORTANT (READ BEFORE MODIFYING ANYTHING) ================================

        # List of lists of filenames (size: numChannels x numOfFilenames)
        self.allChannelsFilenames = []
        # List of roiMask filenames (size: numOfFilenames)
        self.roiFilenames = []
        # List of GT filenames (size:numOfFilenames)
        self.gtFilenames = []

        self.numClasses =  len(self.confModel['labels'])

        # Load the filenames from the configuration files and the parameters specific to the mode
        if mode == 'training':
            logging.info('-- Initializing TRAINING Batch generator')
            self.numChannels = len(self.confTrain['channelsTraining'])
            self.numOfCasesLoadedPerEpoch = self.confTrain['numOfCasesLoadedPerEpochWhenTrainingWholeVolume']
            self.loadFilenames(self.confTrain['channelsTraining'], self.confTrain['gtLabelsTraining'],
                          self.confTrain['roiMasksTraining'] if ('roiMasksTraining' in self.confTrain) else None)
        elif mode == 'validation':
            logging.info('-- Initializing VALIDATION Batch generator')
            self.numChannels = len(self.confTrain['channelsValidation'])
            #self.numOfCasesLoadedPerEpoch = self.confTrain['numOfCasesLoadedPerEpochWhenValidatingWholeVolume']
            self.numOfCasesLoadedPerEpoch = 1

            self.loadFilenames(self.confTrain['channelsValidation'], self.confTrain['gtLabelsValidation'],
                               self.confTrain['roiMasksValidation'] if ('roiMasksValidation' in self.confTrain) else None)
        else:
            raise Exception('ERROR: Batch generator mode is not valid. Valid options are training or validation')

        # Check if, given the number of cases loaded per subepoch and the total number of samples, we
        # will need to load new data in every subepoch or not.

        self.loadNewFilesEveryEpoch = len(self.allChannelsFilenames[0]) > self.numOfCasesLoadedPerEpoch
        logging.info(self.id + " Loading new files every subepoch: " + str(self.loadNewFilesEveryEpoch))

    def unloadFiles(self):
        logging.debug(self.id + " Unloading Files")
        for image in self.currentChannelImages:
            for channelImage in image:
                del channelImage

        for image in self.currentGt:
            del image

        for image in self.currentRois:
            del image

        del self.currentChannelImages
        del self.currentGt
        del self.currentRois

        self.currentChannelImages = []
        self.currentGt = []
        self.currentRois = []

    def generateSingleBatch(self, numBatch):
        """
            Creates a batch of segments according to the conf file. It supposes that the images are already
            loaded in self.currentVolumes.

            :return: It returns the data and ground truth of a complete batch as data, gt. These structures are theano-compatible with shape:
                        np.ndarray(shape=(self.confTrain['batchSizeTraining'], self.numChannels, tileSize, tileSize, tileSize), dtype=np.float32)
        """

        if not(self.currentChannelImages == []):
            dims = self.currentChannelImages[numBatch][0].shape

            batch = np.ndarray(shape=(1, self.numChannels, dims[0], dims[1], dims[2]), dtype=np.float32)
            gt =    np.ndarray(shape=(1, self.numClasses, dims[0], dims[1], dims[2]),  dtype=np.float32)

            # For every sample we have to generate
            for channel in range(self.numChannels):
                if self.loadNewFilesEveryEpoch:
                    batch[0, channel, :, :, :] = self.currentChannelImages[numBatch][channel].copy()
                else:
                    batch[0, channel, :, :, :] = self.currentChannelImages[numBatch][channel]

            auxGt = self.currentGt[numBatch]

            # Transform the Hard Segmentation GT to one-hot encoding
            for i in range(self.numClasses):
                gt[0, i, :, :, :] = (auxGt == self.confModel['labels'][i]).astype(np.int16)

            # Return the batch data with its corresponding ground truth
            return batch, gt
        else:
            raise Exception(self.id + " No images loaded in self.currentVolumes." )

    def generateBatchesForOneEpoch(self):
        # Choose the random images that will be sampled in this epoch
        indexCurrentImages = self.rndSequence.sample(xrange(0, len(self.allChannelsFilenames[0])), self.numOfCasesLoadedPerEpoch)

        # Load the files if needed
        if (self.currentEpoch == 0) or ((self.currentEpoch > 0) and self.loadNewFilesEveryEpoch):
            logging.debug(self.id + " Loading images number : %s" % indexCurrentImages )
            # Load the images for the epoch
            i = 0
            for realImageIndex in indexCurrentImages:
                # For every channel
                loadedImageChannels = []
                for channel in range(0, len(self.allChannelsFilenames)):
                    # Load the corresponding image for the corresponding channel and append it to the list of channels
                    # for the current imageIndex
                    loadedImageChannels.append(nib.load(self.allChannelsFilenames[channel][realImageIndex]).get_data())

                    # Check that all the channels have the same dimensions
                    if channel > 0:
                        assert loadedImageChannels[channel].shape == loadedImageChannels[0].shape, self.id + " Data size incompatibility when loading image channels for volume %s" % self.allChannelsFilenames[channel][realImageIndex]

                # Append all the channels of the image to the list
                self.currentChannelImages.append(loadedImageChannels)

                # Load GT for the current image
                gt = nib.load(self.gtFilenames[realImageIndex]).get_data()
                assert gt.shape == loadedImageChannels[0].shape, self.id + " Data size incompatibility when loading GT %s" % self.gtFilenames[realImageIndex]
                self.currentGt.append(gt)

        #TODO FIX IT Generate only batches for the given validation batch size
        for batch in range(0, len(indexCurrentImages)):
            #logging.debug(self.id + " Generating batch: %d" % batch )
            data, gt = self.generateSingleBatch(batch)
            self.queue.put((data,gt))

        # Unload the files if we are loading new files every subpeoc
        if self.loadNewFilesEveryEpoch:
            self.unloadFiles()

""" #################################################################################################################
                                              Main with dummy test
    ################################################################################################################# """
def setupLogger(logFilename):

    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)-10s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=logFilename,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    # Now, we can log to the root logger, or any other logger. First the root...
    print ('Logging to file: %s' % logFilename)
    logging.info('Logging to file: %s' % logFilename)

if __name__ == "__main__":
    setupLogger("../tmp/output.log")
    # Read the conf file
    confModel = {}
    execfile("../conf/simpledmExample_model_CTSamCook.conf", confModel)

    confTrain = {}
    execfile("../conf/simpledmExample_training_CTSamCook_Morelabels.conf", confTrain)

    # Create the batch generator
    batchGen = Simple3DBatchGenerator(confTrain, confModel, mode='training', infiniteLoop=False)

    batchGen.generateBatches()

    for e in range(0, confTrain['numEpochs']):
        print 'Epoch: %d' % e
        for se in range(0, confTrain['numSubepochs']):
            print '- Subepoch: %d' % se
            batchesPerSubepoch = confTrain['numTrainingSegmentsLoadedOnGpuPerSubep'] // confTrain['batchSizeTraining']
            for batch in range(0, batchesPerSubepoch):
                print '--- Waiting for batch %d' % batch
                minibatch = batchGen.getBatch()
                print '--- Processing batch %d' % batch
                #time.sleep(1)
                print '--- Done'

