import numpy as np
import matplotlib.pyplot as plt
import pickle
import seaborn as sbn
import os
import medpy.metric.binary as metricOps
import logging
import pandas as pd

from scipy.stats import wilcoxon

""" #################################################################################################################
                                              Metrics Monitor class
    ################################################################################################################# """

class MetricsMonitor:
    """
        This class implements the functionality needed to evaluate the quality of the predictions
        made by a CNN, and monitor its evolution.
    """
    NUM_SEGMENTATION_STATS = 8
    NUM_METRICS = 5
    NUM_DISTANCE_METRICS = 2
    @staticmethod
    def getHardSegmentationFromTensor(probMap, labels=None):
        """
            Given probMap in the form of a Theano tensor: ( shape = (N, numClasses, H, W, D), float32) or
            ( shape = (numClasses, H, W, D), float32), where the values indicates the probability for every class,
             it returns a hard segmentation per data sample where the value corresponds to the segmentation label with
             the highest probability. The label is sequential from 0 to numClasses-1, unless a labels list is specified.
             If labels is provided, the output labels are linearly mapped: the voxels whose probability is maximum in
                   the c-th channel of the input probMap will be assigned label[c].

            :param probMap: probability maps containing the per-class prediction/
            :param labels: List containing the real label value. If it is provided, then the labels in the output hard segmentation
                   will contain the value provided in 'labels'. The mapping is lineal: the voxels whose probability is maximum in
                   the c-th channel of the input probMap will be assigned label[c].

            :return: if a 5D tensor is provided (N, numClasses, H, W, D), it returns a 4D tensor with shape = (N, H, W, D).
                     if a 4D tensor is provided, it returns a 3D tensor with shape = H, W, D
        """
        if len(probMap.shape) == 5:
            hardSegm = np.argmax(probMap, axis=1).astype(np.int16)
        elif len(probMap.shape) == 4:
            hardSegm = np.argmax(probMap, axis=0).astype(np.int16)
        else:
            return None

        # If labels are provided, replace the values in the matrix by linearly mapping them to the values in the label list.
        if labels is not None:
            finalHardSegm = np.copy(hardSegm)
            for l in range (len(labels)):
                finalHardSegm[hardSegm==l] = labels[l]
            return finalHardSegm
        else:
            return hardSegm

    @staticmethod
    def getOneHotEncodingFromHardSegmentation(hardSegm, labels):
        """
            Given a hard segmentation, it returns the 1-hot encoding.
        """
        dims = hardSegm.shape

        oneHot = np.ndarray(shape=(1, len(labels), dims[-3], dims[-2], dims[-1]), dtype=np.float32)

        # Transform the Hard Segmentation GT to one-hot encoding
        for i, labelValue in enumerate(labels):
            oneHot[0, i, :, :, :] = (hardSegm == labelValue).astype(np.int16)

        return oneHot

    @staticmethod
    def getMetricsForWholeSegmentation(hardSegmPrediction, hardSegmGt, labels, roi=None, calculateDistances = False):
        """
            :param hardSegmPrediction: Hard segmentation corresponding to the single case to evaluate (3D: H, W, D)
                                       The voxel values indicate the label
            :param hardSegmGt: GT tensor: Hard segmentation corresponding to the GT (3D: H, W, D)
                                  The voxel values indicate the label
            :param labels: List containing the labels for which we will evaluate the metrics
            :param roi: [Optional] If provided, the metric is only calculated in the voxels where the ROI is 1.
            :param calculateDistances: If true, the method will also return Hausdorff and Contour Mean Distances.

            See https://www.researchgate.net/post/How_do_I_measure_specificity_and_sensitivity_in_a_multiple_class_classification_problem [accessed Jan 19, 2017].
            for more info about how to calculate these metrics in a multiclass scenario.

            :return ndarray containing the metrics per label in the form metrics[LABEL][METRIC]. The metrics are:
                    0 = accuracy
                    1 = sensitivity
                    2 = specificity
                    3 = dice
                    4 = jaccard
                    5 = hausdorff [OPTIONAL, If calculateDistances]
                    6 = contour mean distance [OPTIONAL, If calculateDistances]

        """
        numLabels = len(labels)

        # Calculate accuracy, sensitivity, specificity and DICE per label, as specified
        # here: https://www.researchgate.net/post/How_do_I_measure_specificity_and_sensitivity_in_a_multiple_class_classification_problem
        if calculateDistances:
            metrics = np.zeros(shape=(1, numLabels, MetricsMonitor.NUM_METRICS + MetricsMonitor.NUM_DISTANCE_METRICS))
        else:
            metrics = np.zeros(shape=(1, numLabels, MetricsMonitor.NUM_METRICS))

        for i, l in enumerate(labels):
            #logging.info("Label %s" % str(l))
            hardSegmGtEql = (hardSegmGt == l)
            hardSegmGtNotEql = (hardSegmGt != l)
            hardSegmPredictionEql = (hardSegmPrediction == l)
            hardSegmPredictionNotEql = (hardSegmPrediction != l)

            if roi is None:
                # "TP of C1" is all C1 instances that are classified as C1.
                tp = np.sum(((hardSegmGtEql) & (hardSegmPredictionEql)).astype(dtype=np.int))
                # "TN of C1" is all non-C1 instances that are not classified as C1.
                tn = np.sum(((hardSegmGtNotEql) & (hardSegmPredictionNotEql)).astype(dtype=np.int))
                # "FN of C1" is all C1 instances that are not classified as C1.
                fn = np.sum(((hardSegmGtEql) & (hardSegmPredictionNotEql)).astype(dtype=np.int))
                # "FP of C1" is all non-C1 instances that are classified as C1.
                fp = np.sum(((hardSegmGtNotEql) & (hardSegmPredictionEql)).astype(dtype=np.int))
            else:
                # "TP of C1" is all C1 instances that are classified as C1.
                tp = np.sum(((hardSegmGtEql) & (hardSegmPredictionEql) & (roi == 1)).astype(dtype=np.int))
                # "TN of C1" is all non-C1 instances that are not classified as C1.
                tn = np.sum(((hardSegmGtNotEql) & (hardSegmPredictionNotEql) & (roi == 1)).astype(dtype=np.int))
                # "FN of C1" is all C1 instances that are not classified as C1.
                fn = np.sum(((hardSegmGtEql) & (hardSegmPredictionNotEql) & (roi == 1)).astype(dtype=np.int))
                # "FP of C1" is all non-C1 instances that are classified as C1.
                fp = np.sum(((hardSegmGtNotEql) & (hardSegmPredictionEql) & (roi == 1)).astype(dtype=np.int))

            #logging.info("Metrics")
            # Accuracy
            metrics[0, i, 0] = float(tp + tn) / float(tp + tn + fn + fp)
            # Sensitivity
            # If tp + fn == 0, then it means that there is no label i. Therefore, in this case, sensitivity = 1)
            # This condition is here to avoid dividing by 0 when there is no label i in the GT.
            if (tp + fn) != 0:
                metrics[0, i, 1] = float(tp) / float(tp + fn)
            else:
                metrics[0, i, 1] = np.nan
                metrics[0, i, 3] = np.nan
                metrics[0, i, 4] = np.nan
            # Specificity
            metrics[0, i, 2] = float(tn) / float(tn + fp)

            # DICE
            # If tp + fn + fp == 0, then DICE is nan because in particular it means that tp + fn = 0, i.e. there is no label i.
            if ((tp + fn + fp) != 0) and ((tp + fn) != 0):
                metrics[0, i, 3] = float(2 * tp) / float(2 * tp + fn + fp)
                # Jaccard = DICE / (2 - DICE)
                metrics[0, i, 4] = metrics[0, i, 3] / (2 - metrics[0, i, 3])

            if calculateDistances:
                # Hausdorff distance

                hgt = (hardSegmGtEql).astype(int)
                hsp = (hardSegmPredictionEql).astype(int)
                if np.max(hgt) == 0 or np.max(hsp) == 0:
                    logging.info("Setting HD to Nan")
                    metrics[0, i, 5] = np.nan
                    logging.info("Setting ASSD to Nan..")
                    metrics[0, i, 6] = np.nan
                else:
                    logging.info("Calculating HD ..")
                    metrics[0, i, 5] = metricOps.hd(hgt, hsp)
                    logging.info("Calculating ASSD ..")
                    metrics[0, i, 6] = metricOps.assd(hgt, hsp)

        return metrics

    @staticmethod
    def getPredictionStatsWithRespectToROI(hardSegmPrediction, hardSegmGt, labels, roi):
        """
            :param hardSegmPrediction: Hard segmentation corresponding to the single case to evaluate (3D: H, W, D)
                                       The voxel values indicate the label
            :param hardSegmGt: GT tensor: Hard segmentation corresponding to the GT (3D: H, W, D)
                                  The voxel values indicate the label
            :param labels: List containing the labels for which we will evaluate the metrics
            :param roi: ROI

            :return ndarray containing the metrics per label in the form metrics[LABEL][METRIC]. The metrics are:
                    0 = avg distance to ROI border for TP
                    1 = std distance to ROI border for TP
                    2 = avg distance to ROI border for TN
                    3 = std distance to ROI border for TN
                    4 = avg distance to ROI border for FN
                    5 = std distance to ROI border for FN
                    6 = avg distance to ROI border for FP
                    7 = std distance to ROI border for FP
        """
        numLabels = len(labels)

        metrics = np.zeros(shape=(1, numLabels, MetricsMonitor.NUM_SEGMENTATION_STATS))

        for i, l in enumerate(labels):
            #logging.info("Label %s" % str(l))
            hardSegmGtEql = (hardSegmGt == l)
            hardSegmGtNotEql = (hardSegmGt != l)
            hardSegmPredictionEql = (hardSegmPrediction == l)
            hardSegmPredictionNotEql = (hardSegmPrediction != l)

            # "TP of C1" is all C1 instances that are classified as C1.
            tp = (hardSegmGtEql) & (hardSegmPredictionEql) & (roi == 1)
            # "TN of C1" is all non-C1 instances that are not classified as C1.
            tn = (hardSegmGtNotEql) & (hardSegmPredictionNotEql) & (roi == 1)
            # "FN of C1" is all C1 instances that are not classified as C1.
            fn = (hardSegmGtEql) & (hardSegmPredictionNotEql) & (roi == 1)
            # "FP of C1" is all non-C1 instances that are classified as C1.
            fp = (hardSegmGtNotEql) & (hardSegmPredictionEql) & (roi == 1)
            #fp = np.sum(((hardSegmGtNotEql) & (hardSegmPredictionEql) & (roi == 1)).astype(dtype=np.int))

            # generate the
            roiSitkImage = sitk.GetImageFromArray(roi)
            contour = sitk.BinaryContour(roiSitkImage)
            dst = sitk.DanielssonDistanceMap(contour)
            dataDst = sitk.GetArrayFromImage(dst)

            metrics[0, i, 0] = dataDst[tp].mean()
            metrics[0, i, 1] = dataDst[tp].std()

            metrics[0, i, 2] = dataDst[tn].mean()
            metrics[0, i, 3] = dataDst[tn].std()

            metrics[0, i, 4] = dataDst[fn].mean()
            metrics[0, i, 5] = dataDst[fn].std()

            metrics[0, i, 6] = dataDst[fp].mean()
            metrics[0, i, 7] = dataDst[fp].std()

        return metrics


    @staticmethod
    def getBatchMetrics(prediction, gt):
        """
            :param prediction: Prediction tensor (5D: N, numClasses, H, W, D)
            :param gt: GT tensor (5D: N, numClasses, H, W, D)

            See https://www.researchgate.net/post/How_do_I_measure_specificity_and_sensitivity_in_a_multiple_class_classification_problem [accessed Jan 19, 2017].
            for more info about how to calculate these metrics in a multi-class scenario.

            :return ndarray containing the metrics per label in the form metrics[LABEL][METRIC]. The metrics are:
                    0 = accuracy
                    1 = sensitivity
                    2 = specificity
                    3 = dice
                    4 = jaccard

        """

        hardSegmPrediction = MetricsMonitor.getHardSegmentationFromTensor(prediction)
        hardSegmGt = MetricsMonitor.getHardSegmentationFromTensor(gt)

        numLabels = prediction.shape[1]

        # Calculate accuracy, sensitivity, specificity and DICE per label, as specified
        # here: https://www.researchgate.net/post/How_do_I_measure_specificity_and_sensitivity_in_a_multiple_class_classification_problem

        metrics = np.zeros(shape=(1, numLabels, MetricsMonitor.NUM_METRICS))

        for i in range(numLabels):
            # "TP of C1" is all C1 instances that are classified as C1.
            tp = np.sum(((hardSegmGt == i) & (hardSegmPrediction == i)).astype(dtype=np.int))
            # "TN of C1" is all non-C1 instances that are not classified as C1.
            tn = np.sum(((hardSegmGt != i) & (hardSegmPrediction != i)).astype(dtype=np.int))
            # "FN of C1" is all C1 instances that are not classified as C1.
            fn = np.sum(((hardSegmGt == i) & (hardSegmPrediction != i)).astype(dtype=np.int))
            # "FP of C1" is all non-C1 instances that are classified as C1.
            fp = np.sum(((hardSegmGt != i) & (hardSegmPrediction == i)).astype(dtype=np.int))

            # Accuracy
            metrics[0, i, 0] = float(tp + tn) / float(tp + tn + fn + fp)

            # Sensitivity
            # If tp + fn == 0, then it means that there is no label i.)
            # This condition is here to avoid dividing by 0 when there is no label i in the GT.
            if (tp + fn) != 0:
                metrics[0, i, 1] = float(tp) / float(tp + fn)
            else:
                metrics[0, i, 1] = np.nan

            # Specificity
            # If tn + fp == 0, then it means that there is no other label than i. )
            # This condition is here to avoid dividing by 0 when there is no label i in the GT.
            if (tn + fp) != 0:
                metrics[0, i, 2] = float(tn) / float(tn + fp)
            else:
                metrics[0, i, 2] = np.nan

            # DICE
            # If tp + fn + fp == 0, then it means that there is no label i. Therefore, in this case, DICE = 1)
            # This condition is here to avoid dividing by 0 when there is no label i in the GT.
            if (tp + fn + fp) != 0:
                metrics[0, i, 3] = float(2 * tp) / float(2 * tp + fn + fp)
            else:
                metrics[0, i, 3] = np.nan

            # Jaccard = DICE / (2 - DICE)
            metrics[0, i, 4] = metrics[0, i, 3] / (2 - metrics[0, i, 3])

        return metrics

    @staticmethod
    def runningMeanConv(x, N):
        """
            Code from: http://stackoverflow.com/questions/13728392/moving-average-or-running-mean
        """
        return np.convolve(x, np.ones((N,))/N, mode='valid')

    @staticmethod
    def plotComparativeStatsForTwoModels(statsModel1, statsModel2, confModel, legMethod1, legMethod2, metric=3):
        """
           Plots, in the same figure, the value of the given "metric" for both models.
        """

        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        labels =  confModel["labels"]
        index = np.arange(len(labels))
        bar_width = 0.35
        opacity = 0.4

        # Fetch data
        means1 = statsModel1['metricsValidationFullVolumes'][-1, :, metric] if 'metricsValidationFullVolumes' in statsModel1 else statsModel1['metricsValidationFullVolumesAvg'][-1, :, metric]

        means2 = statsModel2['metricsValidationFullVolumes'][-1, :, metric] if 'metricsValidationFullVolumes' in statsModel2 else statsModel2['metricsValidationFullVolumesAvg'][-1, :, metric]

        # Plot
        fig, ax = plt.subplots()

        rects1 = plt.bar(index, means1, bar_width,
                         alpha=opacity,
                         color='b',
                         #yerr=std_men,
                         #error_kw=error_config,
                         label=legMethod1)

        rects2 = plt.bar(index + bar_width, means2, bar_width,
                         alpha=opacity,
                         color='r',
                         #yerr=std_women,
                         #error_kw=error_config,
                         label=legMethod2)

        plt.xlabel('Label')
        if metric == 3:
            plt.ylabel('DSC')
        else:
            plt.ylabel('Metric')

        plt.title('Scores by label and method')
        plt.xticks(index + bar_width / 2, tuple([str(e) for e in labels]))
        plt.legend()

        plt.tight_layout()
        plt.show()

    @staticmethod
    def plotComparativeStatsForNModels(stats, confModel, legends, metric=3):
        """
           Plots, in the same figure, the value of the given "metric" for all the models in the list models.
        """

        numModels = len(stats)

        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        labels =  confModel["labels"]
        index = np.arange(len(labels))
        bar_width = 0.8/float(numModels)
        opacity = 1

        # Fetch data
        means = []
        stds = []
        case = -1

        for i in range(numModels):
            means.append(stats[i]['metricsValidationFullVolumes'][case, :, metric] if 'metricsValidationFullVolumes' in stats[i] else stats[i]['metricsValidationFullVolumesAvg'][case, :, metric])

            if 'metricsValidationFullVolumesStd' in stats[i]:
                stds.append(stats[i]['metricsValidationFullVolumesStd'][case, :, metric])
            else:
                stds = None
            print "Means Method %i: %f" % (i,  np.nanmean(means[i]))


        # Plot
        fig, ax = plt.subplots()
        rects = []
        for i in range(numModels):
            rects.append(plt.bar(index + bar_width * i, means[i], bar_width,
                         alpha=opacity,
                         color=colors[i],
                         yerr=stds[i],
                         #error_kw=error_config,
                         label=legends[i]))

        plt.xlabel('Label')
        if metric == 3:
            plt.ylabel('DSC')
        else:
            plt.ylabel('Metric')

        #plt.ylim(0, 50)

        plt.title('Scores by label and method')
        plt.xticks(index + bar_width / numModels, tuple([str(e) for e in labels]))
        plt.legend()

        plt.tight_layout()
        plt.show()

    @staticmethod
    def plotComparativeStatsForNModelsMultifold(stats, confModel, legends = None, metric=3):
        """
           Plots, in the same figure, the value of the given "metric" for all the models in the list models.
           It uses boxplots instead of bar plots, therefore the stats of every model are expected to include a field 'allMetrics' of type ndarray
           with shape (NUM_SAMPLES, NUM_LABELS, NUM_METRICS).

           stats is a list of lists of matrices indexed as:

                stats[NUM_FOLD][NUM_MODEL][NUM_SAMPLES, NUM_LABELS, NUM_METRICS]

        """
        if legends is None:
            legends = [ x for x in range(len(stats)) ]

        numModels = len(stats)

        # Configuration
        sbn.set_style("whitegrid")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        labels =  confModel["labels"]

        # Fetch data
        #stats[NUM_FOLD][NUM_MODEL]['allMetrics'][NUM_SAMPLES, NUM_LABELS, NUM_METRICS]

        data = pd.DataFrame(data=None, index=None, columns = ['DSC','Label','Fold','Model'])

        numFolds = len(stats)
        numModels = len(stats[0])
        numLabels = stats[0][0]['allMetrics'].shape[1]

        #print "Samples in method 0 : %i" % stats[][0]['allMetrics'].shape[0]
        #print "Samples in method 1 : %i" % stats[2][0]['allMetrics'].shape[0]
        print "Legends: " + str(legends)
        for fold in range(numFolds):
            for model in range(numModels):
                for sample in range(stats[fold][0]['allMetrics'].shape[0]):
                    for label in range(1, numLabels):
#                        print "Fold %i" % fold
#                        print "model %i" % model
#                        print "sample %i" % sample
#                        print "label %i" % label

                        data.loc[data.shape[0]] = [ stats[fold][model]['allMetrics'][sample, label, metric], label, fold, legends[model]]

        matPValues = np.zeros(shape=(numLabels-1,numModels,numModels))
        for l in range(1, numLabels):
            print "===== Stats for Label %i =======" % l
            for model0 in range(numModels):
                for model1 in range(numModels):
                    mod0 = data[(data['Model'] == legends[model0]) & (data['Label'] == l)]['DSC'].as_matrix()
                    mod0 = mod0[~np.isnan(mod0)]

                    mod1 = data[(data['Model'] == legends[model1]) & (data['Label'] == l)]['DSC'].as_matrix()
                    mod1 = mod1[~np.isnan(mod1)]

                    print " ====== Method \"%s\" vs Method \"%s\" ==========" % (str(legends[model0]), str(legends[model1]))
                    print "---\t  | ---\t | ---\t  | ---"
                    print "%s | %s  | %s  " % ("Median", str(np.median(mod0)), str(np.median(mod1)))  # , str(mod2.mean()))
                    print "%s | %s  | %s  " % ("Mean", str(mod0.mean()), str(mod1.mean())) #, str(mod2.mean()))
                    print "%s | %s  | %s  " % ("Std", str(mod0.std()), str(mod1.std())) #, str(mod1.std()))
                    matPValues[l-1,model0, model1] = wilcoxon(mod0, mod1).pvalue

                    print "Wilcoxon test (p-value): %.5f \n" % (matPValues[l-1,model0, model1])

        matPValuesAll = np.zeros(shape=(numModels,numModels))

        print "\n ===== STATS FOR ALL LABELS ======= \n"

        for model0 in range(numModels):
            for model1 in range(numModels):
                mod0 = data[(data['Model'] == legends[model0]) & (data['Label'] != 0) ]['DSC'].as_matrix()
                mod0 = mod0[~np.isnan(mod0)]

                mod1 = data[(data['Model'] == legends[model1]) & (data['Label'] != 0)]['DSC'].as_matrix()
                mod1 = mod1[~np.isnan(mod1)]

                print " ====== Method \"%s\" vs Method \"%s\" ==========" % (str(legends[model0]), str(legends[model1]))
                print "---\t  | ---\t | ---\t  | ---"
                print "%s | %s  | %s  " % ("Median", str(np.median(mod0)), str(np.median(mod1)))  # , str(mod2.mean()))
                print "%s | %s  | %s  " % ("Mean", str(mod0.mean()), str(mod1.mean())) #, str(mod2.mean()))
                print "%s | %s  | %s  " % ("Std", str(mod0.std()), str(mod1.std())) #, str(mod1.std()))
                matPValuesAll[model0, model1] = wilcoxon(mod0, mod1).pvalue

                print "Wilcoxon test (p-value): %.10f \n" % (matPValuesAll[model0, model1])


        matPValues[np.isnan(matPValues)] = 0
        matPValuesAll[np.isnan(matPValuesAll)] = 0

        print "\n ===== P-values between every pair of methods ======= \n"

        print matPValuesAll

        sbn.plt.figure()
        # Plot
        sbn.boxplot(x="Label", y="DSC", hue="Model", data=data, palette="PRGn", notch=True, showmeans=True)
        sbn.despine(offset=10, trim=True)

        sbn.plt.show()

        sbn.plt.figure()
        # Plot
        ax = sbn.boxplot(x="Model", y="DSC", data=data, palette="PRGn", notch=True, showmeans=True)
        sbn.despine(offset=10, trim=True)

        medians = data.groupby(['Model'])['DSC'].median().values
        median_labels = [str(np.round(s, 2)) for s in medians]

        pos = range(len(medians))
        for tick, label in zip(pos, ax.get_xticklabels()):
            sbn.plt.text(pos[tick], medians[tick] + 0.5, median_labels[tick],
                    horizontalalignment='center', size='x-small', color='w', weight='semibold')

        sbn.plt.show()

    @staticmethod
    def plotComparativeAvgValidationStatsMultiFolds(stats, legends, metric=3):
        """
            stats[NUM_FOLD][NUM_MODEL]
        """
        numModels = len(stats[0])
        numFolds  = len(stats)
        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        # Plot
        plt.figure()

        colors = ["red", "blue", "green"]

        for i in range(numModels):
            print "processing model " + str(i)
            #accum = np.zeros(shape=(stats[i]['metricsValidationFullVolumesAvg'].shape[0], 1, 1))
            #for j in range(1, len(confModel['labels'])):
            #    accum = accum + stats[i]['metricsValidationFullVolumesAvg'][:, j, metric]
            # print np.squeeze(accum / float(len(confModel['labels'])-1))
            ex = stats[0][i]['metricsValidationFullVolumesAvg'][:,1:, metric].mean(axis=1)
            mins = np.ones(ex.shape) * 10000000
            maxs = np.ones(ex.shape) * -10000000
            accumMeans = np.zeros(ex.shape)

            for fold in range(numFolds):
                means = stats[fold][i]['metricsValidationFullVolumesAvg'][:, 1:, metric].mean(axis=1)
                mins = np.minimum(mins, means)
                maxs = np.maximum(maxs, means)
                accumMeans += means

            accumMeans /= numFolds

            print accumMeans
            print "mins: " + str(mins)
            print "maxs: " + str(maxs)

            plt.plot(np.squeeze(accumMeans), label = legends[i], color=colors[i])
            plt.fill_between(list(range(len(accumMeans))), mins, maxs, color=colors[i], alpha=0.3)

        plt.title("(Val) DICE Full Volume")
        plt.xlabel("Epochs (x5)")
        plt.legend(bbox_to_anchor = (1, 1), loc = 2, ncol = 1, title = "Labels")
        plt.show()

    @staticmethod
    def plotComparativeAvgValidationStats(stats, confModel, legends, metric=3):
        """
        """
        numModels = len(stats)

        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        # Plot
        plt.figure()

        for i in range(numModels):
            #accum = np.zeros(shape=(stats[i]['metricsValidationFullVolumesAvg'].shape[0], 1, 1))
            #for j in range(1, len(confModel['labels'])):
            #    accum = accum + stats[i]['metricsValidationFullVolumesAvg'][:, j, metric]
            # print np.squeeze(accum / float(len(confModel['labels'])-1))
            means = stats[i]['metricsValidationFullVolumesAvg'][:,1:, metric].mean(axis=1)

            print means

            plt.plot(np.squeeze(means), label = legends[i])

        plt.title("(Val) DICE Full Volume")
        plt.xlabel("Epochs (x5)")
        plt.legend(bbox_to_anchor = (1, 1), loc = 2, ncol = 1, title = "Labels")
        plt.show()


    @staticmethod
    def plotComparativeAdvLosses(stats, confModel, legends, labelValue=3, rmWindow = 5):
        """
           Plots, in the same figure, loss of different segmenters
        """
        labelIndex = confModel["labels"].index(labelValue)

        numModels = len(stats)

        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        labels =  confModel["labels"]
        index = np.arange(len(labels))
        bar_width = 0.8/float(numModels)
        opacity = 1

        # Plot
        plt.figure()
        xlabel = "Sub Epoch"

        numRows = 2
        numCols = 6
        plotsLoss = []

        for i in range(numModels):
            loss = stats[i]['indepTrainingLossByBatchAdv']
            metricsTraining = stats[i]['metricsTrainingBySubepochAdv']

            # =================================================
            # ============= TRAINING STATS ====================

            plt.subplot(numRows, numCols, 1)
            #plotsLoss.append(plt.plot(np.array(loss)))
            plotsLoss.append(plt.plot(MetricsMonitor.runningMeanConv(np.array(loss), rmWindow)))

            plt.title("(Train) Loss")
            plt.xlabel("Batch")

            plt.subplot(numRows, numCols, 2)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 0], rmWindow))
            plt.title("(Train) Accuracy")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 3)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 1], rmWindow))
            plt.title("(Train) Sensitivity (TP/(TP+FN))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 4)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 2], rmWindow))
            plt.title("(Train) Specificity (TN/(TN+FP))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 5)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 3], rmWindow))
            plt.title("(Train) Dice")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 6)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 4], rmWindow), label=legends[i])
            plt.title("(Train) Jaccard")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

        plt.legend()
        plt.show()

    @staticmethod
    def plotComparativeSegmenterLosses(stats, confModel, labelValue=-1, legends = [], rmWindow = 20):
        """
           Plots, in the same figure, loss of different segmenters
        """
        if not (labelValue == -1):
            labelIndex = confModel["labels"].index(labelValue)

        numModels = len(stats)

        if legends == []:
            legends = ["Model %s" % str(i) for i in range(numModels)]

        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        labels = confModel["labels"]
        index = np.arange(len(labels))
        bar_width = 0.8/float(numModels)
        opacity = 1

        # Plot
        plt.figure()
        xlabel = "Sub Epoch"

        numRows = 2
        numCols = 6
        plotsLoss = []

        for i in range(numModels):
            loss = stats[i]['indepTrainingLossByBatch']
            metricsTraining = stats[i]['metricsTrainingBySubepoch']
            metricsValidation = stats[i]['metricsValidationBySubepoch']

            # =================================================
            # ============= TRAINING STATS ====================

            plt.subplot(numRows, numCols, 1)
            #plotsLoss.append(plt.plot(np.array(loss)))
            plotsLoss.append(plt.plot(MetricsMonitor.runningMeanConv(np.array(loss), rmWindow)))

            plt.title("(Train) Loss")
            plt.xlabel("Batch")

            plt.subplot(numRows, numCols, 2)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 0], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 0].mean(axis=1), rmWindow))
            plt.title("(Train) Accuracy")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 3)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 1], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 1].mean(axis=1), rmWindow))
            plt.title("(Train) Sensitivity (TP/(TP+FN))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 4)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 2], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 2].mean(axis=1), rmWindow))
            plt.title("(Train) Specificity (TN/(TN+FP))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 5)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 3], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 3].mean(axis=1), rmWindow))
            plt.title("(Train) Dice")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 6)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 4], rmWindow), label = legends[i])
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 4].mean(axis=1), rmWindow), label = legends[i])
            plt.title("(Train) Jaccard")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.legend(bbox_to_anchor=(0, 0.3), loc=2,
                       ncol=1, title="Legend")

            # =================================================
            # ============= VALIDATION STATS ====================

            plt.subplot(numRows, numCols, 7)

            if not (labelValue == -1):
                plt.plot(stats[i]['metricsValidationFullVolumesAvg'][:, labelIndex, 3])
            else:
                plt.plot(stats[i]['metricsValidationFullVolumesAvg'][:, 1:, 3].mean(axis=1))

            plt.title("(Val) DICE Full Volume")
            plt.xlabel("Every N Epochs")
            plt.ylim(0, 1)

            if metricsValidation[:, 0, 0].shape[0] != 0:
                plt.subplot(numRows, numCols, 8)
                if not (labelValue == -1):
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 0], rmWindow))
                else:
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 0].mean(axis=1), rmWindow))
                plt.title("(Val) Accuracy")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 9)
                if not (labelValue == -1):
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 1], rmWindow))
                else:
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 1].mean(axis=1), rmWindow))
                plt.title("(Val) Sensitivity (TP/(TP+FN))")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 10)
                if not (labelValue == -1):
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 2], rmWindow))
                else:
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 2].mean(axis=1), rmWindow))
                plt.title("(Val) Specificity (TN/(TN+FP))")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 11)
                if not (labelValue == -1):
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 3], rmWindow))
                else:
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 3].mean(axis=1), rmWindow))
                plt.title("(Val) Dice")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 12)
                if not (labelValue == -1):
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 4], rmWindow))
                else:
                    plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 4].mean(axis=1), rmWindow))
                plt.title("(Val) Jaccard")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

        plt.xlabel('Label')
        if not (labelValue == -1):
            plt.suptitle("Metrics for label index %d (value=%d) (shown with a running mean window = %d" % (labelIndex, labelValue, rmWindow), size=16)
        else:
            plt.suptitle("Avg Metrics for all the labels, except background (0) (shown with a running mean window = %d)" % rmWindow, size=16)

        #plt.title('Training metrics for different models')
        plt.show()


    @staticmethod
    def plotComparativeSegmenterLossesOriginal(stats, confModel, legends, labelValue=1):
        """
           Plots, in the same figure, loss of different segmenters
        """
        labelIndex = confModel["labels"].index(labelValue)

        numModels = len(stats)

        # Configuration
        sbn.set_style("white")
        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        colors = sbn.color_palette("hls", numModels)

        labels =  confModel["labels"]
        index = np.arange(len(labels))
        bar_width = 0.8/float(numModels)
        opacity = 1

        # Plot
        plt.figure()
        xlabel = "Sub Epoch"

        numRows = 2
        numCols = 6
        plotsLoss = []

        for i in range(numModels):
            loss = stats[i]['indepTrainingLossByBatch']
            metricsTraining = stats[i]['metricsTrainingBySubepoch']
            metricsValidation = stats[i]['metricsValidationBySubepoch']

            rmWindow = 20
            # =================================================
            # ============= TRAINING STATS ====================

            plt.subplot(numRows, numCols, 1)
            #plotsLoss.append(plt.plot(np.array(loss)))
            plotsLoss.append(plt.plot(MetricsMonitor.runningMeanConv(np.array(loss), rmWindow)))

            plt.title("(Train) Loss")
            plt.xlabel("Batch")

            plt.subplot(numRows, numCols, 2)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 0], rmWindow))
            plt.title("(Train) Accuracy")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 3)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 1], rmWindow))
            plt.title("(Train) Sensitivity (TP/(TP+FN))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 4)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 2], rmWindow))
            plt.title("(Train) Specificity (TN/(TN+FP))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 5)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 3], rmWindow))
            plt.title("(Train) Dice")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 6)
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 4], rmWindow), label = legends[i])
            plt.title("(Train) Jaccard")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.legend(bbox_to_anchor=(0, 0.3), loc=2,
                       ncol=1, title="Legend")

            # =================================================
            # ============= VALIDATION STATS ====================

            plt.subplot(numRows, numCols, 7)
            plt.plot(stats[i]['metricsValidationFullVolumesAvg'][:, labelIndex, 3])
            plt.title("(Val) DICE Full Volume")
            plt.xlabel("Every N Epochs")
            plt.ylim(0, 1)

            if (metricsValidation[:, labelIndex, 0].shape[0] != 0):
                plt.subplot(numRows, numCols, 8)
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 0], rmWindow))
                plt.title("(Val) Accuracy")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 9)
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 1], rmWindow))
                plt.title("(Val) Sensitivity (TP/(TP+FN))")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 10)
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 2], rmWindow))
                plt.title("(Val) Specificity (TN/(TN+FP))")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 11)
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 3], rmWindow))
                plt.title("(Val) Dice")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)

                plt.subplot(numRows, numCols, 12)
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 4], rmWindow))
                plt.title("(Val) Jaccard")
                plt.xlabel(xlabel)
                plt.ylim(0, 1)



        plt.xlabel('Label')
        plt.suptitle("Metrics for label index %d (value=%d) (shown with a running mean window = %d" % (labelIndex, labelValue, rmWindow), size=16)

        #plt.title('Training metrics for different models')
        plt.show()


    @staticmethod
    def plotAllLabelsFullVolVal(stats, confModel):
        sbn.set_style("white")

        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)
        xlabel = "Epoch"
        plt.figure()

        metricsValidation = stats['metricsValidationBySubepoch']

        for i in range(0, len(confModel['labels'])):
            plt.plot(stats['metricsValidationFullVolumesAvg'][:, i, 3], label="Label " + str(confModel['labels'][i]))

        plt.ylim(0, 1)
        plt.title("(Val) DICE Full Volume")
        plt.xlabel("Every N Epochs")
        plt.legend(bbox_to_anchor = (1, 1), loc = 2,
                                          ncol = 1, title = "Labels")
        plt.show()


    @staticmethod
    def plotStats(stats, confModel, labelValue = -1, rmWindow = 20):
        """
           Plots the stats (metrics) of a training session.

            If labelValue == -1, then it shows the average of all the labels (except background)
        """
        if not (labelValue == -1):
            labelIndex = confModel["labels"].index(labelValue)

        sbn.set_style("white")

        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        metricsTraining = stats['metricsTrainingBySubepoch']
        numRows = 2
        numCols = MetricsMonitor.NUM_METRICS + 1

        xlabel = "Sub Epoch"
        plt.figure()

        # =================================================
        # ============= TRAINING STATS ====================
        plt.subplot(numRows, numCols,1)
        #plt.plot(MetricsMonitor.runningMeanConv(np.array(stats['indepTrainingLossByBatch']), rmWindow))
        plt.plot(MetricsMonitor.runningMeanConv(np.array(stats['trainingLossByBatch']), rmWindow))
        plt.title("(Train) Loss")
        plt.xlabel("Batch")

        plt.subplot(numRows, numCols, 2)
        if not (labelValue == -1):
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 0], rmWindow))
        else:
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 0].mean(axis=1), rmWindow))
        plt.title("(Train) Accuracy")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 3)
        if not (labelValue == -1):
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 1], rmWindow))
        else:
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 1].mean(axis=1), rmWindow))

        plt.title("(Train) Sensitivity (TP/(TP+FN))")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 4)

        if not (labelValue == -1):
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 2], rmWindow))
        else:
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 2].mean(axis=1), rmWindow))

        plt.title("(Train) Specificity (TN/(TN+FP))")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 5)
        if not (labelValue == -1):
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 3], rmWindow))
        else:
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 3].mean(axis=1), rmWindow))

        plt.title("(Train) Dice")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 6)
        if not (labelValue == -1):
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 4], rmWindow))
        else:
            plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, 1:, 4].mean(axis=1), rmWindow))
        plt.title("(Train) Jaccard")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        # =================================================
        # ============= VALIDATION STATS ====================
        metricsValidation = stats['metricsValidationBySubepoch']

        plt.subplot(numRows, numCols, 7)
        if not (labelValue == -1):
            plt.plot(stats['metricsValidationFullVolumesAvg'][:, labelIndex, 3])
        else:
            plt.plot(stats['metricsValidationFullVolumesAvg'][:, 1:, 3].mean(axis=1))

        plt.title("(Val) DICE Full Volume")
        plt.xlabel("Every N Epochs")
        plt.ylim(0, 1)

        if metricsValidation[:, 0, 0].shape[0] != 0:
            plt.subplot(numRows, numCols, 8)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 0], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 0].mean(axis=1), rmWindow))

            plt.title("(Val) Accuracy")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 9)
            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 1], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 1].mean(axis=1), rmWindow))

            plt.title("(Val) Sensitivity (TP/(TP+FN))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 10)

            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 2], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 2].mean(axis=1), rmWindow))

            plt.title("(Val) Specificity (TN/(TN+FP))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 11)

            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 3], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 3].mean(axis=1), rmWindow))

            plt.title("(Val) Dice")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 12)

            if not (labelValue == -1):
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 4], rmWindow))
            else:
                plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, 1:, 4].mean(axis=1), rmWindow))

            plt.title("(Val) Jaccard")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

        if not (labelValue == -1):
            plt.suptitle("Metrics for label index %d (value=%d) (shown with a running mean window = %d" % (labelIndex, labelValue, rmWindow), size=16)
        else:
            plt.suptitle("Avg Metrics for all the labels, except background (0) (shown with a running mean window = %d)" % rmWindow, size=16)
        plt.show()

    @staticmethod
    def plotAdvStats(stats, confModel, labelValue = 1, rmWindow = 20):
        """
           Plots the stats (metrics) of a training session.
        """
        labelIndex = labelValue
        sbn.set_style("white")

        paper_rc = {'lines.linewidth': 1.2}
        sbn.set_context("paper", rc=paper_rc)

        metricsTraining = stats['metricsTrainingBySubepochAdv']
        numRows = 2
        numCols = MetricsMonitor.NUM_METRICS + 1

        xlabel = "Sub Epoch"
        plt.figure()

        # =================================================
        # ============= TRAINING STATS ====================
        plt.subplot(numRows, numCols,1)
        plt.plot(MetricsMonitor.runningMeanConv(np.array(stats['indepTrainingLossByBatchAdv']), rmWindow))
        #plt.plot(np.array(stats['indepTrainingLossByBatchAdv']))
        plt.title("(Train) Loss")
        plt.xlabel("Batch")

        print MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 0], rmWindow)

        plt.subplot(numRows, numCols, 2)
        plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 0], rmWindow))
        plt.title("(Train) Accuracy")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 3)
        plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 1], rmWindow))
        plt.title("(Train) Sensitivity (TP/(TP+FN))")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 4)
        plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 2], rmWindow))
        plt.title("(Train) Specificity (TN/(TN+FP))")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 5)
        plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 3], rmWindow))
        plt.title("(Train) Dice")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        plt.subplot(numRows, numCols, 6)
        plt.plot(MetricsMonitor.runningMeanConv(metricsTraining[:, labelIndex, 4], rmWindow))
        plt.title("(Train) Jaccard")
        plt.xlabel(xlabel)
        plt.ylim(0, 1)

        # =================================================
        # ============= TRAINING STATS ====================
        metricsValidation = stats['metricsValidationBySubepoch']
        if metricsValidation.shape[0] > 0:
            plt.subplot(numRows, numCols, 7)
            plt.plot(stats['metricsValidationFullVolumesAvg'][:, labelIndex, 3])
            plt.title("(Val) DICE Full Volume")
            plt.xlabel("Every N Epochs")
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 8)
            plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 0], rmWindow))
            plt.title("(Val) Accuracy")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 9)
            plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 1], rmWindow))
            plt.title("(Val) Sensitivity (TP/(TP+FN))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 10)
            plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 2], rmWindow))
            plt.title("(Val) Specificity (TN/(TN+FP))")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 11)
            plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 3], rmWindow))
            plt.title("(Val) Dice")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

            plt.subplot(numRows, numCols, 12)
            plt.plot(MetricsMonitor.runningMeanConv(metricsValidation[:, labelIndex, 4], rmWindow))
            plt.title("(Val) Jaccard")
            plt.xlabel(xlabel)
            plt.ylim(0, 1)

        plt.suptitle("Metrics for label index %d (shown with a running mean window = %d" % (labelIndex, rmWindow), size=16)

        plt.show()
