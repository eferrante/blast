import theano
import theano.tensor as T
import lasagne
import lasagne as nn
from lasagne.layers import DenseLayer
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

import numpy as np
from theano.sandbox.cuda import dnn

class SpatialPyramidPooling3DDNNLayer(lasagne.layers.Layer):
    """
    Based on the original class SpatialPyramidPoolingDNNLayer

    3D Spatial Pyramid Pooling Layer
    Performs spatial pyramid pooling (SPP) over the input.
    It will turn a 3D input of arbitrary size into an output of fixed
    dimension.
    Hence, the convolutional part of a DNN can be connected to a dense part
    with a fixed number of nodes even if the dimensions of the
    input image are unknown.
    The pooling is performed over :math:`l` pooling levels.
    Each pooling level :math:`i` will create :math:`M_i` output features.
    :math:`M_i` is given by :math:`n_i * n_i * n_i`,
    with :math:`n_i` as the number of pooling operation per dimension in
    level :math:`i`, and we use a list of the :math:`n_i`'s as a
    parameter for SPP-Layer.
    The length of this list is the level of the spatial pyramid.
    Parameters
    ----------
    incoming : a :class:`Layer` instance or tuple
        The layer feeding into this layer, or the expected input shape.
    pool_dims : list of integers
        The list of :math:`n_i`'s that define the output dimension of each
        pooling level :math:`i`. The length of pool_dims is the level of
        the spatial pyramid.
    mode : string
        Pooling mode, one of 'max', 'average_inc_pad' or 'average_exc_pad'.
        Defaults to 'max'.
    **kwargs
        Any additional keyword arguments are passed to the :class:`Layer`
        superclass.
    Notes
    -----
    This layer should be inserted between the convolutional part of a
    DNN and its dense part. Convolutions can be used for
    arbitrary input dimensions, but the size of their output will
    depend on their input dimensions. Connecting the output of the
    convolutional to the dense part then usually demands us to fix
    the dimensions of the network's InputLayer.
    The spatial pyramid pooling layer, however, allows us to leave the
    network input dimensions arbitrary. The advantage over a global
    pooling layer is the added robustness against object deformations
    due to the pooling on different scales.
    References
    ----------
    .. [1] He, Kaiming et al (2015):
           Spatial Pyramid Pooling in Deep Convolutional Networks
           for Visual Recognition.
           http://arxiv.org/pdf/1406.4729.pdf.
    """
    def __init__(self, incoming, pool_dims=[4, 2, 1], mode='max', **kwargs):
            super(SpatialPyramidPooling3DDNNLayer, self).__init__(incoming,
                                                                **kwargs)
            if len(self.input_shape) != 5:
                raise ValueError("Tried to create a SPP layer with "
                                 "input shape %r. Expected 5 input dimensions "
                                 "(batchsize, channels, 3 spatial dimensions)."
                                 % (self.input_shape,))
            self.mode = mode
            self.pool_dims = pool_dims

    def get_output_for(self, input, **kwargs):
        input_size = tuple(symb if fixed is None else fixed
                           for fixed, symb
                           in zip(self.input_shape[2:], input.shape[2:]))
        pool_list = []
        for pool_dim in self.pool_dims:
            win_size = tuple((i + pool_dim - 1) // pool_dim
                             for i in input_size)
            str_size = tuple(i // pool_dim for i in input_size)

            pool = dnn.dnn_pool(input, win_size, str_size, self.mode, (0, 0, 0))

            # Crop the pooled layer to guarantee it has the sze specified in pool_dim
            pool = pool[:,:,0:pool_dim, 0:pool_dim, 0:pool_dim]

            pool = pool.flatten(3)
            pool_list.append(pool)

        return theano.tensor.concatenate(pool_list, axis=2)

    def get_output_shape_for(self, input_shape):
        num_features = sum(p*p*p for p in self.pool_dims)

        return (input_shape[0], input_shape[1], num_features)

class GaussianSampleLayer3D(nn.layers.MergeLayer):
    """
        Layer based on: https://github.com/Lasagne/Recipes/blob/master/examples/variational_autoencoder/variational_autoencoder.py
    """
    def __init__(self, mu, logsigma, rng=None, **kwargs):
        # mu: (nBatchs, nChannels, x, y, z)
        # logsigma: (nBatchs, 1, x, y, z)

        self.rng = rng if rng else RandomStreams(nn.random.get_rng().randint(1,2147462579))

        # Save the original dimensions of the data
#        self.muShape = mu.shape
#        self.logsigmaShape = logsigma.shape

        # Flatten the input mu and logsigmas
#        if len(self.muShape) != 5 or len(self.logsigmaShape) != 5:
#            raise ValueError("Tried to create a GaussianSampleLayer3D layer with "
#                             "input shape mu: %r, logsigma: %r. Expected 5 input dimensions "
#                             "(batchsize, channels, 3 spatial dimensions)."
#                             % (self.muShape, self.logsigmaShape))

        super(GaussianSampleLayer3D, self).__init__([mu, logsigma], **kwargs)

    def get_output_shape_for(self, input_shapes):
        return input_shapes[0]
        #return self.muShape


    def get_output_for(self, inputs, deterministic=False, **kwargs):
        mu, logsigma = inputs

        reshapedMu = mu.reshape((mu.shape[0], mu.shape[1], mu.shape[2] * mu.shape[3] * mu.shape[4]))
        reshapedLogsigma = logsigma.reshape((logsigma.shape[0], logsigma.shape[1], logsigma.shape[2] * logsigma.shape[3] * logsigma.shape[4]))

        if deterministic:
            return mu

        #result = reshapedMu + T.exp(reshapedLogsigma) * self.rng.normal(reshapedMu.shape)
        result = reshapedMu + reshapedLogsigma * self.rng.normal(reshapedMu.shape)

        return result.reshape(mu.shape)

def testSpatialPyramidPooling3DDNNLayer():
    inputVar = T.ftensor5('inputs')
    #dataSize = (1, 8, 10, 15, 12)
    dataSize = (None, 128, None, None, None)
    net = {}

    # Input layer:
    net['data'] = lasagne.layers.InputLayer(dataSize, input_var=inputVar)

    net['spatPyr'] = SpatialPyramidPooling3DDNNLayer(net['data'], pool_dims=[4, 2, 1], mode='max')

    net['fc'] = DenseLayer(net['spatPyr'], num_units=127)

    predPyr = lasagne.layers.get_output(net['spatPyr'], deterministic=True)
    predFc = lasagne.layers.get_output(net['fc'], deterministic=True)

    print "Compiling theano function"
    getPredPyrFn = theano.function([inputVar], predPyr)
    getPredFcFn = theano.function([inputVar], predFc)

    data = np.ones(shape=(1, 128, 9, 14, 8), dtype=np.float32)

    data[0,0,1,1,1] = 5

    print "Out of Pyr: " + str(getPredPyrFn(data).shape)
    print "Pyr size as indciated by function: " + str(lasagne.layers.get_output_shape(net['spatPyr']))

    print "Out of Fc: " + str(getPredFcFn(data).shape)


    data = np.ones(shape=(1, 128, 11, 13, 12), dtype=np.float32)
    data[0,0,1,1,1] = 5

    print "Out of Pyr: " + str(getPredPyrFn(data).shape)
    print "Pyr size as indciated by function: " + str(lasagne.layers.get_output_shape(net['spatPyr']))

    print "Out of Fc: " + str(getPredFcFn(data).shape)


if __name__ == "__main__":
    # Choose whether you'll train or test
    testSpatialPyramidPooling3DDNNLayer()
