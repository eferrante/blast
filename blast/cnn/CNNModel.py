import theano.tensor as T
import theano

import lasagne.nonlinearities
from lasagne.nonlinearities import leaky_rectify

import lasagne.objectives

import lasagne as nn

from lasagne.layers import InputLayer, SliceLayer, ConcatLayer, Upscale3DLayer, PadLayer, DropoutLayer, DenseLayer, GlobalPoolLayer, NonlinearityLayer

from lasagne.layers.dnn import Conv3DDNNLayer, Pool3DDNNLayer, batch_norm_dnn
import numpy as np
from lasagne.layers import batch_norm
from lasagne.nonlinearities import linear

from SpecialLayers import GaussianSampleLayer3D

import pickle

""" #################################################################################################################
                                              Basic CNN Model class
    ################################################################################################################# """

class CNNModel:
    """
        This class implements a basic CNNModel that can be trained using Theano/Lasagne.
        Any CNNModel must inherit from this class.
    """

    def __init__(self, confModel):
        # self.net stores the Theano CNN
        self.net = {}
        self.dataSize = None
        self.output = None
        self.model = None
        self.confModel = confModel

        # If there is any parameter that must no be trained, they should be included in this list
        self.nonTrainableParameters = []

        self.inputVar = None
        self.gt = None

    def checkDropout(self, layer, value=0.02):
        # Helper function to apply dropoutonly when it was specified in the conf file
        if self.confModel['useDropout']:
            return DropoutLayer(layer, p=value)
        else:
            return layer

    def checkDropoutAdv(self, layer, value=0.02):
            # Helper function to apply dropoutonly when it was specified in the conf file
            if self.confModel['useDropoutAdv']:
                return DropoutLayer(layer, p=value)
            else:
                return layer

    def checkBatchNorm(self, layer):
        # Helper function to apply batch normalization only when it was specified in the conf file
        if self.confModel['batchNorm']:
            return batch_norm_dnn(layer)
        else:
            return layer

    def chkFullRes(self, fullRes, padIfTrue):
        # Helper function to add the necessary padding to the convolutional layers so that
        # full resolution feature maps can be generated at testing time
        if fullRes:
            return padIfTrue
        else:
            return 0

def softmaxMultidim(x):
    e_x = theano.tensor.exp(x - x.max(axis=1, keepdims=True))

    return e_x / e_x.sum(axis=1, keepdims=True)

""" #################################################################################################################
                                              SimpleDeepMedic CNN Model class
    ################################################################################################################# """

class CNNModel_SimpleDeepmedic(CNNModel):
        """
            This class implements a standard Deepmedic architecture.

            Special Params:
                confModel['convDropoutRatio']: dropout ratio for convolutional layers
                confModel['fullyconnectedDropoutRatio']: dropout ratio for the 'fully connected'
                confModel['onlyFinalLayerTrainable']: specifies if only the final layer will be trainable (useful when doing transfer learning).
        """

        def __init__(self, confModel, fullRes = False, inputVar = None, inputToCascadeModel=False):

            CNNModel.__init__(self, confModel)

            if inputVar is None:
                self.inputVar = T.ftensor5('input')
            else:
                self.inputVar = inputVar

            self.gt = T.ftensor5('gt')
            self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

            self.output = None

            # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
            # tileSize = 51

            if fullRes:
                self.dataSize = (None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
                #self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
                #self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
            else:
                #self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
                self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width

            # ==== Create the CNN =====
            net = {}

            # Input layer:
            net['data'] = InputLayer(self.dataSize, input_var = self.inputVar)

            # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
            if fullRes:
                net['data'] = PadLayer(net['data'], [(21, 21), (21, 21), (21,21)], val=self.padValue)

            # ======== HIGH RESOLUTION PATH, CONSIDERING ONLY A 25X25x25 INPUT TILE ========

            # Layers slicing the input volume so that only the central volume is considered into the high resolution path.
            # If the input volume is a tile of 51x51x51 voxels, then the central area will be 25x25x25 and 9x9x9 predictions
            # will be produced.
            # If a full resolution volume is provided (not tiled, at testing time for example), then the sliced layer
            # will only remove voxels so that the high resolution and low resolution paths have compatible sizes.

            net['slice1'] = SliceLayer(net['data'], indices=slice(13, -13), axis=2)
            net['slice2'] = SliceLayer(net['slice1'], indices=slice(13, -13), axis=3)
            net['slice3'] = SliceLayer(net['slice2'], indices=slice(13, -13), axis=4)

            net['conv1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['slice3'], stride=1, num_filters=30, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv1'],  stride=1, num_filters=30, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv3'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv2'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv4'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv3'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv5'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv4'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv6'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv5'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv7'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv6'],  stride=1, num_filters=50, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['conv8'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv7'],  stride=1, num_filters=50, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])

            # ======== LOW RESOLUTION PATH, CONSIDERING A 51X51X51 INPUT TILE WHICH IS DOWNSAMPLED BY POOLING========

            # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path
            net['downsampling'] = Pool3DDNNLayer(net['data'], pool_size=3, mode='average_exc_pad')

            net['low_conv1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['downsampling'], stride=1, num_filters=30, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['low_conv2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv1'],  stride=1, num_filters=30, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['low_conv3'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv2'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['low_conv4'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv3'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['low_conv5'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv4'],  stride=1, num_filters=40, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['low_conv6'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv5'],  stride=1, num_filters=50, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])
            net['low_conv7'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv6'],  stride=1, num_filters=50, filter_size=(3, 3, 3))), confModel['convDropoutRatio'])

            # ======== CONCATENATE LOW AND HIGH RESOLUTION LAYERS ========

            net['concatLowHigh'] = ConcatLayer([Upscale3DLayer(net['low_conv7'], scale_factor=3), net['conv8']])

            # Remove the 'trainable' tag from the weights of the convolutional layers if 'onlyFinalLayerTrainable' is true.
            if 'onlyFinalLayerTrainable' in confModel.keys():
                if confModel['onlyFinalLayerTrainable']:
                    self.nonTrainableParameters = lasagne.layers.get_all_params(net['concatLowHigh'], trainable=True)

            net['fc1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['concatLowHigh'], stride=1, num_filters=150, filter_size=(1, 1, 1))), confModel['fullyconnectedDropoutRatio'])
            net['fc2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['fc1'], stride=1, num_filters=150, filter_size=(1, 1, 1), pad='same')), confModel['fullyconnectedDropoutRatio'])

            net['softmax'] = self.checkBatchNorm(Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1), nonlinearity=softmaxMultidim))

            self.net = net
            self.output = net['softmax']

class CNNModel_SimpleDeepmedic_3Paths(CNNModel):
    """
        This class implements a DeepMedic architecture with 3 paths.

        Input: 64 x 64 x 64 -> Pred: 16 x 16 x 16

        Special Params:
            confModel['convDropoutRatio']: dropout ratio for convolutional layers
            confModel['fullyconnectedDropoutRatio']: dropout ratio for the 'fully connected'
            confModel['onlyFinalLayerTrainable']: specifies if only the final layer will be trainable (useful when doing transfer learning).

    """

    def __init__(self, confModel, fullRes=False, inputVar=None, inputToCascadeModel=False):

        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (
            None, confModel['numChannels'], 64, 64, 64)  # Batch size x Img Channels x Depth x Height x Width

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        if fullRes:
            net['data'] = PadLayer(net['data'], [(24, 24), (24, 24), (24, 24)], val=self.padValue)

        # ======== HIGH RESOLUTION PATH, CONSIDERING ONLY A 32x32x32 INPUT TILE ========

        # Layers slicing the input volume so that only the central volume is considered into the high resolution path.
        # If the input volume is a tile of 51x51x51 voxels, then the central area will be 25x25x25 and 9x9x9 predictions
        # will be produced.
        # If a full resolution volume is provided (not tiled, at testing time for example), then the sliced layer
        # will only remove voxels so that the high resolution and low resolution paths have compatible sizes.

        net['slice1'] = SliceLayer(net['data'], indices=slice(16, -16), axis=2)
        net['slice2'] = SliceLayer(net['slice1'], indices=slice(16, -16), axis=3)
        net['slice3'] = SliceLayer(net['slice2'], indices=slice(16, -16), axis=4)

        # 32^3 -> 30^2
        net['conv1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['slice3'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 30^3 -> 28^2
        net['conv2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 28^3 -> 26^2
        net['conv3'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv2'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 26^3 -> 24^2
        net['conv4'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv3'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 24^3 -> 22^2
        net['conv5'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv4'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 22^3 -> 20^2
        net['conv6'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv5'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 20^3 -> 18^2
        net['conv7'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv6'], stride=1, num_filters=50, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 18^3 -> 16^2
        net['conv8'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['conv7'], stride=1, num_filters=50, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])

        # ======== LOW RESOLUTION PATH (DOWNSAMPLED BY 2) ========

        # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path
        # 64^3 -> 32^2
        net['downsampling_2'] = Pool3DDNNLayer(net['data'], pool_size=2, mode='average_exc_pad')

        # 32^3 -> 30^2
        net['low_conv1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['downsampling_2'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 30^3 -> 28^2
        net['low_conv2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 28^3 -> 26^2
        net['low_conv3'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv2'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 26^3 -> 24^2
        net['low_conv4'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv3'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 24^3 -> 22^2
        net['low_conv5'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv4'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 22^3 -> 20^2
        net['low_conv6'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv5'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 20^3 -> 18^2
        net['low_conv7'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv6'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 18^3 -> 16^2
        net['low_conv8'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv7'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 16^3 -> 14^2
        net['low_conv9'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv8'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 14^3 -> 12^2
        net['low_conv10'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv9'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 12^3 -> 10^2
        net['low_conv11'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv10'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 10^3 -> 8^2
        net['low_conv12'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_conv11'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])


        # ======== LOW RESOLUTION PATH (DOWNSAMPLED BY 4) ========

        # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path
        # 64^3 -> 16^2
        net['downsampling_4'] = Pool3DDNNLayer(net['data'], pool_size=4, mode='average_exc_pad')

        # 16^3 -> 14^2
        net['low_low_conv1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['downsampling_4'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 14^3 -> 12^2
        net['low_low_conv2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_low_conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 12^3 -> 10^2
        net['low_low_conv3'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_low_conv2'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 10^3 -> 8^2
        net['low_low_conv4'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_low_conv3'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 8^3 -> 6^2
        net['low_low_conv5'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_low_conv4'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])
        # 6^3 -> 4^2
        net['low_low_conv6'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['low_low_conv5'], stride=1, num_filters=40, filter_size=(3, 3, 3))),confModel['convDropoutRatio'])

        # ======== CONCATENATE LOW AND HIGH RESOLUTION PATHS ========

        net['concatLowHigh'] = ConcatLayer([Upscale3DLayer(net['low_low_conv6'], scale_factor=4), Upscale3DLayer(net['low_conv12'], scale_factor=2), net['conv8']])

        # Remove the 'trainable' tag from the weights of the convolutional layers if 'onlyFinalLayerTrainable' is true.
        if 'onlyFinalLayerTrainable' in confModel.keys():
            if confModel['onlyFinalLayerTrainable']:
                self.nonTrainableParameters = lasagne.layers.get_all_params(net['concatLowHigh'], trainable=True)

        net['fc1'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['concatLowHigh'], stride=1, num_filters=150, filter_size=(1, 1, 1))),confModel['fullyconnectedDropoutRatio'])

        net['fc2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['fc1'], stride=1, num_filters=150, filter_size=(1, 1, 1), pad='same')), confModel['fullyconnectedDropoutRatio'])

        net['softmax'] = self.checkBatchNorm(Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1),nonlinearity=softmaxMultidim))

        self.net = net
        self.output = net['softmax']

class CNNModel_SimpleDeepmedic_Heteroscedastic(CNNModel):
    """
        This class implements a Deepmedic architecture which also estimates uncertainty
        following https://arxiv.org/abs/1703.04977

        Input: 51 x 51 x 51 ----> Pred: 9 x 9 x 9

        Special params:
            confModel['convDropoutRatio']: dropout ratio for convolutional layers
            confModel['fullyconnectedDropoutRatio']: dropout ratio for the 'fully connected'
            confModel['onlyFinalLayerTrainable']: specifies if only the final layer will be trainable (useful when doing transfer learning).
    """

    def __init__(self, confModel, fullRes=False, inputVar=None, inputToCascadeModel=False):

        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (
            None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        if fullRes:
            net['data'] = PadLayer(net['data'], [(21, 21), (21, 21), (21, 21)], val=self.padValue)

        # ======== HIGH RESOLUTION PATH, CONSIDERING ONLY A 25X25x25 INPUT TILE ========

        # Layers slicing the input volume so that only the central volume is considered into the high resolution path.
        # If the input volume is a tile of 51x51x51 voxels, then the central area will be 25x25x25 and 9x9x9 predictions
        # will be produced.
        # If a full resolution volume is provided (not tiled, at testing time for example), then the sliced layer
        # will only remove voxels so that the high resolution and low resolution paths have compatible sizes.

        net['slice1'] = SliceLayer(net['data'], indices=slice(13, -13), axis=2)
        net['slice2'] = SliceLayer(net['slice1'], indices=slice(13, -13), axis=3)
        net['slice3'] = SliceLayer(net['slice2'], indices=slice(13, -13), axis=4)

        net['conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['slice3'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv2'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv3'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv4'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv5'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv6'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv8'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv7'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])

        # ======== LOW RESOLUTION PATH, CONSIDERING A 51X51X51 INPUT TILE WHICH IS DOWNSAMPLED BY POOLING========

        # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path
        net['downsampling'] = Pool3DDNNLayer(net['data'], pool_size=3, mode='average_exc_pad')

        net['low_conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['downsampling'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv2'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv3'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv4'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv5'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv6'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])

        # ======== CONCATENATE LOW AND HIGH RESOLUTION LAYERS ========

        net['concatLowHigh'] = ConcatLayer([Upscale3DLayer(net['low_conv7'], scale_factor=3), net['conv8']])

        # Remove the 'trainable' tag from the weights of the convolutional layers if 'onlyFinalLayerTrainable' is true.
        if 'onlyFinalLayerTrainable' in confModel.keys():
            if confModel['onlyFinalLayerTrainable']:
                self.nonTrainableParameters = lasagne.layers.get_all_params(net['concatLowHigh'], trainable=True)

        net['fc1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['concatLowHigh'], stride=1, num_filters=150, filter_size=(1, 1, 1))),
            confModel['fullyconnectedDropoutRatio'])

        net['fc2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['fc1'], stride=1, num_filters=150, filter_size=(1, 1, 1), pad='same')),confModel['fullyconnectedDropoutRatio'])

        #net['softmax'] = self.checkBatchNorm(Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1),nonlinearity=softmaxMultidim))

        net['mus'] = Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1), nonlinearity=lasagne.nonlinearities.linear)

        net['sigmas'] = Conv3DDNNLayer(net['fc2'], stride=1, num_filters=1, filter_size=(1, 1, 1), nonlinearity=theano.tensor.nnet.nnet.softplus)

        out = []

        for i in range(self.confModel['heteroscedasticNumSamplers']):
            name = 'sampler' + str(i)
            net[name] = GaussianSampleLayer3D(net['mus'], net['sigmas'])
            net[name] = lasagne.layers.NonlinearityLayer(net[name], nonlinearity=softmaxMultidim)
            out.append(net['sampler' + str(i)])

        self.net = net
        self.output = out

class CNNModel_SimpleDeepmedic_LearnedDownsampling(CNNModel):
    """
        This class implements the same Architecture that is implemented in CNNModel_SimpleDeepmedic, but instead
        of using average maxpooling to downsample the low resolution path, it learns the downsampling through
        strided convolutions

        Input: 51 x 51 x 51 ----> Pred: 9 x 9 x 9

        Special params:
            confModel['convDropoutRatio']: dropout ratio for convolutional layers
            confModel['fullyconnectedDropoutRatio']: dropout ratio for the 'fully connected'
            confModel['onlyFinalLayerTrainable']: specifies if only the final layer will be trainable (useful when doing transfer learning).
    """

    def __init__(self, confModel, fullRes=False, inputVar=None, inputToCascadeModel=False):

        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (
            None, confModel['numChannels'], confModel['tileSize'][0], confModel['tileSize'][1], confModel['tileSize'][2])  # Batch size x Img Channels x Depth x Height x Width

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        if fullRes:
            net['data'] = PadLayer(net['data'], [(21, 21), (21, 21), (21, 21)], val=self.padValue)

        # ======== HIGH RESOLUTION PATH, CONSIDERING ONLY A 25X25x25 INPUT TILE ========

        # Layers slicing the input volume so that only the central volume is considered into the high resolution path.
        # If the input volume is a tile of 51x51x51 voxels, then the central area will be 25x25x25 and 9x9x9 predictions
        # will be produced.
        # If a full resolution volume is provided (not tiled, at testing time for example), then the sliced layer
        # will only remove voxels so that the high resolution and low resolution paths have compatible sizes.

        net['slice1'] = SliceLayer(net['data'], indices=slice(13, -13), axis=2)
        net['slice2'] = SliceLayer(net['slice1'], indices=slice(13, -13), axis=3)
        net['slice3'] = SliceLayer(net['slice2'], indices=slice(13, -13), axis=4)

        net['conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['slice3'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv2'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv3'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv4'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv5'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv6'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv8'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv7'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])

        # ======== LOW RESOLUTION PATH, CONSIDERING A 51X51X51 INPUT TILE WHICH IS DOWNSAMPLED BY POOLING========

        # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path

        #net['downsampling'] = Pool3DDNNLayer(net['data'], pool_size=3, mode='average_exc_pad')

        net['downsampling'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['data'], stride=3, filter_size=(3, 3, 3), num_filters=15)), confModel['convDropoutRatio'])

        net['low_conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['downsampling'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv2'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv3'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv4'], stride=1, num_filters=40, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv5'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv6'], stride=1, num_filters=50, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])

        # ======== CONCATENATE LOW AND HIGH RESOLUTION LAYERS ========

        net['concatLowHigh'] = ConcatLayer([Upscale3DLayer(net['low_conv7'], scale_factor=3), net['conv8']])

        # Remove the 'trainable' tag from the weights of the convolutional layers if 'onlyFinalLayerTrainable' is true.
        if 'onlyFinalLayerTrainable' in confModel.keys():
            if confModel['onlyFinalLayerTrainable']:
                self.nonTrainableParameters = lasagne.layers.get_all_params(net['concatLowHigh'], trainable=True)

        net['fc1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['concatLowHigh'], stride=1, num_filters=150, filter_size=(1, 1, 1))),
            confModel['fullyconnectedDropoutRatio'])

        net['fc2'] = self.checkDropout(self.checkBatchNorm(Conv3DDNNLayer(net['fc1'], stride=1, num_filters=150, filter_size=(1, 1, 1), pad='same')),confModel['fullyconnectedDropoutRatio'])

        net['softmax'] = self.checkBatchNorm(Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1), nonlinearity=softmaxMultidim))

        self.net = net
        self.output = net['softmax']


class CNNModel_SimpleDeepmedic2D(CNNModel):
    """
        This model implements a 2D version of Deepmedic, which process only one slice at a time, without considering
        any context.

        Planar network: Input: 51 x 51 x 1 ---> Pred: 9 x 9 x 1

        Special params:
            confModel['convDropoutRatio']: dropout ratio for convolutional layers
            confModel['fullyconnectedDropoutRatio']: dropout ratio for the 'fully connected'

    """

    def __init__(self, confModel, fullRes=False, inputVar=None):
        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        if fullRes:
            net['data'] = PadLayer(net['data'], [(21, 21), (21, 21), (0, 0)], val=self.padValue)

        # ======== HIGH RESOLUTION PATH, CONSIDERING ONLY A 25X25x25 INPUT TILE ========

        # Layers slicing the input volume so that only the central volume is considered into the high resolution path.
        # If the input volume is a tile of 51x51x51 voxels, then the central area will be 25x25x25 and 9x9x9 predictions
        # will be produced.
        # If a full resolution volume is provided (not tiled, at testing time for example), then the sliced layer
        # will only remove voxels so that the high resolution and low resolution paths have compatible sizes.

        net['slice1'] = SliceLayer(net['data'], indices=slice(13, -13), axis=2)
        net['slice2'] = SliceLayer(net['slice1'], indices=slice(13, -13), axis=3)


        net['conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['slice2'], stride=1, num_filters=30, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv1'], stride=1, num_filters=30, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv2'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv3'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv4'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv5'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv6'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv8'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv7'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])

        # ======== LOW RESOLUTION PATH, CONSIDERING A 51X51X51 INPUT TILE WHICH IS DOWNSAMPLED BY POOLING========

        # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path
        net['downsampling'] = Pool3DDNNLayer(net['data'], pool_size=[3, 3, 1], mode='average_exc_pad')

        net['low_conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['downsampling'], stride=1, num_filters=30, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv1'], stride=1, num_filters=30, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv2'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv3'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv4'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv5'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv6'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])

        # ======== CONCATENATE LOW AND HIGH RESOLUTION LAYERS ========

        net['concatLowHigh'] = ConcatLayer([Upscale3DLayer(net['low_conv7'], scale_factor=[3, 3, 1]), net['conv8']])
        net['fc1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['concatLowHigh'], stride=1, num_filters=150, filter_size=(1, 1, 1))),
            confModel['fullyconnectedDropoutRatio'])
        net['fc2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['fc1'], stride=1, num_filters=150, filter_size=(1, 1, 1))),
            confModel['fullyconnectedDropoutRatio'])

        net['softmax'] = self.checkBatchNorm(
            Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1),
                           nonlinearity=softmaxMultidim))

        self.net = net
        self.output = net['softmax']

class CNNModel_SimpleDeepmedic_Sandwich(CNNModel):
    """
        This class implements a Deepmedic "sandwich" architecture, which takes 3 contiguous slices as input
        and segments only the central slice.

        Sandwich Deepmedic: 51 x 51 x 3 ---> 9 x 9 x 1

        Special params:
            confModel['convDropoutRatio']: dropout ratio for convolutional layers
            confModel['fullyconnectedDropoutRatio']: dropout ratio for the 'fully connected'
            confModel['onlyFinalLayerTrainable']: specifies if only the final layer will be trainable (useful when doing transfer learning).

    """

    def __init__(self, confModel, fullRes=False, inputVar=None):
        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (
            None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        if fullRes:
            net['data'] = PadLayer(net['data'], [(21, 21), (21, 21), (1, 1)], val=self.padValue)

        # ======== HIGH RESOLUTION PATH, CONSIDERING ONLY A 25X25x25 INPUT TILE ========

        # Layers slicing the input volume so that only the central volume is considered into the high resolution path.
        # If the input volume is a tile of 51x51x51 voxels, then the central area will be 25x25x25 and 9x9x9 predictions
        # will be produced.
        # If a full resolution volume is provided (not tiled, at testing time for example), then the sliced layer
        # will only remove voxels so that the high resolution and low resolution paths have compatible sizes.

        net['slice1'] = SliceLayer(net['data'], indices=slice(13, -13), axis=2)
        net['slice2'] = SliceLayer(net['slice1'], indices=slice(13, -13), axis=3)

        net['conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['slice2'], stride=1, num_filters=30, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv2'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv3'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv4'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv5'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv6'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['conv8'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['conv7'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])

        # ======== LOW RESOLUTION PATH, CONSIDERING A 51X51X51 INPUT TILE WHICH IS DOWNSAMPLED BY POOLING========

        # Layers slicing the input 51x51x51 volume so that only the central 25x25x25 volume is considered into the high resolution path
        net['downsampling'] = Pool3DDNNLayer(net['data'], pool_size=[3, 3, 1], mode='average_exc_pad')

        net['low_conv1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['downsampling'], stride=1, num_filters=30, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv1'], stride=1, num_filters=30, filter_size=(3, 3, 3))),
            confModel['convDropoutRatio'])
        net['low_conv3'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv2'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv4'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv3'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv5'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv4'], stride=1, num_filters=40, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv6'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv5'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])
        net['low_conv7'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['low_conv6'], stride=1, num_filters=50, filter_size=(3, 3, 1))),
            confModel['convDropoutRatio'])

        # ======== CONCATENATE LOW AND HIGH RESOLUTION LAYERS ========

        net['concatLowHigh'] = ConcatLayer([Upscale3DLayer(net['low_conv7'], scale_factor=[3, 3, 1]), net['conv8']])
        net['fc1'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['concatLowHigh'], stride=1, num_filters=150, filter_size=(1, 1, 1))),
            confModel['fullyconnectedDropoutRatio'])
        net['fc2'] = self.checkDropout(
            self.checkBatchNorm(Conv3DDNNLayer(net['fc1'], stride=1, num_filters=150, filter_size=(1, 1, 1))),
            confModel['fullyconnectedDropoutRatio'])

        net['softmax'] = self.checkBatchNorm(
            Conv3DDNNLayer(net['fc2'], stride=1, num_filters=len(self.confModel['labels']), filter_size=(1, 1, 1),
                           nonlinearity=softmaxMultidim))

        self.net = net
        self.output = net['softmax']


class CNNModel_UNet2D_Sandwich(CNNModel):
    """
        Modified UNet, based on the code by Ricardo Guerrero Moreno.
        The main difference with standard UNet is that it replaces concatenation by summation, when combinining the
        skip connections with the expansive path.

        It implements a Sandwich UNet, where only the context for the top and bottom contiguous slices is considered.

        Inptu: 64 x 64 x 3 -> Pred: 4 x 4 x 1

        Special params:
            confModel['extraFilters'] : filters that will be added to every convolutional layer (if negative, it is substracted)

    """

    def __init__(self, confModel, fullRes=False, inputVar=None):
        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (
                None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (
                None, confModel['numChannels'], self.confModel['tileSize'][0], self.confModel['tileSize'][1],
                self.confModel['tileSize'][2])  # Batch size x Img Channels x Depth x Height x Width

        if 'extraFilters' in confModel.keys():
            extraFilters = confModel['extraFilters']
        else:
            extraFilters = 0

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        if fullRes:
            net['data'] = PadLayer(net['data'], [(0, 0), (0, 0), (1, 1)], val=self.padValue)

        # 64 x 64 x 3 (1) --> 32 x 32 x 1 (16)
        net['conv1_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['data'],     filter_size=(3,3,3), num_filters=16 + extraFilters, pad=[1,1,0]))
        net['conv1_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv1_1a'], filter_size=(3,3,1), num_filters=16 + extraFilters, pad='same', nonlinearity=None))
        net['relu1']      = lasagne.layers.NonlinearityLayer(net['conv1_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['pool1']      = Pool3DDNNLayer(net['relu1'], pool_size=(2,2,1))

        # 32 x 32 x 1 (16) --> 16 x 16 x 1 (32)
        net['conv2_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['pool1'],    filter_size=(3,3,1), num_filters=32 + extraFilters, pad='same'))
        net['conv2_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv2_1a'], filter_size=(3,3,1), num_filters=32 + extraFilters, pad='same', nonlinearity=None))
        net['relu2']      = lasagne.layers.NonlinearityLayer(net['conv2_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['pool2']      = Pool3DDNNLayer(net['relu2'], pool_size=(2,2,1))

        # 16 x 16 x 1 (32) --> 8 x 8 x 1 (64)
        net['conv3_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['pool2'],    filter_size=(3,3,1), num_filters=64 + extraFilters, pad='same'))
        net['conv3_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv3_1a'], filter_size=(3,3,1), num_filters=64 + extraFilters, pad='same', nonlinearity=None))
        net['relu3']      = lasagne.layers.NonlinearityLayer(net['conv3_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['pool3']      = Pool3DDNNLayer(net['relu3'], pool_size=(2,2,1))

        # 8 x 8 x 1 (64) --> 8 x 8 x 1 (128)
        net['conv4_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['pool3'],    filter_size=(3,3,1), num_filters=128 + extraFilters, pad='same'))
        net['conv4_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv4_1a'], filter_size=(3,3,1), num_filters=128 + extraFilters, pad='same', nonlinearity=None))
        net['relu4']      = lasagne.layers.NonlinearityLayer(net['conv4_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['drop4']      = lasagne.layers.DropoutLayer(net['relu4'], p=0.5)

        # 8 x 8 x 1 (128) --> 16 x 16 x 1 (64)
        net['deconv4']    = batch_norm_dnn(Conv3DDNNLayer(lasagne.layers.Upscale3DLayer(net['drop4'], scale_factor=2), filter_size=(3,3,1), num_filters= 64 + extraFilters, pad='same',  nonlinearity=None))

        # 8 x 8 x 1 (128) --> 16 x 16 x 1 (64)
        net['concat5']    = lasagne.layers.NonlinearityLayer(lasagne.layers.ElemwiseSumLayer([net['deconv4'],net['conv3_1b']], cropping=[None, None, 'center', 'center', 'center']),nonlinearity = lasagne.nonlinearities.rectify)
        net['conv5_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['concat5'],  filter_size=(3,3,1), num_filters=64 + extraFilters, pad='same'))
        net['conv5_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv5_1a'], filter_size=(3,3,1), num_filters=64 + extraFilters, pad='same'))
        net['drop5']      = lasagne.layers.DropoutLayer(net['conv5_1b'], p=0.5)
        net['deconv5']    = batch_norm_dnn(Conv3DDNNLayer(lasagne.layers.Upscale3DLayer(net['drop5'], scale_factor=[2,2,1]), filter_size=(3,3,1), num_filters=32, pad='same',  nonlinearity=None))

        # 16 x 16 x 1 (64) --> 32 x 32 x 1 (32)
        net['concat6']    = lasagne.layers.NonlinearityLayer(lasagne.layers.ElemwiseSumLayer([net['deconv5'],net['conv2_1b']], cropping=[None, None, 'center', 'center', 'center']),nonlinearity = lasagne.nonlinearities.rectify)
        net['conv6_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['concat6'],  filter_size=(3,3,1), num_filters=32 + extraFilters, pad='same'))
        net['conv6_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv6_1a'], filter_size=(3,3,1), num_filters=32 + extraFilters, pad='same'))
        net['drop6']      = lasagne.layers.DropoutLayer(net['conv6_1b'], p=0.5)
        net['deconv6']    = batch_norm_dnn(Conv3DDNNLayer(lasagne.layers.Upscale3DLayer(net['drop6'], scale_factor=[2,2,1]), filter_size=(3,3,1), num_filters=16, pad='same',  nonlinearity=None))

        # 32 x 32 x 1 (32) --> 64 x 64 x 1 (16)
        net['concat7']    = lasagne.layers.NonlinearityLayer(lasagne.layers.ElemwiseSumLayer([net['deconv6'],net['conv1_1b']], cropping=[None, None, 'center', 'center', 'center']),nonlinearity = lasagne.nonlinearities.rectify)
        net['conv7_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['concat7'],  filter_size=(3,3,1), num_filters=16 + extraFilters, pad='same'))
        net['conv7_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv7_1a'], filter_size=(3,3,1), num_filters=16 + extraFilters, pad='same'))
        net['conv_out']   = batch_norm_dnn(Conv3DDNNLayer(net['conv7_1b'],   filter_size=(3,3,1), num_filters=len(self.confModel['labels']), pad='same',  nonlinearity=None))
        net['output']     = Conv3DDNNLayer(net['conv_out'], num_filters=len(self.confModel['labels']), filter_size=(1,1,1), nonlinearity=softmaxMultidim)

        self.net = net
        self.output = net['output']

class CNNModel_UNet3D(CNNModel):
    """
        Modified 3D UNet, based on the code by Ricardo Guerrero Moreno.

        The main difference with standard UNet is that it replaces concatenation by summation, when combinining the
        skip connections with the expansive path.

        64 x 64 x 64 (1) -> 4 x 4 x 1 (NumLabels)

        Special params:
            confModel['extraFilters'] : filters that will be added to every convolutional layer (if negative, it is substracted)
    """

    def __init__(self, confModel, fullRes=False, inputVar=None):
        CNNModel.__init__(self, confModel)

        if inputVar is None:
            self.inputVar = T.ftensor5('input')
        else:
            self.inputVar = inputVar

        self.gt = T.ftensor5('gt')
        self.padValue = theano.shared(np.array(0.0, dtype=theano.config.floatX))

        self.output = None

        if 'extraFilters' in confModel.keys():
            extraFilters = confModel['extraFilters']
        else:
            extraFilters = 0

        # For a segment of size 51x51x51, the network produces a 9x9x9 prediction corresponding to the central voxels
        # tileSize = 51

        if fullRes:
            self.dataSize = (None, confModel['numChannels'], None, None, None)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 69, 90, 72)  # Batch size x Img Channels x Depth x Height x Width
            # self.dataSize = (None, confModel['numChannels'], 90, 90, 90)  # Batch size x Img Channels x Depth x Height x Width
        else:
            # self.dataSize = (None, confModel['numChannels'], 51, 51, 51)  # Batch size x Img Channels x Depth x Height x Width
            self.dataSize = (None, confModel['numChannels'], self.confModel['tileSize'][0], self.confModel['tileSize'][1], self.confModel['tileSize'][2])  # Batch size x Img Channels x Depth x Height x Width

        # ==== Create the CNN =====
        net = {}

        # Input layer:
        net['data'] = InputLayer(self.dataSize, input_var=self.inputVar)

        # If fullRes==True, the input volume is padded so that it generates a full resolution prediction volume
        #if fullRes:
        # [16, 16, 32, 32, 64, 128, 64, 32, 16
        #    net['data'] = PadLayer(net['data'], [(0, 0), (0, 0), (0, 0)], val=self.padValue)

        # 64 x 64 x 64 (1) --> 32 x 32 x 32 (16)
        net['conv1_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['data'],     filter_size=(3,3,3), num_filters=16 + extraFilters, pad='same'))
        net['conv1_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv1_1a'], filter_size=(3,3,3), num_filters=16 + extraFilters, pad='same', nonlinearity=None))
        net['relu1']      = lasagne.layers.NonlinearityLayer(net['conv1_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['pool1']      = Pool3DDNNLayer(net['relu1'], pool_size=(2,2,2))

        # 32 x 32 x 1 (16) --> 16 x 16 x 1 (32)
        net['conv2_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['pool1'],    filter_size=(3,3,3), num_filters=32 + extraFilters, pad='same'))
        net['conv2_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv2_1a'], filter_size=(3,3,3), num_filters=32 + extraFilters, pad='same', nonlinearity=None))
        net['relu2']      = lasagne.layers.NonlinearityLayer(net['conv2_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['pool2']      = Pool3DDNNLayer(net['relu2'], pool_size=(2,2,2))

        # 16 x 16 x 1 (32) --> 8 x 8 x 1 (64)
        net['conv3_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['pool2'],    filter_size=(3,3,3), num_filters=64 + extraFilters, pad='same'))
        net['conv3_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv3_1a'], filter_size=(3,3,3), num_filters=64 + extraFilters, pad='same', nonlinearity=None))
        net['relu3']      = lasagne.layers.NonlinearityLayer(net['conv3_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['pool3']      = Pool3DDNNLayer(net['relu3'], pool_size=(2,2,2))

        # 8 x 8 x 1 (64) --> 8 x 8 x 1 (128)
        net['conv4_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['pool3'],    filter_size=(3,3,3), num_filters=128 + extraFilters, pad='same'))
        net['conv4_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv4_1a'], filter_size=(3,3,3), num_filters=128 + extraFilters, pad='same', nonlinearity=None))
        net['relu4']      = lasagne.layers.NonlinearityLayer(net['conv4_1b'],nonlinearity = lasagne.nonlinearities.rectify)
        net['drop4']      = lasagne.layers.DropoutLayer(net['relu4'], p=0.5)

        # 8 x 8 x 1 (128) --> 16 x 16 x 1 (64)
        net['deconv4']    = batch_norm_dnn(Conv3DDNNLayer(lasagne.layers.Upscale3DLayer(net['drop4'], scale_factor=2), filter_size=(3,3,3), num_filters=64 + extraFilters, pad='same',  nonlinearity=None))

        # 8 x 8 x 1 (128) --> 16 x 16 x 1 (64)
        net['concat5']    = lasagne.layers.NonlinearityLayer(lasagne.layers.ElemwiseSumLayer([net['deconv4'],net['conv3_1b']], cropping=[None, None, 'center', 'center', 'center']),nonlinearity = lasagne.nonlinearities.rectify)
        net['conv5_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['concat5'],  filter_size=(3,3,3), num_filters=64 + extraFilters, pad='same'))
        net['conv5_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv5_1a'], filter_size=(3,3,3), num_filters=64 + extraFilters, pad='same'))
        net['drop5']      = lasagne.layers.DropoutLayer(net['conv5_1b'], p=0.5)
        net['deconv5']    = batch_norm_dnn(Conv3DDNNLayer(lasagne.layers.Upscale3DLayer(net['drop5'], scale_factor=[2,2,2]), filter_size=(3,3,3), num_filters=32 + extraFilters, pad='same',  nonlinearity=None))

        # 16 x 16 x 1 (64) --> 32 x 32 x 1 (32)
        net['concat6']    = lasagne.layers.NonlinearityLayer(lasagne.layers.ElemwiseSumLayer([net['deconv5'],net['conv2_1b']], cropping=[None, None, 'center', 'center', 'center']),nonlinearity = lasagne.nonlinearities.rectify)
        net['conv6_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['concat6'],  filter_size=(3,3,3), num_filters=32 + extraFilters, pad='same'))
        net['conv6_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv6_1a'], filter_size=(3,3,3), num_filters=32 + extraFilters, pad='same'))
        net['drop6']      = lasagne.layers.DropoutLayer(net['conv6_1b'], p=0.5)
        net['deconv6']    = batch_norm_dnn(Conv3DDNNLayer(lasagne.layers.Upscale3DLayer(net['drop6'], scale_factor=[2,2,2]), filter_size=(3,3,3), num_filters=16 + extraFilters, pad='same',  nonlinearity=None))

        # 32 x 32 x 1 (32) --> 64 x 64 x 1 (16)
        net['concat7']    = lasagne.layers.NonlinearityLayer(lasagne.layers.ElemwiseSumLayer([net['deconv6'],net['conv1_1b']], cropping=[None, None, 'center', 'center', 'center']),nonlinearity = lasagne.nonlinearities.rectify)
        net['conv7_1a']   = batch_norm_dnn(Conv3DDNNLayer(net['concat7'],  filter_size=(3,3,3), num_filters=16 + extraFilters, pad='same'))
        net['conv7_1b']   = batch_norm_dnn(Conv3DDNNLayer(net['conv7_1a'], filter_size=(3,3,3), num_filters=16 + extraFilters, pad='same'))
        net['conv_out']   = batch_norm_dnn(Conv3DDNNLayer(net['conv7_1b'],   filter_size=(3,3,3), num_filters=len(self.confModel['labels']), pad='same',  nonlinearity=None))
        net['output']     = Conv3DDNNLayer(net['conv_out'], num_filters=len(self.confModel['labels']), filter_size=(1,1,1), nonlinearity=softmaxMultidim)

        self.net = net
        self.output = net['output']

