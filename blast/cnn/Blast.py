import cPickle as pickle
import collections
import copy
import datetime
import gc
import logging
import os
import sys

import lasagne
import nibabel as nib
import nilearn.image as nilearnimage
import numpy as np
import scipy.ndimage
import scipy.stats
import theano.tensor

import CNNModel
from BatchGenerator import Simple3DBatchGenerator
from blast.cnn.MetricsMonitor import MetricsMonitor
from ..aux.Common import ensureDir, loadFilenames, logDictToInfoStream, loadFilenamesSingle, getMedicalImageBasename, getMedicalImageExtension


def categorical_crossentropy_numerical_stab(coding_dist, true_dist):
    if true_dist.ndim == coding_dist.ndim:
        return -theano.tensor.sum(true_dist * theano.tensor.log(coding_dist + 1e-8),
                           axis=coding_dist.ndim - 1)
    elif true_dist.ndim == coding_dist.ndim - 1:
        return theano.tensor.nnet.CrossentropyCategorical1Hot(coding_dist, true_dist)
    else:
        raise TypeError('rank mismatch between coding and true distributions')

class Blast:

    def __init__(self):
        # Configuration objects
        self.confModel = None
        self.confTrain = None
        self.confTest = None
        # Models
        self.segmTileBasedModel = None
        self.segmFullResModel = None

        # ====== SEGMENTER EXPRESSIONS AND FUNCTIONS ===============
        # Loss, prediction and training functions for the segmenter
        self.lossSegm = None
        self.predSegm = None
        self.predSegm_test = None

        self.predFullResSegm = None
        self.predFullResSegm_test = None

        self.getSegmUncertFullRes_test = None

        # Loss, prediction and training functions for the adversarial
        self.lossAdv = None
        self.predAdv = None

        # Training function that returns the loss
        self.trainSegmFn = None

        # Prediction function used to predict while training (not for full resolution predictions, only for segments)
        self.getPredFn = None

        # Prediction function used to predict while training (for full resolution predictions)
        self.getSegmPredFullResFn = None

        # Prediction function used to predict the uncertainties in models with aleatoric uncertainity
        self.getSegmUncertFullResFn = None

        self.stats = {}

        self.ensembleNetworks = []

    def createModel(self, confModel):#, fullRes = False):
        """
            It loads a given configuration file and creates the corresponding CNN model

        :param confModel: configuration object
        :param fullRes: for certain CNN models (like CNNModel_FFD2D_Stride1), if fullRes==True
                        then padding is added to the conv layers so that a full resolution output is generated.
        :return:
        """

        self.confModel = confModel

        logging.info("[CONF] Creating CNN model of type: %s" % str(confModel['modelType']))
        # Creates a CNN model based on the conf file
        if confModel['modelType'] == 'SimpleDeepmedic':
            self.segmTileBasedModel = CNNModel.CNNModel_SimpleDeepmedic(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_SimpleDeepmedic(confModel, True)
        elif confModel['modelType'] == 'SimpleDeepmedic2D':
            self.segmTileBasedModel = CNNModel.CNNModel_SimpleDeepmedic2D(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_SimpleDeepmedic2D(confModel, True)
        elif confModel['modelType'] == 'SimpleDeepmedic_3Paths':
            self.segmTileBasedModel = CNNModel.CNNModel_SimpleDeepmedic_3Paths(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_SimpleDeepmedic_3Paths(confModel, True)
        elif confModel['modelType'] == 'SimpleDeepmedic_Sandwich':
            self.segmTileBasedModel = CNNModel.CNNModel_SimpleDeepmedic_Sandwich(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_SimpleDeepmedic_Sandwich(confModel, True)
        elif confModel['modelType'] == 'SimpleDeepmedic_LearnedDownsampling':
            self.segmTileBasedModel = CNNModel.CNNModel_SimpleDeepmedic_LearnedDownsampling(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_SimpleDeepmedic_LearnedDownsampling(confModel, True)
        elif confModel['modelType'] == 'SimpleDeepmedic_Heteroscedastic':
            self.segmTileBasedModel = CNNModel.CNNModel_SimpleDeepmedic_Heteroscedastic(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_SimpleDeepmedic_Heteroscedastic(confModel, True)
        elif confModel['modelType'] == 'UNet2D_Sandwich':
            self.segmTileBasedModel = CNNModel.CNNModel_UNet2D_Sandwich(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_UNet2D_Sandwich(confModel, True)
        elif confModel['modelType'] == 'UNet3D':
            self.segmTileBasedModel = CNNModel.CNNModel_UNet3D(confModel, False)
            self.segmFullResModel = CNNModel.CNNModel_UNet3D(confModel, True)
        else:
            logging.info("[FATAL ERROR]: The type of CNN model specified in the confModel['modelType'] = '%s' is not implemented. Check Blast.createModel() for a list of available architectures." % confModel['modelType'])
            exit(2)

        # If there is a model initialization specified in the confFile, then the weights are loaded into the new model
        if 'modelInitialization' in confModel.keys():
            logging.info("[CONF] Initializing model with weights from: %s" % confModel['modelInitialization'])
            self.initializeModel(confModel['modelInitialization'])

    def train(self, confTrain, temporalOutputFilename=None):
        """
            It loads the training data specified in the conf file, and trains a CNN model using it.
            Training is tile based.

        :param confTrain: training configuration object
        :param temporalOutputName: filename specifying where we will store the partially trained model. If it is
                        not specified, then the partially trained model will be stored at confTraining['trainingSessionName']
        :return: trainingLoss, valLoss : It returns the last training and validation loss

        """
        assert (self.segmTileBasedModel is not None), "[ERROR] A valid CNN model must be created before training"
        self.confTrain = confTrain

        # Define the name of the file where the temporal training session will be stored while training
        if temporalOutputFilename is None:
            temporalOutputFilename = confTrain['trainingSessionName']

        # Print the configuration dictionaries
        if '__builtins__' in self.confTrain:
            del self.confTrain['__builtins__']
        if '__builtins__' in self.confModel:
            del self.confModel['__builtins__']

        logging.info("\nCONF MODEL: \n")
        logDictToInfoStream(self.confModel, "[CONF MODEL]")
        logging.info("\nCONF TRAIN: \n")
        logDictToInfoStream(self.confTrain, "[CONF TRAIN]")

        # Create and start the batch generation process for training
        validatePerSubepoch = not (self.confTrain['channelsValidation'] == []) and (self.confTrain['performPerSubepochValidation'])
        validateFullResolution = not (self.confTrain['channelsValidation'] == [])

        logging.info("[CONF] Validating per subepoch: " + str(validatePerSubepoch))
        logging.info("[CONF] Validating on full resolution after %d epochs: %s" % (self.confTrain['epochsPerformFullValEvery'], str(validateFullResolution)))

        # == Get the loss that will be used to guide the back propagation ==
        # Useful: https://github.com/Lasagne/Lasagne/issues/704
        self.predSegm = lasagne.layers.get_output(self.segmTileBasedModel.output)

        if self.confModel['modelType'] == 'SimpleDeepmedic_Heteroscedastic':

            self.predSegm_test = lasagne.layers.get_output(self.segmTileBasedModel.output[0], deterministic=True)
            # Calculate the loss
            #self.loss = lasagne.objectives.categorical_crossentropy_numerical_stab(self.pred, self.activeModel.gt)

            self.lossSegm = categorical_crossentropy_numerical_stab(self.predSegm[0], self.segmTileBasedModel.gt)
            for i in range (1, len(self.predSegm)):
                self.lossSegm += categorical_crossentropy_numerical_stab(self.predSegm[i], self.segmTileBasedModel.gt)

            self.lossSegm /= float(len(self.predSegm))

            # Add L2 regularization multiplied with the corresponding factor
            regL2 = self.confTrain['l2Factor'] * lasagne.regularization.regularize_network_params(self.segmTileBasedModel.output,
                                                                                                  lasagne.regularization.l2)
            self.lossSegm = self.lossSegm.mean() + regL2

            # == Get the parameters we will update
            params = lasagne.layers.get_all_params(self.segmTileBasedModel.output, trainable=True)

            # If it is a cascade model, then the wegihts corresponding to the input model will be removed
            logging.info("Trainable params before removing: %d " % len(params))
            for p in self.segmTileBasedModel.nonTrainableParameters:
                params.remove(p)
            logging.info("Trainable params after removing: %d " % len(params))

            # Learning rate that will be used by the optimizer
            self.learningRateSegm = theano.shared(np.array(self.confTrain['learningRate'], dtype=theano.config.floatX))

            # Create update expressions for training, using Stochastic Gradient
            # Descent (SGD) with Nesterov momentum.
            if  self.confTrain['optMethod'] == 'nesterov':
                updates = lasagne.updates.nesterov_momentum(self.lossSegm, params, learning_rate=self.learningRateSegm, momentum=0.9)
            elif self.confTrain['optMethod'] == 'adadelta':
                updates = lasagne.updates.adadelta(self.lossSegm, params, learning_rate=self.learningRateSegm)
            elif self.confTrain['optMethod'] == 'adam':
                updates = lasagne.updates.adam(self.lossSegm, params, learning_rate=self.learningRateSegm)

            # Create and start the batch generation process for training
            batchGenTraining = Simple3DBatchGenerator(confTrain, self.confModel, mode='training', infiniteLoop=False, maxQueueSize=confTrain['maxQueueSize'])
            batchGenTraining.generateBatches()

            # Compile the theano function
            logging.info("-- Compiling Theano functions ...")

            # Theano function used to train the model based on the loss function
            self.trainSegmFn = theano.function([self.segmTileBasedModel.inputVar, self.segmTileBasedModel.gt], self.lossSegm, updates=updates)

            # Theano function used to produce a prediction without computing the loss
            self.getPredFn = theano.function([self.segmTileBasedModel.inputVar], self.predSegm_test)

            # Theano function used to produce a full resolution prediction without computing the loss
            self.predFullResSegm_test = lasagne.layers.get_output(self.segmFullResModel.output[0], deterministic=True)
            self.getSegmUncertFullRes_test = lasagne.layers.get_output(self.segmFullResModel.net['sigmas'], deterministic=True)

            self.getSegmPredFullResFn = theano.function([self.segmFullResModel.inputVar], self.predFullResSegm_test)

            self.getSegmUncertFullResFn = theano.function([self.segmFullResModel.inputVar], self.getSegmUncertFullRes_test)

        else:
            # == Get the loss that will be used to guide the back propagation ==
            # Useful: https://github.com/Lasagne/Lasagne/issues/704
            self.predSegm = lasagne.layers.get_output(self.segmTileBasedModel.output)
            self.predSegm_test = lasagne.layers.get_output(self.segmTileBasedModel.output, deterministic=True)

            # Calculate the loss
            #self.loss = lasagne.objectives.categorical_crossentropy_numerical_stab(self.pred, self.activeModel.gt)

            self.lossSegm = categorical_crossentropy_numerical_stab(self.predSegm, self.segmTileBasedModel.gt)

            # Add L2 regularization multiplied with the corresponding factor
            regL2 = self.confTrain['l2Factor'] * lasagne.regularization.regularize_network_params(self.segmTileBasedModel.output,
                                                                                                  lasagne.regularization.l2)
            self.lossSegm = self.lossSegm.mean() + regL2

            # == Get the parameters we will update
            params = lasagne.layers.get_all_params(self.segmTileBasedModel.output, trainable=True)

            # If it is a cascade model, then the wegihts corresponding to the input model will be removed
            #if self.confModel['modelType'] == 'Cascade_SimpleDeepmedic':
            logging.info("Trainable params before removing: %d " % len(params))
            for p in self.segmTileBasedModel.nonTrainableParameters:
                params.remove(p)
            logging.info("Trainable params after removing: %d " % len(params))

            # Learning rate that will be used by the optimizer
            self.learningRateSegm = theano.shared(np.array(self.confTrain['learningRate'], dtype=theano.config.floatX))

            # Create update expressions for training, using Stochastic Gradient
            # Descent (SGD) with Nesterov momentum.
            if  self.confTrain['optMethod'] == 'nesterov':
                updates = lasagne.updates.nesterov_momentum(self.lossSegm, params, learning_rate=self.learningRateSegm, momentum=0.9)
            elif self.confTrain['optMethod'] == 'adadelta':
                updates = lasagne.updates.adadelta(self.lossSegm, params, learning_rate=self.learningRateSegm)
            elif self.confTrain['optMethod'] == 'adam':
                updates = lasagne.updates.adam(self.lossSegm, params, learning_rate=self.learningRateSegm)

            # Create and start the batch generation process for training
            batchGenTraining = Simple3DBatchGenerator(confTrain, self.confModel, mode='training', infiniteLoop=False, maxQueueSize=confTrain['maxQueueSize'])
            batchGenTraining.generateBatches()

            # Compile the theano function
            logging.info("-- Compiling Theano functions ...")

            # Theano function used to train the model based on the loss function
            self.trainSegmFn = theano.function([self.segmTileBasedModel.inputVar, self.segmTileBasedModel.gt], self.lossSegm, updates=updates)

            # Theano function used to produce a prediction without computing the loss
            self.getPredFn = theano.function([self.segmTileBasedModel.inputVar], self.predSegm_test)

            # Theano function used to produce a full resolution prediction without computing the loss
            self.predFullResSegm_test = lasagne.layers.get_output(self.segmFullResModel.output, deterministic=True)
            self.getSegmPredFullResFn = theano.function([self.segmFullResModel.inputVar], self.predFullResSegm_test)

        if validatePerSubepoch:
            batchGenValidation = Simple3DBatchGenerator(confTrain, self.confModel, mode='validation', infiniteLoop=False, maxQueueSize=6)
            batchGenValidation.generateBatches()

        # Create an 'stats' dict that will be stored after every epoch to facilitate data analysis
        self.stats['trainingLossBySubepoch']  = []
        self.stats['trainingLossByBatch'] = []
        self.stats['indepTrainingLossByBatch'] = []

        self.stats['metricsTrainingBySubepoch'] = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))
        self.stats['metricsValidationBySubepoch'] = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))
        self.stats['metricsValidationFullVolumesAvg'] = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))
        self.stats['metricsValidationFullVolumesStd'] = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))

        # Variable to control the losses
        minLoss = 10000
        subepochsNotImprovingLoss = 0

        # Add saving loss val and training
        logging.info("-- Start training ...")
        for epoch in range(self.confTrain['numEpochs']):
            #print ('Epoch %d' % epoch)
            # Clean the memory before starting the epoch
            gc.collect()

            trainingError = 0.0
            batchNum = 1
            logging.info("-- Epoch: " + str(epoch))

            if self.confTrain['adaptLearningRateStrategy'] == 'scheduled':
                if epoch in self.confTrain['halfLearningRateAtEpoch']:
                    #Then the learning rate is decreased to the half
                    newLearningRate = np.divide(self.learningRateSegm.get_value(), 2)
                    self.learningRateSegm.set_value(np.array(newLearningRate, dtype=theano.config.floatX))
                    logging.info("------ [CONF] New learning rate is set to: %f" % newLearningRate)

            if epoch == 0:
                self.updateFullResModelParameters()
                #self.fullInferenceOnTrainingData(self.confTrain, "EP" + str(epoch) + "_")

            for subepoch in range(self.confTrain['numSubepochs']):
                batchesPerSubepoch = self.confTrain['numTrainingSegmentsLoadedOnGpuPerSubep'] // self.confTrain['batchSizeTraining']
                logging.info("---- Sub epoch: (%d, %d)" % (epoch, subepoch))
                inputs, gt = 0, 0

                for batch in range(batchesPerSubepoch):
                    gc.collect()
                    # Retrieves the batch
                    #logging.info("---- Batches in training queue before getting batch: %d" % batchGenTraining.getNumBatchesInQueue())

                    inputs, gt = batchGenTraining.getBatch()

                    #logging.info("--- Number of batches in the queue: %d" % batchGenTraining.getNumBatchesInQueue())

                    # Call the training function
                    indepTrainingError = self.trainSegmFn(inputs, gt)

                    trainingError += indepTrainingError
                    self.stats['trainingLossByBatch'].append(trainingError / batchNum)
                    self.stats['indepTrainingLossByBatch'].append(indepTrainingError)

                    batchNum += 1

                # Run the info in the validation dataset
                logging.info("------ SUBEPOCH TRAINING loss:\t\t{:.6f}".format(trainingError / batchNum))

                # Compute predictions for last training batch
                logging.info("------ SUBEPOCH TRAINING stats (last batch):")
                pred = self.getPredFn(inputs)
                metrics = MetricsMonitor.getBatchMetrics(pred, gt)

                # Print the statistics to the log
                logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t]")
                for i in range(metrics.shape[1]):
                    logging.info("Label %d : %s" % (self.confModel["labels"][i], str(metrics[0,i,:])))

                # Save the metrics by subepoch
                self.stats['metricsTrainingBySubepoch'] = np.concatenate((self.stats['metricsTrainingBySubepoch'], metrics))

                # Compute predictions and matrics for the validation batch if provided
                if validatePerSubepoch:
                    #logging.info(
                    #    "---- Batches in validation queue before getting batch: %d" % batchGenValidation.getNumBatchesInQueue())
                    valInputs, valGt = batchGenValidation.getBatch()
                    pred = self.getPredFn(valInputs)

                    valMetrics = MetricsMonitor.getBatchMetrics(pred, valGt)
                    self.stats['metricsValidationBySubepoch'] = np.concatenate((self.stats['metricsValidationBySubepoch'], valMetrics))

                    logging.info("------ SUBEPOCH VALIDATION stats:")
                    # Print the statistics to the log
                    logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t]")
                    for i in range(valMetrics.shape[1]):
                        logging.info("Label %d : %s" % (self.confModel["labels"][i], str(valMetrics[0, i, :])))

                if (trainingError / batchesPerSubepoch) < minLoss:
                    minLoss = trainingError / batchesPerSubepoch
                    subepochsNotImprovingLoss = 0
                else:
                    subepochsNotImprovingLoss += 1

                self.stats['trainingLossBySubepoch'].append(trainingError / batchesPerSubepoch)

                sys.stdout.flush()

                # If there are 3 subepochs in a row in which no loss improvement is achieved ...
                if (self.confTrain['adaptLearningRateStrategy'] == 'adaptive') and subepochsNotImprovingLoss >= 3:
                    #Then the learning rate is decreased to the half
                    newLearningRate = np.divide(self.learningRateSegm.get_value(), 2)
                    self.learningRateSegm.set_value(np.array(newLearningRate, dtype=theano.config.floatX))

                    logging.info("------ [CONF] New learning rate is set to: %f" % newLearningRate)

                # Update the parameters of the full resolution model (used for full volume segmentation) using
                # with the parameters of the learned model

            if confTrain['epochsSaveTrainedModelEvery'] != -1 and epoch % confTrain['epochsSaveTrainedModelEvery'] == 0:
                logging.info("------ Saving partially trained model to: " + temporalOutputFilename)
                self.saveModel(temporalOutputFilename)

            # Check if we must perform validation on the full volumes in this epoch
            if validateFullResolution and ((epoch % self.confTrain['epochsPerformFullValEvery'] == 0) or (epoch == self.confTrain['numEpochs']-1)):
                logging.info("")
                logging.info("============================================================")
                logging.info("======== INFERENCE ON COMPLETE VALIDATION VOLUMES ========")
                logging.info("============================================================")

                # If we are not in the last epoch, then no probability map neither segmentation maps are saved in disk
                if epoch == self.confTrain['numEpochs']-1:
                    saveProbMapsForEachClass = self.confTrain['saveProbMapsForEachClassVal']
                    saveSegmentation = self.confTrain['saveSegmentationVal']
                    generateVisualizationCommandsFile = True
                else:
                    saveProbMapsForEachClass = [False for x in self.confModel['labels']]
                    saveSegmentation = False
                    generateVisualizationCommandsFile = False

                # Update the parameters of the full resolution model (used for full volume segmentation) using
                # with the parameters of the learned model
                self.updateFullResModelParameters()

                avgMetricsFullVolVal, stdMetricsFullVolVal, allMetrics = self.inference(inputChannelsFiles = self.confTrain['channelsValidation'],
                               inputGtLabels = self.confTrain['gtLabelsValidation'],
                               inputRoiMasks = self.confTrain['roiMasksValidation'] if ('roiMasksValidation' in self.confTrain) else None,
                               namesForPredictionsPerCase = self.confTrain['namesForPredictionsPerCaseValidation'] if ('namesForPredictionsPerCaseValidation' in self.confTrain) else None,
                               outputDir = os.path.join(self.confTrain["folderForOutput"], "val/"),
                               saveProbMapsForEachClass = saveProbMapsForEachClass,
                               saveSegmentation = saveSegmentation,
                               generateVisualizationCommandsFile = generateVisualizationCommandsFile)
                logging.info("")


                avgMetricsFullVolVal = np.reshape(avgMetricsFullVolVal, (1, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))
                self.stats['metricsValidationFullVolumesAvg'] = np.concatenate((self.stats['metricsValidationFullVolumesAvg'], avgMetricsFullVolVal))

                stdMetricsFullVolVal = np.reshape(stdMetricsFullVolVal,(1, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))
                self.stats['metricsValidationFullVolumesStd'] = np.concatenate((self.stats['metricsValidationFullVolumesStd'], stdMetricsFullVolVal))

                # ALL METRICS always contains all the predictions for the last validation case.
                self.stats['allMetrics'] = allMetrics

        logging.info("------ DONE Training finished in (secs): ")
#        toc()

        return minLoss

    def crop(self, img, roi):
        """
            Crops the image in the area corresponding to the smallest cube that contains the ROI.
        """
        minCoords = np.min(np.argwhere(roi == 1), axis=0)
        maxCoords = np.max(np.argwhere(roi == 1), axis=0)

        res = img[minCoords[0]:maxCoords[0], minCoords[1]:maxCoords[1], minCoords[2]:maxCoords[2]]

        #print "Cropped img dimensions: %s" % str(res.shape)
        return res

    def cropRoiAndMakeItMultipleOf(self, img, roi, multipleOf=[3,3,3]):
        """
            Crops the image in the area corresponding to the smallest cube that contains the ROI,
            and guarantees that the size of the cropped image is multiple of a given number.
            If we have to pad the image to make it multiple of the given number, then the returned tuple "validShape"
            indicates the area in the cropped image that actually corresponds to img data and not to padded values.

            It returns:

            croppedImg: img cropped with the desired size
            minCoords: bottom-left corner of the cropped image in the original img coordinates
            maxCoords: top-right corner of the cropped image in the original img coordinates
            validShape: part of the croppedImg that actually corresponds to img data and not to padded values.
        """
        minCoords = np.min(np.argwhere(roi == 1), axis=0)
        maxCoords = np.max(np.argwhere(roi == 1), axis=0)

        size = maxCoords - minCoords

        extraSize = np.array([0 if size[0] % multipleOf[0] == 0 else multipleOf[0] - (size[0] % multipleOf[0]),
                              0 if size[1] % multipleOf[1] == 0 else multipleOf[1] - (size[1] % multipleOf[1]),
                              0 if size[2] % multipleOf[2] == 0 else multipleOf[2] - (size[2] % multipleOf[2])])

        maxCoords = maxCoords + extraSize
        res = img[minCoords[0]:maxCoords[0], minCoords[1]:maxCoords[1], minCoords[2]:maxCoords[2]]

        validShape = res.shape[:]

        if (res.shape[0] % multipleOf[0] != 0) or (res.shape[1] % multipleOf[1] != 0) or (res.shape[2] % multipleOf[2] != 0):
            res = self.padVolumeToMakeItMultipleOf(res, multipleOf)

        logging.debug("---- Cropped img dimensions: %s" % str(res.shape))
        return res, minCoords, maxCoords, validShape

    def padVolumeToMakeItMultipleOf(self, v, multipleOf=[3,3,3], mode='symmetric'):

        padding = ((0, 0 if v.shape[0] % multipleOf[0] == 0 else multipleOf[0] - (v.shape[0] % multipleOf[0])),
                   (0, 0 if v.shape[1] % multipleOf[1] == 0 else multipleOf[1] - (v.shape[1] % multipleOf[1])),
                   (0, 0 if v.shape[2] % multipleOf[2] == 0 else multipleOf[2] - (v.shape[2] % multipleOf[2])))

        return np.pad(v, padding, mode)

    def segmentWithRoiEnsemble(self, imgChannels, roi, returnOriginalSize=True):
        """
            Segment only the area contained in the ROI, using an Ensemble of networks. This methods saves memory when compared to segmenting the complete image.

            If returnOriginalSize==True, then the area out of the ROI will be assigned to the label corresponding to the output channel 0
            and the returned probability maps will have the same dimensions than the original image channels.

            If returnOriginalSize==False, then the method will return only the probability corresponding to the cropped area around the ROI.

            imgChannels is a list of list of images, indexed as: imgChannels[MODEL][CHANNEL], i.e. it has different images per method.
        """
        assert len(self.ensembleNetworks) == len(imgChannels), "ERROR: Number of ensembles (%i) should be equal to number of images (%i)" % (len(self.ensembleNetworks), len(imgChannels))

        numEnsembles = len(self.ensembleNetworks)
        numChannels = len(imgChannels[0])

        for e in range(numEnsembles):

            for i in range(1, numChannels):
                assert imgChannels[e][0].shape == imgChannels[e][i].shape, "ERROR: All channels must have the same resolution"

            # MODIFY IF A NEW MODEL IS ADDED
            if self.confModel['modelType'] == 'SimpleDeepmedic2D':
                # Crop the first image channel to use it as a reference
                croppedImg0, minCoords, maxCoords, validShape = self.cropRoiAndMakeItMultipleOf(imgChannels[e][0], roi,
                                                                                                multipleOf=[3, 3, 1])
            elif self.confModel['modelType'] == 'UNet2D_Sandwich':
                croppedImg0, minCoords, maxCoords, validShape = self.cropRoiAndMakeItMultipleOf(imgChannels[e][0], roi,
                                                                                                multipleOf=[8, 8, 1])
            elif self.confModel['modelType'] == 'UNet3D':
                croppedImg0, minCoords, maxCoords, validShape = self.cropRoiAndMakeItMultipleOf(imgChannels[e][0], roi,
                                                                                           multipleOf=[8, 8, 8])
            elif self.confModel['modelType'] == 'SimpleDeepmedic_3Paths':
                croppedImg0, minCoords, maxCoords, validShape = self.cropRoiAndMakeItMultipleOf(imgChannels[e][0], roi,
                                                                                           multipleOf=[8, 8, 8])
            else:
                # Crop the first image channel to use it as a reference
                croppedImg0, minCoords, maxCoords, validShape = self.cropRoiAndMakeItMultipleOf(imgChannels[e][0], roi)

            # If it's the first ensemble, initializes the container with 0s
            if e == 0:
                croppedProbMaps = np.zeros(shape=(1, len(self.confModel['labels']), croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),dtype=np.float32)

            # Creates the theano-compatible data structure
            data = np.ndarray(shape=(1, numChannels, croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),
                          dtype=np.float32)

            # Fill the data structure with the cropped data
            data[0, 0, :, :, :] = croppedImg0
            # TODO: CHECK HERE, IT WONT WORK IF MULTICHANNEL IS PROVIDED
            for channel in range(1, numChannels):
                data[0, channel, :, :, :] = imgChannels[e][channel]

            if self.confModel['modelType'] == 'SimpleDeepmedic2D':
                # Initialize the croppedProbMaps structure
                croppedProbMaps = np.ndarray(shape=(
                1, len(self.confModel['labels']), croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),
                                             dtype=np.float32)

                # Iterate for every plane in the data structure, along the last axis
                for i in range(data.shape[4]):
                        aux = self.ensembleNetworks[e].getSegmPredFullResFn(np.reshape(data[0, :, :, :, i],
                                                                                       newshape=(1, data.shape[1],
                                                                                                 data.shape[2],
                                                                                                 data.shape[3], 1)))
                        croppedProbMaps[0, :, :, :, i] += np.squeeze(aux)

            else:
                croppedProbMaps += self.ensembleNetworks[e].getSegmPredFullResFn(data)



        croppedProbMaps /= float(len(self.ensembleNetworks))

        # Reconstruct the cropped maps in the original resolution
        finalProbMaps = np.ndarray(shape=( 1, len(self.confModel['labels']), imgChannels[0][0].shape[0], imgChannels[0][0].shape[1],
            imgChannels[0][0].shape[2]), dtype=np.float32)

        if returnOriginalSize:
            for i in range(len(self.confModel['labels'])):
                # Assign the corresponding probabilities to the labels out of the ROI
                if i == 0:
                    finalProbMaps[0, i, :, :, :] = np.ones(shape=imgChannels[0][0].shape)
                else:
                    finalProbMaps[0, i, :, :, :] = np.zeros(shape=imgChannels[0][0].shape)

                # Overwrite the probabilities of the images into the ROI
                finalProbMaps[0, i, minCoords[0]:maxCoords[0], minCoords[1]:maxCoords[1],
                minCoords[2]:maxCoords[2]] = croppedProbMaps[0, i, :validShape[0], :validShape[1], :validShape[2]]

            return finalProbMaps
        else:
            return croppedProbMaps

    def _cropImageToSegmentAccordingToModel(self, img, roi):
        # MODIFY IF A NEW MODEL IS ADDED
        if self.confModel['modelType'] == 'SimpleDeepmedic2D':
            # Crop the first image channel to use it as a reference
            return self.cropRoiAndMakeItMultipleOf(img, roi, multipleOf=[3,3,1])
        elif self.confModel['modelType'] == 'UNet2D_Sandwich':
            return self.cropRoiAndMakeItMultipleOf(img, roi, multipleOf=[8, 8, 1])
        elif self.confModel['modelType'] == 'UNet3D':
            return self.cropRoiAndMakeItMultipleOf(img, roi, multipleOf=[8, 8, 8])
        elif self.confModel['modelType'] == 'SimpleDeepmedic_3Paths':
            return self.cropRoiAndMakeItMultipleOf(img, roi, multipleOf=[4, 4, 4])
        else:
            return self.cropRoiAndMakeItMultipleOf(img, roi)

    def segmentWithRoi(self, imgChannels, roi, returnOriginalSize = True, computeUncert = 0):
        """
            Segment only the area contained in the ROI. This methods saves memory when compared to segmenting the complete image.

            If returnOriginalSize==True, then the area out of the ROI will be assigned to the label corresponding to the output channel 0
            and the returned probability maps will have the same dimensions than the original image channels.

            If returnOriginalSize==False, then the method will return only the probability corresponding to the cropped area around the ROI.

            if ensembleNetworks are not used, imgChannels is a list of images where every image represet a channel: imgChannels[CHANNEL]

            if ensembleNetworks are used, imgChannels is a list of list of images, indexed as: imgChannels[MODEL][CHANNEL], i.e. it has different
                images per method.

            computeUncert: Type of uncertainty to be estimated. Options are:
                        0 : Not unceraintanty
                        1 : Heteroscedastic Uncertainty. Only can be used if the model type is SimpleDeepmedic_Heteroscedastic. If used, the method returns the probabilities and the aleatoric uncertainties.
                        2 : Epistemic and aleatoric uncertainty obtained by repetead non-deterministic inference (using Dropout) computed as indicated in Kendal paper. IMPORTANT: The test model must be compiled using Deterministic=False for the uncertainty to make sense.
        """

        useEnsemble = len(self.ensembleNetworks) > 0

        numChannels = len(imgChannels)

        if computeUncert == 1:
            assert self.confModel['modelType'] == 'SimpleDeepmedic_Heteroscedastic', "ERROR: To compute uncertainty only the model SimpleDeepmedic_Heteroscedastic can be used"
        elif computeUncert == 2:
            assert self.confModel['modelType'] != 'SimpleDeepmedic_Heteroscedastic' and not useEnsemble, "ERROR: To compute uncertainty using Dropout the model must be different from SimpleDeepmedic_Heteroscedastic and can not be an ensemble"

        for i in range(1, numChannels):
            assert imgChannels[0].shape == imgChannels[i].shape, "ERROR: All channels must have the same resolution"

        # Crops the image around the ROI, and makes it compatible with the size required by the segmentation model
        croppedImg0, minCoords, maxCoords, validShape = self._cropImageToSegmentAccordingToModel(imgChannels[0], roi)

        # Creates the theano-compatible data structure
        data = np.ndarray(shape=(1, numChannels, croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),
                      dtype=np.float32)

        # Fill the data structure with the cropped data
        data[0, 0, :, :, :] = croppedImg0
        #TODO: CHECK HERE, IT WONT WORK IF MULTICHANNEL IS PROVIDED

        for channel in range(1, numChannels):
            croppedImg, _, _, _ = self._cropImageToSegmentAccordingToModel(imgChannels[channel], roi)
            data[0,channel,:,:,:] = croppedImg

        # Segment the images
        # croppedProbMaps = self.getPredFn(data)
#        tic()
        if self.confModel['modelType'] == 'SimpleDeepmedic2D':
            # Initialize the croppedProbMaps structure
            croppedProbMaps = np.ndarray(shape=(1, len(self.confModel['labels']), croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),
                          dtype=np.float32)

            # Iterate for every plane in the data structure, along the last axis
            for i in range(data.shape[4]):
                # Obtain the segmentation for the given plane and store it in the croppedProbMaps
                if useEnsemble:
                    for n in range(len(self.ensembleNetworks)):
                        aux = self.ensembleNetworks[n].getSegmPredFullResFn(np.reshape(data[0,:,:,:,i], newshape=(1,data.shape[1], data.shape[2], data.shape[3], 1)))
                        croppedProbMaps[0,:,:,:,i] += np.squeeze(aux)
                else:
                    aux = self.getSegmPredFullResFn(np.reshape(data[0,:,:,:,i], newshape=(1,data.shape[1], data.shape[2], data.shape[3], 1)))
                    croppedProbMaps[0,:,:,:,i] = np.squeeze(aux)

            if useEnsemble:
                croppedProbMaps /= float(len(self.ensembleNetworks))

        else:
            if useEnsemble:
                croppedProbMaps = np.zeros(shape=(1, len(self.confModel['labels']), croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),dtype=np.float32)
                for n in range(len(self.ensembleNetworks)):
                    croppedProbMaps+= self.ensembleNetworks[n].getSegmPredFullResFn(data)
                croppedProbMaps /= float(len(self.ensembleNetworks))

            # Compute uncertainty and predictions doing inference several times
            elif computeUncert == 2:
                numDropoutInference = 10

                croppedProbMaps = np.zeros(shape=(1, len(self.confModel['labels']), croppedImg0.shape[0], croppedImg0.shape[1], croppedImg0.shape[2]),dtype=np.float32)

                for n in range(numDropoutInference):
                    croppedProbMaps+= self.getSegmPredFullResFn(data)
                croppedProbMaps /= float(numDropoutInference)
            else:
                croppedProbMaps = self.getSegmPredFullResFn(data)

#        toc()

        # Reconstruct the cropped maps in the original resolution
        finalProbMaps = np.ndarray(shape=(1, len(self.confModel['labels']), imgChannels[0].shape[0], imgChannels[0].shape[1], imgChannels[0].shape[2]), dtype=np.float32)

        # Heteroscedastic Uncertainty
        if computeUncert == 1:
            croppedUncertainty = self.getSegmUncertFullResFn(data)
        elif computeUncert == 2:
            croppedUncertainty = scipy.stats.entropy(np.squeeze(croppedProbMaps))
            croppedUncertainty = croppedUncertainty.reshape(1, 1, croppedUncertainty.shape[0], croppedUncertainty.shape[1], croppedUncertainty.shape[2])

        if returnOriginalSize:
            for i in range(len(self.confModel['labels'])):
                # Assign the corresponding probabilities to the labels out of the ROI
                if i == 0:
                    finalProbMaps[0, i,:,:,:] = np.ones(shape=imgChannels[0].shape)
                else:
                    finalProbMaps[0, i,:,:,:] = np.zeros(shape=imgChannels[0].shape)

                # Overwrite the probabilities of the images into the ROI
                finalProbMaps[0, i, minCoords[0]:maxCoords[0], minCoords[1]:maxCoords[1], minCoords[2]:maxCoords[2]] = croppedProbMaps[0,i,:validShape[0],:validShape[1],:validShape[2]]

            if computeUncert != 0:
                finalUncert = np.zeros(shape = (finalProbMaps.shape))

                finalUncert[0,0,minCoords[0]:maxCoords[0], minCoords[1]:maxCoords[1],minCoords[2]:maxCoords[2]] = croppedUncertainty[0, 0, :validShape[0], :validShape[1], :validShape[2]]
                return finalProbMaps, finalUncert
            else:
                return finalProbMaps
        else:
            if computeUncert != 0:
                return croppedProbMaps, croppedUncertainty
            # Dropout-based uncertainty
#                return croppedProbMaps

    def segment(self, imgChannels):
        """
            Segment an image given the image channels.

                    """
        numChannels = len(imgChannels)

        paddedChannels = []

        for i in range(0, numChannels):
            assert imgChannels[0].shape == imgChannels[i].shape, "ERROR: All channels must have the same resolution"
            paddedChannels.append(self.padVolumeToMakeItMultipleOf(imgChannels[i]))

        # Creates the theano-compatible data structure
        data = np.ndarray(shape=(1, numChannels, paddedChannels[0].shape[0], paddedChannels[0].shape[1], paddedChannels[0].shape[2]),
                      dtype=np.float32)

        # Fill the data structure with the cropped data
        for channel in range(numChannels):
            data[0,channel,:,:,:] = paddedChannels[channel]

        # Segment the images

        finalProbMaps = self.getSegmPredFullResFn(data)

        print "Prediction res: %s" % str(finalProbMaps.shape)
        print "Original res: %s" % str(imgChannels[0].shape)

        return finalProbMaps[:,:,0:imgChannels[0].shape[0], 0:imgChannels[0].shape[1], 0:imgChannels[0].shape[2]]

    def saveModel(self, outputFile = None):
        if self.segmTileBasedModel is not None:
            if outputFile is None:
                outputFile = self.confModel['modelName'] + ".p"

            out = {}

            #out['updatesParams'] = [p.get_value() for p in self.updates.keys()]
            out['weightsParams'] = lasagne.layers.get_all_param_values(self.segmTileBasedModel.output)
            out['weightsParamsFullRes'] = lasagne.layers.get_all_param_values(self.segmFullResModel.output)

            #if self.
            # Workaround to be able to pickle the confModel.
            if '__builtins__' in self.segmTileBasedModel.confModel:
                del self.segmTileBasedModel.confModel['__builtins__']

            out['confModel'] = self.segmTileBasedModel.confModel
            out['stats'] = self.stats

            if self.confTrain is not None:
                if '__builtins__' in self.confTrain:
                    del self.confTrain['__builtins__']

            if self.confTest is not None:
                if '__builtins__' in self.confTest:
                    del self.confTest['__builtins__']

            out['confTrain'] = self.confTrain
            out['confTest'] = self.confTest

            pickle.dump(out, open(outputFile, "wb"))

        elif not (self.ensembleNetworks == []):
            logging.info("Saving ensemble testing stats (no model is saved)...")

            if outputFile is None:
                outputFile = confModel['modelName'] + ".p"

            out = {}

            # out['updatesParams'] = [p.get_value() for p in self.updates.keys()]
            # Workaround to be able to pickle the confModel.
            if '__builtins__' in self.confModel:
                del self.confModel['__builtins__']

            out['confModel'] = self.confModel

            if self.confTrain is not None:
                if '__builtins__' in self.confTrain:
                    del self.confTrain['__builtins__']

            if self.confTest is not None:
                if '__builtins__' in self.confTest:
                    del self.confTest['__builtins__']

            out['confTrain'] = self.confTrain
            out['confTest'] = self.confTest

            out['stats'] = self.stats

            pickle.dump(out, open(outputFile, "wb"))
        else:
            logging.warning("No model was saved because it was not initialized ")


    def loadModel(self, inputFile, compileDeterministic=True): #, fullRes = False):
        """
            This method loads a new model from a pickle file, creating a new model, initializing it with the stored weights and
            compiling the functions necessary for testing it.

            If you only want to initilize a model that has been created, then use 'initializeModel'

            :param inputFile: the pickle file which contains the model.
            :return:
        """

        # Load the pickled model
        dump = pickle.load(open(inputFile, "rb"))

        # Initializes an empty model using the pickled model configuration
        self.createModel(dump['confModel'])# , fullRes)

        #for p, value in zip(updates.keys(), values1):
        #p.set_value(value)
        lasagne.layers.set_all_param_values(self.segmTileBasedModel.output, dump['weightsParams'])

        self.stats = dump['stats']

        # == Validation loss ==

        logging.info("Compiling Theano validation functions (Deterministic = %s )..." % str(compileDeterministic))

        if self.confModel['modelType'] == 'SimpleDeepmedic_Heteroscedastic':
            self.predSegm = lasagne.layers.get_output(self.segmTileBasedModel.output[0], deterministic=True)
            self.predFullResSegm = lasagne.layers.get_output(self.segmFullResModel.output[0], deterministic=True)

            self.getSegmUncertFullRes_test = lasagne.layers.get_output(self.segmFullResModel.net['sigmas'], deterministic=True)
            self.getSegmUncertFullResFn = theano.function([self.segmFullResModel.inputVar], self.getSegmUncertFullRes_test)
        else:
            self.predSegm = lasagne.layers.get_output(self.segmTileBasedModel.output, deterministic=compileDeterministic)
            self.predFullResSegm = lasagne.layers.get_output(self.segmFullResModel.output, deterministic=compileDeterministic)

        self.getPredFn = theano.function([self.segmTileBasedModel.inputVar], self.predSegm)
        self.getSegmPredFullResFn = theano.function([self.segmFullResModel.inputVar], self.predFullResSegm)

        # Update the full resolution model parameters
        self.updateFullResModelParameters()

    def initializeModel(self, inputFile): #, fullRes = False):
        """
            This method initilizes a model that has been already created, using the weights stored in the pickled model.

            If you only want to load a new model from scratch, you should use 'loadModel'.

            :param inputFile: the pickle file which contains the model.
            :return:
        """

        # Load the pickled model
        dump = pickle.load(open(inputFile, "rb"))

        # Initialize the model
        lasagne.layers.set_all_param_values(self.segmTileBasedModel.output, dump['weightsParams'])

        # Update the full resolution model parameters
        self.updateFullResModelParameters()

    def loadEnsembleModel(self, trainedModelFilenames, fullRes = False):
        """
        :param trainedModelFilenames: List containing the filename of independent models that will constitute the ensemble
        :param fullRes: Indicates if the models are loaded to produce full resolution images when doing inference
        :return:
        """
        for i in range(0, len(trainedModelFilenames)):
            regCNN = Blast()

            # Initialize the model and set the conf parameters
            print "Loading ensemble trained model nbr:"+ str(i) +  ": " + trainedModelFilenames[i]
            regCNN.loadModel(trainedModelFilenames[i])
            self.ensembleNetworks.append(regCNN)

        self.confModel = self.ensembleNetworks[0].confModel

        self.stats = self.ensembleNetworks[0].stats

    def updateFullResModelParameters(self):
        """
            Updates the full resolution model parameters with the ones from the active model.
        """
        lasagne.layers.set_all_param_values(self.segmFullResModel.output, lasagne.layers.get_all_param_values(self.segmTileBasedModel.output))

    def fullInferenceOnTrainingData(self, confTrain, prefixForPredictions = ""):
        """
            It runs does full inference in the training dataset and stores de volumes.
        """
        _, _, _ = self.inference(inputChannelsFiles= confTrain['channelsTraining'],
                       inputGtLabels= confTrain['gtLabelsTraining'],
                       inputRoiMasks= confTrain['roiMasksTraining'] if ('roiMasksTraining' in confTrain) else None,
                       namesForPredictionsPerCase= None,
                       outputDir = os.path.join(confTrain["folderForOutput"], "fullInferenceOnTraining/"),
                       saveProbMapsForEachClass = [confTrain['saveSegmentationAndProbMapsFullResTraining'] for c in range(len(self.confModel["labels"]))],
                       saveSegmentation = confTrain['saveSegmentationAndProbMapsFullResTraining'],
                       prefixForPredictions = prefixForPredictions,
                       computeMetrics = False,
                       saveOriginalSize = False)

    def test(self, confTest):
        """
            It runs the complete test. It supposes that a model has already been loaded (by training or calling "laodModel".
        """

        # Print the configuration dictionaries
        if '__builtins__' in confTest:
            del confTest['__builtins__']

        logging.info("\nCONF TEST: \n")
        logDictToInfoStream(confTest, "[CONF TEST]")

        if 'originalResGtLabelsTest' in confTest.keys():

            avgMetricsFullVolVal, stdMetricsFullVolVal, allMetrics, avgMetricsOrigResFullVolVal, stdMetricsOrigResFullVolVal, allMetricsOrigResFullVolVal = self.inference(inputChannelsFiles= confTest['channelsTest'],
                            inputGtLabels= confTest['gtLabelsTest'],
                            inputRoiMasks= confTest['roiMasksTest'] if ('roiMasksTest' in confTest) else None,
                            namesForPredictionsPerCase= confTest['namesForPredictionsPerCaseTest'] if ('namesForPredictionsPerCaseTest' in confTest) else None,
                            outputDir = confTest["folderForOutput"],
                            saveProbMapsForEachClass = confTest['saveProbMapsForEachClass'],
                            saveSegmentation = confTest['saveSegmentation'],
                            calculateDistances = confTest['calculateDistances'],
                            originalResGtLabels = confTest['originalResGtLabelsTest'],
                            generateVisualizationCommandsFile = confTest['generateVisualizationCommandsFile'] if ('generateVisualizationCommandsFile' in confTest) else False,
                            computeDropoutUncert = True if "computeEpistemicUncertainty" in confTest.keys() and confTest["computeEpistemicUncertainty"]==True else False)

        else: # namesForPredictionsPerCaseVal
            avgMetricsFullVolVal, stdMetricsFullVolVal, allMetrics = self.inference(inputChannelsFiles=confTest['channelsTest'],
                            inputGtLabels=confTest['gtLabelsTest'],
                            inputRoiMasks=confTest['roiMasksTest'] if ('roiMasksTest' in confTest) else None,
                            namesForPredictionsPerCase=confTest['namesForPredictionsPerCaseTest'] if ('namesForPredictionsPerCaseTest' in confTest) else None,
                            outputDir=confTest["folderForOutput"],
                            saveProbMapsForEachClass=confTest['saveProbMapsForEachClass'],
                            saveSegmentation=confTest['saveSegmentation'],
                            calculateDistances=confTest['calculateDistances'],
                            generateVisualizationCommandsFile = confTest['generateVisualizationCommandsFile'] if ('generateVisualizationCommandsFile' in confTest) else False,
                            computeDropoutUncert = True if "computeEpistemicUncertainty" in confTest.keys() and confTest["computeEpistemicUncertainty"] == True else False,
                            extraMetricsForMixedLabels = confTest['extraMetricsForMixedLabels'] if ('extraMetricsForMixedLabels' in confTest) else [])

        avgMetricsFullVolVal = np.reshape(avgMetricsFullVolVal, (1, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))

        stdMetricsFullVolVal = np.reshape(stdMetricsFullVolVal, (1, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))

        if avgMetricsFullVolVal is not None:
            self.stats['metricsValidationFullVolumesAvg'] = avgMetricsFullVolVal

        if stdMetricsFullVolVal is not None:
            self.stats['metricsValidationFullVolumesStd'] = stdMetricsFullVolVal

        self.stats['allMetrics'] = allMetrics

        if 'originalResGtLabelsTest' in confTest.keys():
            avgMetricsOrigResFullVolVal = np.reshape(avgMetricsOrigResFullVolVal, (1, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))

            stdMetricsOrigResFullVolVal = np.reshape(stdMetricsOrigResFullVolVal, (1, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))

            if avgMetricsFullVolVal is not None:
                self.stats['metricsValidationFullVolumesAvgOrigRes'] = avgMetricsOrigResFullVolVal

            if stdMetricsFullVolVal is not None:
                self.stats['metricsValidationFullVolumesStdOrigRes'] = stdMetricsOrigResFullVolVal

            self.stats['allMetricsOrigRes'] = allMetricsOrigResFullVolVal

    def inference(self, inputChannelsFiles, inputGtLabels, inputRoiMasks, namesForPredictionsPerCase, outputDir, saveProbMapsForEachClass, saveSegmentation, prefixForPredictions = '', computeMetrics = True, saveOriginalSize=True, calculateDistances=False, originalResGtLabels=None, generateVisualizationCommandsFile=False, generateProbMapsFilenames=False, computeDropoutUncert=False, extraMetricsForMixedLabels = []):
        """
            It infers the probability maps and calculate the metrics for the given files.

            :param inputChannelsFiles: list of path files containing the path to the filenames for every channel (size: numChannels).
            :param inputGtLabels: path to the file containing the GT filenames
            :param inputRoiMasks: [Optional] path to the file containing the ROI masks (or None if no mask will be used for segmentation)
            :param namesForPredictionsPerCase: [Optional] path to the file containing the names that will be used to generate the predictions (or None if not provided).
            :param outputDir: [Optional, only needed if (True in saveProbMapsForEachClass) or saveSegmentation] folder where the output probability maps and segmentations will be stored.
            :param saveProbMapsForEachClass: list of booleans indicating, for every label, whether the prob map must be saved or not
            :param saveSegmentation: boolean indicating if the hard segmentation must be saved or not
            :param prefixForPredictions: [OPTIONAL, default = ''] prefix that will be added to the file name of all the stored probability maps and segmentations. It can be useful
                    if you want to save the predictions at every epoch in the same folder (you just pass the number of epoch as prefixForPredictions).
            :param computeMetrics [OPTIONAL, default = True] specifies if the metrics must be compted
            :param saveOriginalSize [OPTIONAL, default = True] specifies if the probability maps and segmentations will be saved in the original size of the input images, or
                                                                only considering the cropped area around the ROI (if the ROI is specified).
            :param generateProbMapsFilenames [OPTIONAL, default = False] If true, it generates a file containing visualization commands for the probability maps stored
            :param extraMetricsForMixedLabels [OPTIONAL, default = []] It will compute extra metrics, considering the labels in every sublist as a unique new label. For example:
                            extraMetricsForMixedLabels = [[1,2,4], [1,4]] will compute two more metrics (apart of those computed for the original labels defined in the modelConf),
                            where the union of [1,2,4] will be reported as label 5, and the union of [1,4] will be seen as label 6.

        """
        useEnsemble = isinstance(inputChannelsFiles[0], list)
        # inputChannelsFiles[MODEL][CHANNELS]
        # IMPORTANT: if useEnsemble, then allChannelsFilenames will be a list of lists
        #            otherwise, allChannelsFilenames will be a simple list of strings (filenames)
        computeAleatoricUncert = self.confModel['modelType']  == 'SimpleDeepmedic_Heteroscedastic'

        assert not (computeDropoutUncert and computeAleatoricUncert), 'Uncertainty based on Dropout sampling can not be computed for SimpleDeepmedic_Heteroscedastic model.'

        if saveSegmentation or (True in saveProbMapsForEachClass) or computeAleatoricUncert:
            ensureDir(outputDir)

        if useEnsemble:
            # Load the test files
            allChannelsFilenames = []

            for i in range(len(inputChannelsFiles)):
                allChannelsFilenamesPerModel, gtFilenames, roiFilenames, namesForPredictionsPerCaseFilenames = loadFilenames(inputChannelsFiles[i], inputGtLabels,
                                                                                          inputRoiMasks, namesForPredictionsPerCase)
                allChannelsFilenames.append(allChannelsFilenamesPerModel)
        else:
            allChannelsFilenames, gtFilenames, roiFilenames, namesForPredictionsPerCaseFilenames = loadFilenames(
                inputChannelsFiles, inputGtLabels,
                inputRoiMasks, namesForPredictionsPerCase)

        if originalResGtLabels is not None:
            originalResGtFilenames = loadFilenamesSingle(originalResGtLabels)
        else:
            originalResGtFilenames = []

        if calculateDistances:
            allMetrics = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS + MetricsMonitor.NUM_DISTANCE_METRICS))
            allMetricsOrigRes = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS + MetricsMonitor.NUM_DISTANCE_METRICS))
        else:
            allMetrics = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))
            allMetricsOrigRes = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_METRICS))

        segmentationStatsMetrics = np.ndarray(shape=(0, len(self.confModel['labels']), MetricsMonitor.NUM_SEGMENTATION_STATS))

        if len(extraMetricsForMixedLabels) > 0:
            allMetricsMixedLabels = [np.ndarray(shape=(0, 2, MetricsMonitor.NUM_METRICS)) for xxx in range(0,len(extraMetricsForMixedLabels)) ]

        commandsAndDice = {}
        voxelsPerLabelInGt = {}

        # For every test case
        for i in range(len(gtFilenames)):
            # Load channel files
            inputChannels = []

            gc.collect()
            if useEnsemble:
                # inputChannels[MODEL][CHANNEL][SAMPLE]
                for e in range(len(inputChannelsFiles)):
                    aux = []
                    for c in range(len(allChannelsFilenames[e])):
                        aux.append(nib.load(allChannelsFilenames[e][c][i]).get_data())
                    inputChannels.append(aux)

            else:
                # inputChannels[CHANNEL][SAMPLE]
                for c in range(len(allChannelsFilenames)):
                    inputChannels.append(nib.load(allChannelsFilenames[c][i]).get_data())

            # Set the padding value to the minimum value of the data
            if not saveOriginalSize and computeMetrics:
                print 'WARNING: INFERED PROBABILITY MAPS AND GT MAY HAVE DIFFERENT SIZES. IF saveOriginalSize==True THEN computeMetrics SHOULD BE FALSE BECAUSE SIZE COMPATIBILITY CAN NOT BE ASSURED'

            if self.confModel['paddingStrategy'] == 'fixedValue':
                newPadding = self.confModel['paddingValue']
            else:
                if useEnsemble:
                    newPadding = np.min(inputChannels[0][0])
                else:
                    newPadding = np.min(inputChannels[0])

#            newPadding = -3.9999

            logging.info('-- Padding the full res volume with value: %f' % newPadding)

            if len(self.ensembleNetworks) == 0:
                self.segmFullResModel.padValue.set_value(np.array(newPadding, dtype=theano.config.floatX))
            else:
                for j in range(len(self.ensembleNetworks)):
                    self.ensembleNetworks[j].segmFullResModel.padValue.set_value(np.array(newPadding, dtype=theano.config.floatX))

            # Load ROI filename if provided
            roi = nib.load(roiFilenames[i]).get_data() if not(roiFilenames == []) else None

            # Get the name that will be used for prediction
            nameForPrediction = namesForPredictionsPerCaseFilenames[i]

            # Generate the prediction
            logging.info('-- Predicting probability map for image [%d]' % (i))
            logging.info('-- whose GT file is %s' % gtFilenames[i])
            logging.info('---- Using ROI: %s' % str(roi is not None))

            # In case of testing, the confTrain is None, that is why I first ask for self.confTrain.
            # If we are in testing mode (confTrain is None) then we segment with ROI.
            if self.confTrain is not None:
                if roi is not None:
                    if useEnsemble:
                        pred = self.segmentWithRoiEnsemble(inputChannels, roi, saveOriginalSize)
                    else:
                        if computeAleatoricUncert:
                            pred, uncert = self.segmentWithRoi(inputChannels, roi, saveOriginalSize, computeUncert=1)
                        elif computeDropoutUncert:
                            pred, uncert = self.segmentWithRoi(inputChannels, roi, saveOriginalSize, computeUncert=2)
                        else:
                            pred = self.segmentWithRoi(inputChannels, roi, saveOriginalSize)
                else:
                    if useEnsemble:
                        #TODO: Add ensemble to .segment
                        assert False, "ERROR: SEGMENT WITHOUT ROI IS NOT IMPLEMENTED FOR ENSEMBLES"
                    else:
                        pred = self.segment(inputChannels)
            else:
                if useEnsemble:
                    pred = self.segmentWithRoiEnsemble(inputChannels, roi, saveOriginalSize)
                else:
                    if computeAleatoricUncert:
                        print 'NOT COMPUTING UNCER 1'
                        pred, uncert = self.segmentWithRoi(inputChannels, roi, saveOriginalSize, computeUncert=1)
                    elif computeDropoutUncert:
                        print 'COMPUTING UNCERT 2'
                        pred, uncert = self.segmentWithRoi(inputChannels, roi, saveOriginalSize, computeUncert=2)
                    else:
                        print 'NOT COMPUTING UNCERT'
                        pred = self.segmentWithRoi(inputChannels, roi, saveOriginalSize)

            # Transform the one-hot encoding of the probability maps into a hard segmentation
            hardSegmPred = np.squeeze(MetricsMonitor.getHardSegmentationFromTensor(pred, self.confModel['labels']))
            logging.info('----  Prediction shape: %s' % str(pred.shape))
            logging.info('----  Hard segm prediction shape: %s' % str(hardSegmPred.shape))

            ensureDir(outputDir)

            if computeMetrics:
                # Load GT file
                hardSegmGt = nib.load(gtFilenames[i]).get_data()

                # Generate the metrics for the segmentation
                currentMetrics = MetricsMonitor.getMetricsForWholeSegmentation(hardSegmPred, hardSegmGt, self.confModel['labels'], roi, calculateDistances=calculateDistances)

                # Compute the statistics also for the mixed labels
                if len(extraMetricsForMixedLabels) > 0:
                    logging.info("          [ ========= Metrics for MIXED LABELS ========== ]")

                    for mixedLabelIndex in range (0, len(extraMetricsForMixedLabels)):
                        hardSegmPredMixedLabels = np.zeros(hardSegmPred.shape, dtype=np.int16)
                        hardSegmGtMixedLabels = np.zeros(hardSegmGt.shape, dtype=np.int16)

                        newLabel = extraMetricsForMixedLabels[mixedLabelIndex][1]

                        # Replace the labels by the new label and compute the metrics
                        for oldLabelIndex in range(0, len(extraMetricsForMixedLabels[mixedLabelIndex][0])):
                            hardSegmPredMixedLabels[hardSegmPred == extraMetricsForMixedLabels[mixedLabelIndex][0][oldLabelIndex]] = newLabel

                            hardSegmGtMixedLabels[hardSegmGt == extraMetricsForMixedLabels[mixedLabelIndex][0][oldLabelIndex]] = newLabel

                        # Calculate the metrics
                        currentMetricsMixedLabels = MetricsMonitor.getMetricsForWholeSegmentation(hardSegmPredMixedLabels, hardSegmGtMixedLabels, [0, newLabel], roi, calculateDistances=calculateDistances)

                        logging.info("Fused labels %s : %s" % (str(extraMetricsForMixedLabels[mixedLabelIndex][0]), str(currentMetricsMixedLabels[0, 1, :])))

                        allMetricsMixedLabels[mixedLabelIndex] = np.concatenate((currentMetricsMixedLabels, allMetricsMixedLabels[mixedLabelIndex]))

                        hardSegmPredMixedLabelsImg = nib.Nifti1Image(hardSegmPredMixedLabels, affine=nib.load(gtFilenames[i]).affine.copy())

                        outputFilename = os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "EXTRA_LABEL_" + str(newLabel) + "_" + getMedicalImageExtension(nameForPrediction))

                        nib.save(hardSegmPredMixedLabelsImg, outputFilename)

                logging.info("          [ ========= Metrics in the given resolution ========== ]")
                logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t]")

                for l in range(currentMetrics.shape[1]):
                    logging.info("Label %d : %s" % (self.confModel["labels"][l], str(currentMetrics[0, l, :])))

                allMetrics = np.concatenate((currentMetrics, allMetrics))

#                if generateVisualizationCommandsFile:
#                    currentSegmentationStatsMetrics = MetricsMonitor.getPredictionStatsWithRespectToROI(hardSegmPred, hardSegmGt, self.confModel['labels'], roi)

#                    logging.info("          [ ========= Distance to the ROI border per label ( ========== ]")
#                    logging.info("          [ TP AVG\t\t TP STD\t\t TN AVG\t\t TN STD\t\t FN AVG\t\t FN STD\t\t FP AVG\t\t FP STD\t\t]")
#                    for l in range(currentSegmentationStatsMetrics.shape[1]):
#                        logging.info("Label %d : %s" % (self.confModel["labels"][l], str(currentSegmentationStatsMetrics[0, l, :])))

                # IF WE HAVE TO CALCULATE THE DICE IN THE ORIGINAL RES:
                if len(originalResGtFilenames) > 0:
                    # Load original res file
                    hardSegmGtOrigRes = nib.load(originalResGtFilenames[i])
                    print "Orig GT res size: " + str(hardSegmGtOrigRes.get_data().shape)
                    # Resample the resulting hardSegmPred to the space of the original image
                    hardSegmPredImg = nib.Nifti1Image(hardSegmPred, affine=nib.load(gtFilenames[i]).affine.copy())

                    resampledHardSegmPred = nilearnimage.resample_to_img(hardSegmPredImg, hardSegmGtOrigRes, interpolation='nearest', copy=True, order='F')

                    # Resample the ROI to the space of the original image if provided
                    resampledRoi = None

                    if roi is not None:
                        roiImg = nib.load(roiFilenames[i])
                        print "Roi size: " + str(roiImg.get_data().shape)

                        resampledRoi = nilearnimage.resample_to_img(roiImg, hardSegmGtOrigRes, interpolation='nearest', copy=True, order='F')

                    # Generate the metrics for the segmentation
                    logging.info("          [ ========= Metrics in the original resolution ========== ]")
                    currentMetricsOrigRes = MetricsMonitor.getMetricsForWholeSegmentation(resampledHardSegmPred.get_data(), hardSegmGtOrigRes.get_data(), self.confModel['labels'], resampledRoi.get_data(), calculateDistances=calculateDistances)
                    logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t]")

                    for l in range(currentMetrics.shape[1]):
                        logging.info("Label %d : %s" % (self.confModel["labels"][l], str(currentMetricsOrigRes[0, l, :])))

                    allMetricsOrigRes = np.concatenate((currentMetricsOrigRes, allMetricsOrigRes))

                    # Save segmentation and probability maps in the original resolution if GT is provided in the original res
                    if saveSegmentation:
                        logging.info("Saving segmentation in original resolution: " + prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "ORIG_RES_SEGM_" + getMedicalImageExtension(nameForPrediction))
                        outputFilename = os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "ORIG_RES_SEGM_" + getMedicalImageExtension(nameForPrediction))
                        nib.save(resampledHardSegmPred, outputFilename)

                    for l in range(len(saveProbMapsForEachClass)):
                        if saveProbMapsForEachClass[l]:
                            array_img = nib.Nifti1Image(np.squeeze(pred[0, l, :, :, :]), affine = nib.load(gtFilenames[i]).affine.copy())

                            resampledSoftSegmPred = nilearnimage.resample_to_img(array_img, hardSegmGtOrigRes,interpolation='continuous', copy=True, order='F')
                            outputFilename = os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "ORIG_RES_PRED_L" + str(self.confModel["labels"][l]) + "_" + getMedicalImageExtension(nameForPrediction))

                            nib.save(resampledSoftSegmPred, outputFilename)

                if generateVisualizationCommandsFile:
                    # Count number of voxels per label:
                    for l in range(0, len(self.confModel["labels"])):
                        voxelsPerLabelInGt[l] = (hardSegmGt == self.confModel["labels"][l]).sum()

            # Save the probability maps if confTest['saveProbMapsForEachClass'][l] == True for label l
            for l in range(len(saveProbMapsForEachClass)):
                if saveProbMapsForEachClass[l]:
                    array_img = nib.Nifti1Image(np.squeeze(pred[0, l, :, :, :]), affine = nib.load(gtFilenames[i]).affine.copy())

                    outputFilename = os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "_prob" + str(self.confModel["labels"][l]) + getMedicalImageExtension(nameForPrediction))

                    nib.save(array_img, outputFilename)

            if saveSegmentation:
                array_img = nib.Nifti1Image(hardSegmPred, affine = nib.load(gtFilenames[i]).affine.copy())
                #outputFilename = os.path.join(outputDir, prefixForPredictions + "SEGM_" + nameForPrediction)
                outputFilename = os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "_segm" + getMedicalImageExtension(nameForPrediction))
                nib.save(array_img, outputFilename)

            if computeAleatoricUncert or computeDropoutUncert:
                array_img = nib.Nifti1Image(np.squeeze(uncert[0, 0, :, :, :]),affine=nib.load(gtFilenames[i]).affine.copy())
                outputFilename = os.path.join(outputDir, prefixForPredictions + "UNCERT_" + nameForPrediction)
                nib.save(array_img, outputFilename)

            if generateVisualizationCommandsFile or generateProbMapsFilenames:
                command = "itksnap"

                # Add channel image
                if useEnsemble:
                    command += " -g " + allChannelsFilenames[0][0][i]
                else:
                    command += " -g " + allChannelsFilenames[0][i]
                # Add gt image
                command += " -s " +  gtFilenames[i]

                command += " -o "
                header = "Command to visualize"
                # Add Probability maps
                for l in range(len(saveProbMapsForEachClass)):
                    if saveProbMapsForEachClass[l]:
                        command += " " + os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "_prob" + str(self.confModel["labels"][l]) + getMedicalImageExtension(nameForPrediction))

                if saveSegmentation:
                    # Add predicted hard segmentation mask
                    #command += " " + os.path.join(outputDir, prefixForPredictions + "SEGM_" + nameForPrediction)
                    command += " " + os.path.join(outputDir, prefixForPredictions + getMedicalImageBasename(nameForPrediction) + "_segm" + getMedicalImageExtension(nameForPrediction))

                if len(originalResGtFilenames) > 0:
                    # Modify the command so that it can be used to visualize
                    origFilenamePattern = "_resampled_meanStd_prep_stripped.nii.gz"
                    replaceWithPattern = ".nii.gz"

                    gtOrigFilenamePattern = "_seg_resampled_multi_7_19_2_3_stripped.nii.gz"
                    gtReplaceWithPattern = "_seg_origSize_multi_7_19_2_3.nii.gz"

                    commandForOrigSize = command.replace(origFilenamePattern, replaceWithPattern)
                    commandForOrigSize = commandForOrigSize.replace(gtOrigFilenamePattern, gtReplaceWithPattern)
                    commandForOrigSize = commandForOrigSize.replace("_prob", "ORIG_RES_PRED_L")
                    commandForOrigSize = commandForOrigSize.replace("_segm", "ORIG_RES_SEGM_")

                    logging.info("-- VISUALIZE: " + command)
                    logging.info("-- VISUALIZE ORIG RES: " + commandForOrigSize)

                    header += ",Command to visualize (ORIG RES)"
                    command += "," + commandForOrigSize
                else:
                    logging.info("-- VISUALIZE: " + command)

                for l in range(0, len(self.confModel["labels"])):
                    command += "," + str(currentMetrics[0, l, 1])
                    header += ",L" + str(self.confModel["labels"][l]) + " SEN"
                    command += "," + str(currentMetrics[0, l, 2])
                    header += ",L" + str(self.confModel["labels"][l]) + " SPE"
                    command += "," + str(currentMetrics[0, l, 3])
                    header += ",L" + str(self.confModel["labels"][l]) + " DSC"

                commandsAndDice[command] = np.average(currentMetrics[0, 1:, 3])

        if generateVisualizationCommandsFile:
            orderedCommandsByAvgDice = collections.OrderedDict(sorted(commandsAndDice.items(), key=lambda t: t[1]))

            f = open(os.path.join(outputDir, "commandsVisualization.csv"), 'w')
            header += ", Average DSC for non-0 labels\n"

            f.write(header)

            for k in orderedCommandsByAvgDice.keys():
                line = k + "," + str(orderedCommandsByAvgDice[k])

                print f.write( line + "\n")

            f.close()

        if computeMetrics:
            # Compute average metrics per label
            avgMetrics = np.nanmean(allMetrics, axis=0)
            stdMetrics = np.nanstd(allMetrics, axis=0)

            logging.info("")
            logging.info("")
            logging.info("===== Final avg and std of the metrics (considering only the ROI if provided) =====")
            logging.info("================================= AVG ============================")
            logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t]")
            logging.info("")

            for l in range(avgMetrics.shape[0]):
                logging.info("Label %d : %s" % (self.confModel["labels"][l], str(avgMetrics[l, :])))

            logging.info("================================= STD ============================")
            logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t]")

            for l in range(stdMetrics.shape[0]):
                logging.info("Label %d : %s" % (self.confModel["labels"][l], str(stdMetrics[l, :])))

            if len(extraMetricsForMixedLabels) > 0:

                for mixedLabelIndex in range(0, len(extraMetricsForMixedLabels)):
                    avgMetricsMixedLabels = np.nanmean(allMetricsMixedLabels[mixedLabelIndex], axis=0)
                    stdMetricsMixedLabels = np.nanstd(allMetricsMixedLabels[mixedLabelIndex], axis=0)
                    logging.info("MEAN Fused labels %s : %s" % ( str(extraMetricsForMixedLabels[mixedLabelIndex][0]), str(avgMetricsMixedLabels[1, :])))
                    logging.info("STD Fused labels %s : %s" % ( str(extraMetricsForMixedLabels[mixedLabelIndex][0]), str(stdMetricsMixedLabels[1, :])))

            if len(originalResGtFilenames) > 0:
                # Compute average metrics per label
                avgMetricsOrigRes = np.nanmean(allMetricsOrigRes, axis=0)
                stdMetricsOrigRes = np.nanstd(allMetricsOrigRes, axis=0)

                logging.info("")
                logging.info("")
                logging.info("===== [ORIGINAL RES] Final avg and std of the metrics (considering only the ROI if provided) =====")
                logging.info("======================== AVG ORIGINAL RES =======================")
                logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t] ORIGINAL RES")
                logging.info("")

                for l in range(avgMetrics.shape[0]):
                    logging.info("Label %d : %s" % (self.confModel["labels"][l], str(avgMetricsOrigRes[l, :])))

                logging.info("========================= STD ORIGINAL RES ======================")
                logging.info("          [ ACC\t\t SEN\t\t SPE\t\t DSC\t\t JAC\t\t] ORIGINAL RES")

                for l in range(stdMetrics.shape[0]):
                    logging.info("Label %d : %s" % (self.confModel["labels"][l], str(stdMetricsOrigRes[l, :])))

                return avgMetrics, stdMetrics, allMetrics, avgMetricsOrigRes, stdMetricsOrigRes, allMetricsOrigRes
            else:
                return avgMetrics, stdMetrics, allMetrics
        else:
            return None, None, None

def setupLogger(logFilename):

    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)-10s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=logFilename,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    # Now, we can log to the root logger, or any other logger. First the root...
    print ('Logging to file: %s' % logFilename)
    logging.info('Logging to file: %s' % logFilename)

if __name__ == "__main__":
    # Choose whether you'll train or test
    calibrate = False
    train = False
    test = True
    testFolds = False
    trainFolds = False

    if calibrate:
        # Lists of parameters to calibrate
        #sigmaForGaussianNoises = [0.01]
        learningRates = [0.1, 0.01]
        l2Factors = [0.00001, 0.0001]

#        onlyFinalLayerTrainables = [True, False]

        # Read configuration files
        confModelOrig = {}
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/malibo/maliboCT/UNet3D_MaliboCT_Model_multinorm3.conf", confModelOrig)
#        execfile("../conf/BRATS2017/UNet3D_BRATS2017_Model.conf", confModel)
#        execfile("../conf/CTSamCook/RefineModels/Refine_CTSamCook_4labels_Model.conf", confModelOrig)
        confTrainOrig = {}

        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/malibo/maliboCT/UNet3D_MaliboCT_Training_multinorm3.conf", confTrainOrig)
#        execfile("../conf/BRATS2017/UNet3D_BRATS2017_Training.conf", confTrainOrig)
#        execfile("../conf/CTSamCook/RefineModels/Refine_CTSamCook_4labels_Training.conf", confTrainOrig)


        #execfile("../conf/CTSamCook/4labels/Cascade_CTSamCook_4labels_Training.conf", confTrainOrig)
        #execfile("../conf/CTSamCook/SandwichPlanar_CTSamCook_Training.conf", confTrainOrig)

        del (confTrainOrig['__builtins__'])
        del (confModelOrig['__builtins__'])

        ensureDir(confTrainOrig["folderForOutput"])
        # Setup the logger
        print "Logging to: " + str(os.path.join(confTrainOrig["folderForOutput"], confTrainOrig['trainingSessionName'] + str(datetime.datetime.now()).replace(' ', '_') + ".log"))
        setupLogger(os.path.join(confTrainOrig["folderForOutput"], confTrainOrig['trainingSessionName'] + str(datetime.datetime.now()).replace(' ', '_') + ".log"))
        logging.info(" =======================================")

        # Iterate for every possible combination of parameters
        #for batchSize in batchSizes:
        for learningRate in learningRates:

#            for percentOfSamplesToExtractPositiveTrain in percentOfSamplesToExtractPositiveTrains:
            for l2Factor in l2Factors:
            #        for l2Factor in l2Factors:
                confTrain = copy.deepcopy(confTrainOrig)
                confModel = copy.deepcopy(confModelOrig)

                gc.collect()

                logging.info(" =======================================")
                logging.info(" ===== CALIBRATING WITH PARAMETERS ======")
                logging.info(" =======================================")
                logging.info(" ===== learningRate = %s ======" % learningRate)
                logging.info(" ===== l2Factor = %s ======" % l2Factor)

    #                logging.info(" ===== heteroscedasticNumSamplers = %s ======" % heteroscedasticNumSampler)

    #            logging.info(" ===== batchSize = %s ======" % batchSize)
    #           logging.info(" ===== l2Factor = %s ======" % l2Factor)

                # Initialize the model and set the conf parameters
                confTrain['learningRate'] = learningRate
                confTrain['l2Factor'] = l2Factor

                #confModel['onlyFinalLayerTrainable'] = onlyFinalLayerTrainable
    #                confModel['heteroscedasticNumSamplers'] = heteroscedasticNumSampler
    #            confTrain['batchSizeTraining'] = batchSize
    #            confTrain['l2Factor'] = l2Factor

                outputDir = os.path.join(confTrainOrig["folderForOutput"], "_learningRates_" + str(learningRate) + "_l2Factor_" + str(l2Factor) +  "/") #+ "_batchSize_" + str(batchSize) + "_l2_" + str(l2Factor) + "/")
                confTrain["folderForOutput"] = outputDir
                ensureDir(outputDir)

                # Create the CNN
                segCNN = Blast()

                # Model output name for the current configuration
                modelOutputName = os.path.join(outputDir, confTrain['trainingSessionName'] + "_learningRates_" + str(learningRate) + "_l2Factor_" + str(l2Factor) + "_trainedModel.p") #+ "_batchSize_" + str(batchSize) + "_l2_" + str(l2Factor) + "_trainedModel.p")

                # Create the model
                segCNN.createModel(confModel)

                print 'Starting training'
                # Train the network
    #            tic()
                segCNN.train(confTrain, modelOutputName)
    #            toc()

                # Save the final trained model and statistics
                logging.info("Saving final results to: %s" % modelOutputName)
                segCNN.saveModel(modelOutputName)

        logging.info("Training finished")
        logging.info("")
        logging.info("")

    if trainFolds:
        # Read configuration files
        confModelOrig = {}
        #execfile("../conf/CTSamCook/BrainStrip/SimpleDM_CTSamCook_BrainStrip_Model.conf", confModelOrig)
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/malibo/maliboCT/UNet3D_MaliboCT_Model_multinorm3.conf", confModelOrig)

        confTrainOrig = {}
        #execfile("../conf/CTSamCook/BrainStrip/SimpleDM_CTSamCook_BrainStrip_Training.conf", confTrainOrig)
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/malibo/maliboCT/UNet3D_MaliboCT_Training_multinorm3.conf", confTrainOrig)

        #execfile("../conf/malibo/Training_Standard_Malibo.conf", confTrainOrig)
        del (confModelOrig['__builtins__'])
        del (confTrainOrig['__builtins__'])

        ensureDir(confTrainOrig["folderForOutput"])
        # Setup the logger
        setupLogger(os.path.join(confTrainOrig["folderForOutput"], confTrainOrig['trainingSessionName'] + str(datetime.datetime.now()).replace(' ', '_') + ".log"))
        for i in [1,2,3]:
            confTrain = copy.deepcopy(confTrainOrig)
            confModel = copy.deepcopy(confModelOrig)

            gc.collect()

            for kk in range(len(confTrain["channelsTraining"])):
                confTrain["channelsTraining"][kk] = confTrain["channelsTraining"][kk].replace("fold1", "fold"+str(i))
                confTrain["channelsValidation"][kk] = confTrain["channelsValidation"][kk].replace("fold1","fold" + str(i))

            confTrain["gtLabelsTraining"] = confTrain["gtLabelsTraining"].replace("fold1", "fold"+str(i))
            confTrain["roiMasksTraining"] = confTrain["roiMasksTraining"].replace("fold1", "fold" + str(i))
            confTrain["gtLabelsValidation"] = confTrain["gtLabelsValidation"].replace("fold1", "fold"+str(i))
            confTrain["roiMasksValidation"] = confTrain["roiMasksValidation"].replace("fold1", "fold" + str(i))

            if "cascadeInputModel" in confModel.keys():
                confModel["cascadeInputModel"] = confModel["cascadeInputModel"].replace("fold1", "fold" + str(i))

            if "modelInitialization" in confModel.keys():
                confModel["modelInitialization"] = confModel["modelInitialization"].replace("fold1", "fold" + str(i))

            outputDir = os.path.join(confTrainOrig["folderForOutput"], "training/fold" + str(i) + "/")
            confTrain["folderForOutput"] = outputDir

            ensureDir(outputDir)

            logging.info("===== Running simple training for Fold %s =====" % str(i))
            # Create the CNN
            segCNN = Blast()

            # Create the model
            segCNN.createModel(confModel)

            # Output file where the trained model and statistics will be saved
            modelOutputName = os.path.join(outputDir, confTrain['trainingSessionName'] + "_trainedModel.p")

            # TODO: SOLVER THE PROBLEM WITH SAMPLING IN THE BORDERS WHEN TRAINING TILE BASED!
#            # Train the network
#            tic()
            if confTrain['trainingType'] == 'tile_based':
                segCNN.train(confTrain, modelOutputName)
            elif confTrain['trainingType'] == 'whole_volume':
                segCNN.trainWholeVolume(confTrain, modelOutputName)
            else:
                print 'ERROR: Training strategy not recognized'
#           toc()

            # Save the final trained model and statistics
            logging.info("Saving final results to: %s" % modelOutputName)
            segCNN.saveModel(modelOutputName)

            logging.info("Training finished")

    if testFolds:
        # Read configuration files
#        confModelOrig = {}
        #execfile("../conf/CTSamCook/FrontiersInTBI/SimpleDMAnisotropic_Model_CTSamCook.conf", confModel)
        #execfile("../conf/CTSamCook/FrontiersInTBI/SimpleDM_CTSamCook_Model.conf", confModel)
#        execfile("../conf/CTSamCook/4labels/UNet3D_CTSamCook_4labels_Model.conf", confModelOrig)

        confTestOrig = {}
        #execfile("../conf/CTSamCook/FrontiersInTBI/SimpleDMAnisotropic_Testing_CTSamCook.conf", confTestOrig)
        #execfile("../conf/CTSamCook/FrontiersInTBI/SimpleDM_CTSamCook_Testing.conf", confTestOrig)
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/CTSamCook/SmoothDataset/SimpleDM_CTSamCook_Testing.conf", confTestOrig)

        del (confTestOrig['__builtins__'])
#        del (confModelOrig['__builtins__'])

        ensureDir(confTestOrig["folderForOutput"])
        # Setup the logger
        setupLogger(os.path.join(confTestOrig["folderForOutput"], confTestOrig['testSessionName'] + str(datetime.datetime.now()).replace(' ',
                                                                                                                    '_') + ".log"))
        for i in [2,3]:
            confTest = copy.deepcopy(confTestOrig)
#            confModel = copy.deepcopy(confModelOrig)

            gc.collect()

            for h in range(len(confTest["channelsTest"])):
                if isinstance(confTest["channelsTest"][h], list):
                    for hh in range(len(confTest["channelsTest"][h])):
                        confTest["channelsTest"][h][hh] = confTest["channelsTest"][h][hh].replace("fold1", "fold"+str(i))
                else:
                    confTest["channelsTest"][h] = confTest["channelsTest"][h].replace("fold1", "fold" + str(i))

            confTest["gtLabelsTest"] = confTest["gtLabelsTest"].replace("fold1", "fold"+str(i))
            confTest["roiMasksTest"] = confTest["roiMasksTest"].replace("fold1", "fold" + str(i))
            confTest["folderForOutput"] = confTest["folderForOutput"].replace("fold1", "fold" + str(i))

#            if "cascadeInputModel" in confModel.keys():
#                confModel["cascadeInputModel"] = confModel["cascadeInputModel"].replace("fold1", "fold" + str(i))

            if "originalResGtLabelsTest" in confTest.keys():
                confTest["originalResGtLabelsTest"] = confTest["originalResGtLabelsTest"].replace("fold1", "fold" + str(i))
            if "namesForPredictionsPerCaseTest" in confTest.keys():
                confTest["namesForPredictionsPerCaseTest"] = confTest["namesForPredictionsPerCaseTest"].replace("fold1", "fold" + str(i))

            outputDir = confTest["folderForOutput"]
#            confTest["folderForOutput"] = outputDir

            ensureDir(outputDir)

            logging.info("===== Running testing in Fold %d =====" % i)
            # Create the CNN
            segCNN = Blast()

            if isinstance(confTest["cnnModelFilePath"], list):
                for e in range(len(confTest["cnnModelFilePath"])):
                    confTest["cnnModelFilePath"][e] = confTest["cnnModelFilePath"][e].replace("fold1", "fold" + str(i))

                # Load the saved model
                logging.info("====== Using Ensemble composed by %s models =====" % str(len(confTest["cnnModelFilePath"])))

                segCNN.loadEnsembleModel(confTest["cnnModelFilePath"])
            else:
                confTest["cnnModelFilePath"] = confTest["cnnModelFilePath"].replace("fold1", "fold" + str(i))

                # Load the saved model
                logging.info("====== Using Single Model =====")
                segCNN.loadModel(confTest["cnnModelFilePath"], compileDeterministic = False if "computeEpistemicUncertainty" in confTest.keys() and confTest["computeEpistemicUncertainty"]==True else True)

            # Run the test script with the given configuration file
            segCNN.test(confTest)

            # Output file where the trained model and statistics will be saved
            modelOutputName = os.path.join(outputDir, confTest['testSessionName'] + "_testModel.p")

            # Save the model so that stats can be used for comparison
            logging.info("Saving final results to: %s" % modelOutputName)
            segCNN.saveModel(modelOutputName)

            logging.info("Test for fold %d finished" % i)

    if train:
        # Read configuration files
        confModel = {}
#       execfile("../conf/SimpleDM_CTSamCook_Model.conf", confModel)
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/BRATS2017/UNet3D_BRATS2017_Model.conf", confModel)

        confTrain = {}
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/BRATS2017/UNet3D_BRATS2017_Training_v3.conf", confTrain)
        #execfile("../conf/SimpleDM_Training_CTSamCook.conf", confTrain)

        outputDir = os.path.join(confTrain["folderForOutput"], "training/")
        ensureDir(outputDir)
        print "Start training ...."
        # Setup the logger
        setupLogger(os.path.join(outputDir, confTrain['trainingSessionName'] + str(datetime.datetime.now()).replace(' ', '_') + ".log"))

        logging.info("===== Running simple training =====")
        # Create the CNN
        segCNN = Blast()

        # Create the model
        segCNN.createModel(confModel)

        # Output file where the trained model and statistics will be saved
        modelOutputName = os.path.join(outputDir, confTrain['trainingSessionName'] + "_trainedModel.p")

        # Train the network
#        tic()
        segCNN.train(confTrain, modelOutputName)
#        toc()

        # Save the final trained model and statistics
        logging.info("Saving final results to: %s" % modelOutputName)
        segCNN.saveModel(modelOutputName)

        logging.info("Training finished")

    if test:
        #TODO: ADD COMPUTING VAL IN FULL RES IMAGE
        # Read configuration file. Only confTest is needed because the confModel is stored in the file specified by confTest["cnnModelFilePath"]
        confTest = {}
        execfile("/vol/biomedic/users/eferrant/code/simple-deepmedic/conf/BRATS2017/UNet3D_BRATS2017_Testing_RealEval.conf", confTest)

        outputDir = os.path.join(confTest["folderForOutput"])
        ensureDir(outputDir)

        # Setup the logger
        setupLogger(os.path.join(outputDir, confTest['testSessionName'] + '_' + str(datetime.datetime.now()).replace(' ', '_') + ".log"))

        logging.info("===== Testing trained model =====")

        # Create the CNN
        segCNN = Blast()

        if isinstance(confTest["cnnModelFilePath"], list):
            # Load the saved model
            logging.info("====== Using Ensemble composed by %s models =====" % str(len(confTest["cnnModelFilePath"])))
            segCNN.loadEnsembleModel(confTest["cnnModelFilePath"])
        else:
            # Load the saved model
            logging.info("====== Using Single Model =====")
            segCNN.loadModel(confTest["cnnModelFilePath"], compileDeterministic = False if "computeEpistemicUncertainty" in confTest.keys() and confTest["computeEpistemicUncertainty"]==True else True)

        # Run the test script with the given configuration file
        segCNN.test(confTest)

        # Output file where the trained model and statistics will be saved
        modelOutputName = os.path.join(outputDir, confTest['testSessionName'] + "_testModel.p")

        # Save the model so that stats can be used for comparison
        logging.info("Saving final results to: %s" % modelOutputName)
        segCNN.saveModel(modelOutputName)

