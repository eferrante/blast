#!/usr/bin/python

from blast.cnn import Blast
from blast.aux.Common import tic, toc, ensureDir, loadFilenames

import getopt
import sys
import gc

import logging
import datetime
import os
import copy
import numpy as np
import itertools

def writeFileWithStringCorrespondigToIndices(indicesList, valuesList, outputFile):
    with open(outputFile, "a") as f:
        valuesToWrite = list(np.array(valuesList)[indicesList])

        for item in valuesToWrite:
            f.write("%s\n" % item)


def generateFoldConfFiles(outputDir, numFolds, pathsToAllFilesChannels, pathToAllFilesGT, pathToAllFilesMasks = None, pathToAllFilesNamesForPredictions = None):
    """
    This method takes list of files, and generate new configuration files with the corresponding folds.

    :param pathsToAllFilesChannels: list containing text files, listing the channel files. Ex: ["/path/to/dir/channelsT1.cfg", "/path/to/dir/channelsT2.cfg"]
    :param pathToAllFilesGT: path to a file listing the GT files
    :param pathToAllFilesMasks: path to a file containing the mask files
    :param outputDir: dir where the folders containing the fold conf files will be created
    :param pathToAllFilesNamesForPredictions: names used to generate the predictions

    :param: number of folds (From sklearn.model_selection.KFold) The first n_samples % n_splits folds have size n_samples // n_splits + 1, other folds have size n_samples // n_splits, where n_samples is the number of samples.

    """

    # Read the filenames
    allChannelsFilenames, gtFilenames, roiFilenames, namesForPredictionsPerCaseFilenames = loadFilenames(pathsToAllFilesChannels, pathToAllFilesGT, pathToAllFilesMasks, pathToAllFilesNamesForPredictions)

    numSamples = len(gtFilenames)

    # Based on the code in sklearn.model_selection.KFold
    # generate the indices of the files in every fold
    foldSizes = (numSamples // numFolds) * np.ones(numFolds, dtype=np.int)
    foldSizes[:numSamples % numFolds] += 1

    # foldIndices will contain the indices of the files in every fold
    foldIndices = []
    allIndices = list(range(numSamples))

    current = 0
    for foldSize in foldSizes:
        start, stop = current, current + foldSize
        foldIndices.append(allIndices[start:stop])
        current = stop

    # Generate the combinations of folds for training and testing
    trainCombinations = list(itertools.combinations(range(numFolds), numFolds-1))
    testCombinations = []

    for i in range (numFolds):
        testCombinations.append(tuple(set(range(numFolds)) - set(trainCombinations[i])))

    testCombinations = [tuple(testCombinations[i]) for i in range(numFolds)]

    ensureDir(outputDir)

    # Create the conf files for every fold
    for i in range(numFolds):

        foldDir = outputDir + "/fold" + str(i+1) + "/"
        ensureDir(foldDir)

        # For every fold in the training set:
        for k in range(len(trainCombinations[i])):

            # For every image channel
            for j in range(len(allChannelsFilenames)):
                writeFileWithStringCorrespondigToIndices(foldIndices[trainCombinations[i][k]], allChannelsFilenames[j], os.path.join(foldDir, "train_" + os.path.split(pathsToAllFilesChannels[j])[1]))

            # GT files
            writeFileWithStringCorrespondigToIndices(foldIndices[trainCombinations[i][k]], gtFilenames, os.path.join(foldDir, "train_" + os.path.split(pathToAllFilesGT)[1]))

            # Mask files if provided
            if pathToAllFilesMasks is not None:
                writeFileWithStringCorrespondigToIndices(foldIndices[trainCombinations[i][k]], roiFilenames, os.path.join(foldDir, "train_" + os.path.split(pathToAllFilesMasks)[1]))

            # Names for predictions
            if pathToAllFilesNamesForPredictions is not None:
                writeFileWithStringCorrespondigToIndices(foldIndices[trainCombinations[i][k]], namesForPredictionsPerCaseFilenames, os.path.join(foldDir, "train_" + os.path.split(pathToAllFilesNamesForPredictions)[1]))

        # For every image channel
        for j in range(len(allChannelsFilenames)):
            writeFileWithStringCorrespondigToIndices(foldIndices[testCombinations[i][0]], allChannelsFilenames[j], os.path.join(foldDir, "test_" + os.path.split(pathsToAllFilesChannels[j])[1]))

        # GT files
        writeFileWithStringCorrespondigToIndices(foldIndices[testCombinations[i][0]], gtFilenames, os.path.join(foldDir, "test_" + os.path.split(pathToAllFilesGT)[1]))

        # Mask files if provided
        if pathToAllFilesMasks is not None:
            writeFileWithStringCorrespondigToIndices(foldIndices[testCombinations[i][0]], roiFilenames, os.path.join(foldDir, "test_" + os.path.split(pathToAllFilesMasks)[1]))

        # Names for predictions
        if pathToAllFilesNamesForPredictions is not None:
            writeFileWithStringCorrespondigToIndices(foldIndices[testCombinations[i][0]], namesForPredictionsPerCaseFilenames, os.path.join(foldDir, "test_" + os.path.split(pathToAllFilesNamesForPredictions)[1]))

def printHelp():
    print '\n  ======= Running Blast ======\n'

    print '  Train a model: $ RunBlast.py --mode training --conf-model <confModel.cfg> --conf-training <confFile.cfg> [--folds numFolds]\n'

    print '  Test a model:  $ RunBlast.py --mode testing --conf-testing <confFile.cfg> [--folds numFolds] \n'

    print '  For multiple folds, specifiy numFolds > 1. The script will run RunBlast numFolds times, replacing every time the string \'fold1\' by \'foldK\' (where K is the current fold number) in all the paths provided in the configuration (test or train) file. ' \
          '  The strig will be replaced in the following configuration fields: channelsTraining, gtLabelsTraining, roiMasksTraining, channelsValidation, gtLabelsValidation, roiMasksValidation, folderForOutput \n'

    print '  Print Help: $ RunBlast.py -h \n'

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"",["mode=","conf-model=","conf-training=","conf-testing=","folds=","h"])
    except getopt.GetoptError as err:
        print "\n  ERROR: " + str(err)

        printHelp()
        sys.exit(2)

    for opt, arg in opts:
        numFolds = 1
        if opt == '--mode':
            mode = arg
        elif opt == "--conf-model":
            confModelFilename = arg
        elif opt == "--conf-training":
            confTrainFilename = arg
        elif opt == "--conf-testing":
            confTestFilename = arg
        elif opt == "--folds":
            numFolds = int(arg)
        else:
            printHelp()
            sys.exit(0)

    if opts == []:
        printHelp()
        sys.exit(0)

    if mode == 'training':
        # Read configuration files
        confModel = {}
        execfile(confModelFilename, confModel)

        confTrainOrig = {}
        execfile(confTrainFilename, confTrainOrig)

        del (confTrainOrig['__builtins__'])

        ensureDir(confTrainOrig["folderForOutput"])
        # Setup the logger
        Blast.setupLogger(os.path.join(confTrainOrig["folderForOutput"], confTrainOrig['trainingSessionName'] + str(datetime.datetime.now()).replace(' ', '_') + ".log"))

        for i in range(1,numFolds + 1):

            confTrain = copy.deepcopy(confTrainOrig)
            gc.collect()

            if numFolds > 1:
                logging.info("================================================")
                logging.info("============== Training for Fold %s ============" % str(i))
                logging.info("================================================\n")

                for h in range(len(confTrain["channelsTraining"])):
                    if isinstance(confTrain["channelsTraining"][h], list):
                        for hh in range(len(confTrain["channelsTraining"][h])):
                            confTrain["channelsTraining"][h][hh] = confTrain["channelsTraining"][h][hh].replace("fold1","fold" + str(i))
                            confTrain["channelsValidation"][h][hh] = confTrain["channelsValidation"][h][hh].replace("fold1", "fold" + str(i))
                    else:
                        confTrain["channelsTraining"][h] = confTrain["channelsTraining"][h].replace("fold1", "fold" + str(i))
                        confTrain["channelsValidation"][h] = confTrain["channelsValidation"][h].replace("fold1","fold" + str(i))

                for kk in range(len(confTrain["channelsTraining"])):
                    confTrain["channelsTraining"][kk] = confTrain["channelsTraining"][kk].replace("fold1",
                                                                                                  "fold" + str(i))
                    confTrain["channelsValidation"][kk] = confTrain["channelsValidation"][kk].replace("fold1",
                                                                                                      "fold" + str(i))

                confTrain["gtLabelsTraining"] = confTrain["gtLabelsTraining"].replace("fold1", "fold" + str(i))
                confTrain["gtLabelsValidation"] = confTrain["gtLabelsValidation"].replace("fold1", "fold" + str(i))

                if "roiMasksTraining" in confTrain.keys():
                    confTrain["roiMasksTraining"] = confTrain["roiMasksTraining"].replace("fold1", "fold" + str(i))

                if "roiMasksValidation" in confTrain.keys():
                    confTrain["roiMasksValidation"] = confTrain["roiMasksValidation"].replace("fold1", "fold" + str(i))

                outputDir = os.path.join(confTrainOrig["folderForOutput"], "training/fold" + str(i) + "/")

            else:
                logging.info("================================================")
                logging.info("============ Training for single fold ==========")
                logging.info("================================================\n")

                outputDir = os.path.join(confTrainOrig["folderForOutput"], "training/")

            confTrain["folderForOutput"] = outputDir
            ensureDir(outputDir)

            # Create the CNN
            segCNN = Blast.Blast()

            # Create the model
            segCNN.createModel(confModel)

            # Output file where the trained model and statistics will be saved
            modelOutputName = os.path.join(outputDir, confTrain['trainingSessionName'] + "_trainedModel.p")

            # Train the network
            tic()
            segCNN.train(confTrain, modelOutputName)
            toc()

            # Save the final trained model and statistics
            logging.info("Saving final results to: %s" % modelOutputName)
            segCNN.saveModel(modelOutputName)

            logging.info("Training finished")

    elif mode == 'testing':
        # Read configuration files
        confTestOrig = {}
        execfile(confTestFilename, confTestOrig)

        del (confTestOrig['__builtins__'])

        ensureDir(confTestOrig["folderForOutput"])

        # Setup the logger
        Blast.setupLogger(os.path.join(confTestOrig["folderForOutput"], confTestOrig['testSessionName'] + str(datetime.datetime.now()).replace(' ', '_') + ".log"))

        for i in range(1,numFolds + 1):
            if numFolds > 1:
                logging.info("================================================")
                logging.info("============== Testing for Fold %s ============" % str(i))
                logging.info("================================================\n")

            confTest = copy.deepcopy(confTestOrig)
            gc.collect()

            if numFolds > 1:
                for h in range(len(confTest["channelsTest"])):
                    if isinstance(confTest["channelsTest"][h], list):
                        for hh in range(len(confTest["channelsTest"][h])):
                            confTest["channelsTest"][h][hh] = confTest["channelsTest"][h][hh].replace("fold1",
                                                                                                      "fold" + str(i))
                    else:
                        confTest["channelsTest"][h] = confTest["channelsTest"][h].replace("fold1", "fold" + str(i))

                if "gtLabelsTest" in confTest.keys():
                    confTest["gtLabelsTest"] = confTest["gtLabelsTest"].replace("fold1", "fold"+str(i))
                if "roiMasksTest" in confTest.keys():
                    confTest["roiMasksTest"] = confTest["roiMasksTest"].replace("fold1", "fold" + str(i))

                confTest["cnnModelFilePath"] = confTest["cnnModelFilePath"].replace("fold1", "fold" + str(i))

                if "originalResGtLabelsTest" in confTest.keys():
                    confTest["originalResGtLabelsTest"] = confTest["originalResGtLabelsTest"].replace("fold1", "fold" + str(i))
                if "namesForPredictionsPerCaseVal" in confTest.keys():
                    confTest["namesForPredictionsPerCaseVal"] = confTest["namesForPredictionsPerCaseVal"].replace("fold1", "fold" + str(i))

                outputDir = os.path.join(confTest["folderForOutput"], "test/fold" + str(i) + "/")

            else:
                logging.info("================================================")
                logging.info("============ Testing for single fold ==========")
                logging.info("================================================\n")
                outputDir = os.path.join(confTest["folderForOutput"], "test/")

            confTest["folderForOutput"] = outputDir

            ensureDir(outputDir)

            # Create the CNN
            segCNN = Blast.Blast()

            # Load the saved model
            segCNN.loadModel(confTest["cnnModelFilePath"], compileDeterministic = False if "computeEpistemicUncertainty" in confTest.keys() and confTest["computeEpistemicUncertainty"]==True else True)

            # Run the test script with the given configuration file
            segCNN.test(confTest)

            # Output file where the trained model and statistics will be saved
            modelOutputName = os.path.join(outputDir, confTest['testSessionName'] + "_testModel.p")

            # Save the model so that stats can be used for comparison
            logging.info("Saving final results to: %s" % modelOutputName)
            segCNN.saveModel(modelOutputName)

            logging.info("Test for fold %d finished" % i)
    else:
        print "ERROR: Mode '" + "' not recongnized. \n"
        printHelp()

if __name__ == "__main__":
   main(sys.argv[1:])