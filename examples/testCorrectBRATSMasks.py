"""
    Script used to correct the problematic brainmasks in BRATS

    Author: Enzo Ferrante
    Date: 27/07/2017
"""
import sys
sys.path.append("../blast/")
sys.path.append("../")

from blast.tools.PreProcessor import PreProcessor

# File with problematic stripping
imgFilename = "./data/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512.nii.gz"

# Open the file
ppMask = PreProcessor()
ppMask.open(imgFilename)

# Generate the mask
# generateIntermediateVolume will generate a subfolder in the image folder, containing the intermediate steps. Useful for debugging.
ppMask.image = ppMask.skullStrippingMRI(method=0, useLevelsets=True,generateIntermediateVolumes=True)

# Save the mask
ppMask.writeExtendingFilename(extendingName = "_newBrainmask", subDir= "")

# Open the image again, this time for brain stripping
ppImage = PreProcessor()
ppImage.open(imgFilename)

# Strip the image
ppImage.image  = ppImage.stripImageGivenImageMask(mask=ppMask.image, backgroundValue=0)

# Save the stripped image
ppImage.writeExtendingFilename(extendingName = "_stripped", subDir= "")


