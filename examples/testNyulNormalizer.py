"""
    Example showing how tools.NyulNormalizer can be used as a stand-alone component to normalize images.

    Author: Enzo Ferrante
    Date: 27/07/2017
"""

import SimpleITK as sitk
import sys

sys.path.append("../blast/")
sys.path.append("../")
from blast.tools.NyulNormalizer import NyulNormalizer


# ----- Training -----
print "Training Nyul model ..."

listFiles = ['./data/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512.nii.gz',
             './data/brats_2013_pat0002_1/VSD.Brain.XX.O.MR_Flair.54518.nii.gz',
             './data/brats_2013_pat0003_1/VSD.Brain.XX.O.MR_Flair.54524.nii.gz']

listMasks = ['./data/brats_2013_pat0001_1/brainmask.nii.gz',
             './data/brats_2013_pat0002_1/brainmask.nii.gz',
             './data/brats_2013_pat0003_1/brainmask.nii.gz']

outputModel = './nyulModel'

nyul = NyulNormalizer()

nyul.train(listOfImages=listFiles, listOfMasks=listMasks)
nyul.saveTrainedModel(outputModel)

# ----- Transforming images -----
print "Transforming images using the trained model"

testImg = listFiles[0]
testImgMask = listMasks[0]

image = sitk.ReadImage(testImg)
mask = sitk.ReadImage(testImgMask)

transformedImage = nyul.transform(image, mask)

sitk.WriteImage(transformedImage, './transformedImage.nii.gz')

print "DONE"

