"""
    Script used to correct the problematic brainmasks in BRATS

    Author: Enzo Ferrante
    Date: 27/07/2017
"""
import sys
sys.path.append("../blast/")
sys.path.append("../")

from blast.tools.PreProcessor import PreProcessor

def normalizeZScores():
    """
        It normalizes an image following the z-scores approach (substract the mean and divide by std every voxel)
    """
    imgFilename = "./data/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512.nii.gz"
    maskFilename = "./data/brats_2013_pat0001_1/brainmask.nii.gz"

    # Create a preprocessor instance
    pp = PreProcessor()

    # Open the image
    pp.open(imgFilename)

    # Normalize it with mean and standard deviation. If a mask is provided, mean and std are computed only considering voxels within the mask
    # Values out of the roi are set to -4.0
    pp.image = pp.normalize(roiFilename=maskFilename, backgroundValue=-4.0)

    # Write the normalized image in the same folder, adding the postfix "_normalized_ZScores"
    pp.writeExtendingFilename(extendingName="_normalized_ZScores",subDir="")

def normalizeGlobalZScores():
    """
        It normalizes the images using the global z-scores apprach (substract by the mean of means and divide by the mean of stds across all the images).
    """
    ##############################################################################
    #     Compute the global mean of means and mean of stds using all images
    ##############################################################################
    listFiles = ['./data/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512.nii.gz',
                 './data/brats_2013_pat0002_1/VSD.Brain.XX.O.MR_Flair.54518.nii.gz',
                 './data/brats_2013_pat0003_1/VSD.Brain.XX.O.MR_Flair.54524.nii.gz']

    listMasks = ['./data/brats_2013_pat0001_1/brainmask.nii.gz',
                 './data/brats_2013_pat0002_1/brainmask.nii.gz',
                 './data/brats_2013_pat0003_1/brainmask.nii.gz']

    avgMean = 0
    avgStd = 0

    for i in range(len(listFiles)):
        pp = PreProcessor()
        pp.open(listFiles[i])
        mean, std = pp.getMeanStd(roiFilename=listMasks[i])

        avgMean += mean
        avgStd += std

    avgMean /= len(listFiles)
    avgStd /= len(listFiles)

    ##############################################################################
    #     Normalize the image using the global mean and std
    ##############################################################################
    imgFilename = "./data/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512.nii.gz"
    maskFilename = "./data/brats_2013_pat0001_1/brainmask.nii.gz"

    # Open image
    pp = PreProcessor()
    pp.open(imgFilename)

    # Normalize with global z-scores
    pp.image = pp.normalizeGivenMeanStd(avgMean, avgStd, roiFilename=maskFilename,backgroundValue=-4.0)

    # Write the normalized image in the same folder, adding the postfix "_normalized_GlobalMeanStd"
    pp.writeExtendingFilename(extendingName="_normalized_GlobalMeanStd", subDir="")

def normalizeNyulAndZScores():
    """
        It normalizes an image using Nyul's method, and the z-scores.
    """
    ######################################
    #     Train a nyul model
    ######################################
    listFiles = ['./data/brats_2013_pat0001_1/VSD.Brain.XX.O.MR_Flair.54512.nii.gz',
                 './data/brats_2013_pat0002_1/VSD.Brain.XX.O.MR_Flair.54518.nii.gz',
                 './data/brats_2013_pat0003_1/VSD.Brain.XX.O.MR_Flair.54524.nii.gz']

    listMasks = ['./data/brats_2013_pat0001_1/brainmask.nii.gz',
                 './data/brats_2013_pat0002_1/brainmask.nii.gz',
                 './data/brats_2013_pat0003_1/brainmask.nii.gz']

    outputModel = './nyulModel'

    imgToBeNormalized =listFiles[0]
    maskForImgToBeNormalized = listMasks[0]

    #####################################################
    #     Normalize the image using the trained model
    #####################################################

    # Create a preprocessor instance and transforms the image
    pp = PreProcessor()

    pp.trainNyul(listFiles, listMasks, "./nyulModel")
    # Open the image
    pp.open(imgToBeNormalized)

    # Normalize it with mean and standard deviation. If a mask is provided, mean and std are computed only considering voxels within the mask
    # Values out of the roi are set to -4.0
    pp.image = pp.normalizeNyul(trainedModel="./nyulModel.npz", maskFilename=maskForImgToBeNormalized)

    # Normalize the Nyul normalized image using z-scores.
    pp.image = pp.normalize(roiFilename=maskForImgToBeNormalized, backgroundValue=-4.0)

    # Write the normalized image in the same folder, adding the postfix "_normalized_NyulZScores"
    pp.writeExtendingFilename(extendingName="_normalized_NyulZScores",subDir="")

print "\n\nNormalizing image using Z-Scores ... "
normalizeZScores()

print "\n\nNormalizing using Global Z-Scores ..."
normalizeGlobalZScores()

print "\n\nNormalizing image using Nyul ... "
normalizeNyulAndZScores()

print "\n\nNormalized images were saved in .examples/data/brats_2013_pat0001_1 ... "
