"""
    Brain stripping examples

    Author: Enzo Ferrante
    Date: 27/07/2017
"""
import sys
sys.path.append("../blast/")
sys.path.append("../")

from blast.tools.PreProcessor import PreProcessor

def brainStripNormalCT():
    """
        Brain stripping for CT images with complete skull
    """

    imgFilename = "./data/ct/withCompleteSkull.nii.gz"
    maskFilename =  "./data/ct/withCompleteSkull_resampled_brainMask.nii.gz"

    # Open the image
    pp = PreProcessor()
    pp.open(imgFilename)

    # Resample to 1mm isotropic
    pp.image = pp.resample()

    # Save resampled image
    pp.writeExtendingFilename(extendingName="_resampled", subDir="")

    # Extract the brain mask using morph op and level-sets
    pp.image = pp.skullStrippingCTBrain(useLevelSets=True, useAtlas=False)

    # Save the brain mask
    pp.writeExtendingFilename(extendingName="_resampled_brainMask", subDir="")

    # Reopen the image and brain strip it
    pp = PreProcessor()
    pp.open(imgFilename)

    # Resample to 1mm isotropic
    pp.image = pp.resample()

    pp.image = pp.stripImage(maskFileName=maskFilename, backgroundValue=-4.0)

    pp.writeExtendingFilename(extendingName="_resampled_stripped", subDir="")

def brainStripCTWithCraniectomy():
    """
        Brain stripping for CT images with missing skull (e.g. CT with craniectomy)

        IMPORTANT: RESULTS ARE LESS ACCURATE THAN THOSE OBTAINED WHEN THE SKULL IS COMPLETE
                   Most likely you will have to correct by hand the resulting mask.
                   The simplest way to correct the masks is to downsample them to the original resolution (you can use the method pp.resampleToMatchImage,
                    correct them there, resample again to isotropic 1mm and use the method pp.smoothBinaryMask to produce the final smooth brain mask in high res.
    """

    imgFilename = "./data/ct/withCraniectomy.nii.gz"
    maskFilename =  "./data/ct/withCraniectomy_resampled_brainMask.nii.gz"

    # Open the image
    pp = PreProcessor()
    pp.open(imgFilename)

    # Resample to 1mm isotropic
    pp.image = pp.resample()

    # Save resampled image
    pp.writeExtendingFilename(extendingName="_resampled", subDir="")

    # Extract the brain mask using morph op and level-sets (THE DIFFERENCE WITH THE NORMAL CASE IS THAT useAtlas=True)
    pp.image = pp.skullStrippingCTBrain(useLevelSets=True, useAtlas=True)

    # Save the brain mask
    pp.writeExtendingFilename(extendingName="_resampled_brainMask", subDir="")

    # Reopen the image and brain strip it
    pp = PreProcessor()
    pp.open(imgFilename)

    # Resample to 1mm isotropic
    pp.image = pp.resample()

    pp.image = pp.stripImage(maskFileName=maskFilename, backgroundValue=-4.0)

    pp.writeExtendingFilename(extendingName="_resampled_stripped", subDir="")


print "Brain Stripping CT image with complete skull"
brainStripNormalCT()

print "Brain Stripping CT image with craniectomy"
brainStripCTWithCraniectomy()

