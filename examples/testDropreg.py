"""
    Example showing how tools.Dropreg can be called using the Dropreg python wrapper

    Author: Enzo Ferrante
    Date: 27/07/2017
"""

import sys

sys.path.append("../blast/")
sys.path.append("../")
from blast.tools.Dropreg import Dropreg

d = Dropreg()

src = "./data/ct/withCompleteSkull.nii.gz"

trg = "./data/ct/withCraniectomy.nii.gz"

out = "./data/ct/withCompleteSkull_registered.nii.gz"

d.linearTransformationType = 1
d.linearSimilarityMeasure = 1
d.registerCenterOfMass = True
d.registerLinear = True
d.registerNonlinear = False

d.register(src, trg, output=out, deleteTmpVolumes=True)

d.deleteLastTransformationFiles()
