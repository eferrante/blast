"""
    Example showing how tools.DenseCRF3D can be called using the DenseCRF3D python wrapper

    Author: Enzo Ferrante
    Date: 27/07/2017
"""

import sys

sys.path.append("../blast/")
sys.path.append("../")
from blast.tools.DenseCRF3D import DenseCRF3D

crf = DenseCRF3D()
crf.pRCZandW = ["1.0", "1.0", "1.0", "1.0"]

crf.bRCZandW = ["17.0", "12.0", "10.0", "5.0"]

crf.bMods = ["1.0"]

crf.numberOfIterations = 2

crf.inference(channelsList = ["/path/to/original/image.nii.gz"],
              probList = [ "/path/to/prob/maps/for/label/1.nii.gz",
                       "/path/to/prob/maps/for/label/2.nii.gz",
                       "/path/to/prob/maps/for/label/N.nii.gz"],
              outputDir="./crfOutput/")