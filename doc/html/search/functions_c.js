var searchData=
[
  ['register',['register',['../classblast_1_1tools_1_1Dropreg_1_1Dropreg.html#a3088b8fabb6b85ec47abd4de387ff7a8',1,'blast::tools::Dropreg::Dropreg']]],
  ['resample',['resample',['../classblast_1_1tools_1_1PreProcessor_1_1PreProcessor.html#af07047fc3c356c9c7c22caff76a2f49b',1,'blast::tools::PreProcessor::PreProcessor']]],
  ['resampletomatchimage',['resampleToMatchImage',['../classblast_1_1tools_1_1PreProcessor_1_1PreProcessor.html#ad00176a97e1444d540873d4833581e27',1,'blast::tools::PreProcessor::PreProcessor']]],
  ['retainlargestconnectedcomponent',['retainLargestConnectedComponent',['../classblast_1_1tools_1_1PreProcessor_1_1PreProcessor.html#a624c1448201370e49e0875cf730efa17',1,'blast::tools::PreProcessor::PreProcessor']]],
  ['runningmeanconv',['runningMeanConv',['../classblast_1_1cnn_1_1MetricsMonitor_1_1MetricsMonitor.html#a79d0fcb459956693b34af4405898b60b',1,'blast::cnn::MetricsMonitor::MetricsMonitor']]]
];
