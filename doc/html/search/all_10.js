var searchData=
[
  ['test',['test',['../classblast_1_1cnn_1_1Blast_1_1Blast.html#a5dfe997c89363f51f0dd6d2a1f6f8201',1,'blast::cnn::Blast::Blast']]],
  ['thresholddensedeformationfield',['thresholdDenseDeformationField',['../classblast_1_1cnn_1_1DataAugmenter_1_1DataAugmenter.html#a7a9d04c92e2febb3fd373770ec4dddc5',1,'blast::cnn::DataAugmenter::DataAugmenter']]],
  ['thresholdvalues',['thresholdValues',['../classblast_1_1tools_1_1PreProcessor_1_1PreProcessor.html#ac3c30c51dee8243cf2c4965d3c875452',1,'blast::tools::PreProcessor::PreProcessor']]],
  ['train',['train',['../classblast_1_1cnn_1_1Blast_1_1Blast.html#a422cca7ced85f1dc8244b25602ee3d2b',1,'blast.cnn.Blast.Blast.train()'],['../classblast_1_1tools_1_1NyulNormalizer_1_1NyulNormalizer.html#ae777c0fe5ab3ea081a6be1af7c408268',1,'blast.tools.NyulNormalizer.NyulNormalizer.train()']]],
  ['trainnyul',['trainNyul',['../classblast_1_1tools_1_1PreProcessor_1_1PreProcessor.html#a975835b20868f3897b8dfaf5d4b4ac74',1,'blast::tools::PreProcessor::PreProcessor']]],
  ['trainwholevolume',['trainWholeVolume',['../classblast_1_1cnn_1_1Blast_1_1Blast.html#a214c7c34ba6388c0670c2172daaea3cb',1,'blast::cnn::Blast::Blast']]],
  ['transform',['transform',['../classblast_1_1tools_1_1Dropreg_1_1Dropreg.html#a1ce4ebe8d1f07df4213af165af521dfc',1,'blast.tools.Dropreg.Dropreg.transform()'],['../classblast_1_1tools_1_1NyulNormalizer_1_1NyulNormalizer.html#ad1046b090bd4f415ed54b3e33fd9511d',1,'blast.tools.NyulNormalizer.NyulNormalizer.transform()']]],
  ['transformusinglastresults',['transformUsingLastResults',['../classblast_1_1tools_1_1Dropreg_1_1Dropreg.html#ab80ae8f5b6baedc835a708c58e3e700a',1,'blast::tools::Dropreg::Dropreg']]],
  ['transformwithprefix',['transformWithPrefix',['../classblast_1_1tools_1_1Dropreg_1_1Dropreg.html#a3614724d93c7bd7777af318cb5aca342',1,'blast::tools::Dropreg::Dropreg']]]
];
